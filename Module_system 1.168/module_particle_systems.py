from header_particle_systems import *

#psf_always_emit         = 0x0000000002
#psf_global_emit_dir     = 0x0000000010
#psf_emit_at_water_level = 0x0000000020
#psf_billboard_2d        = 0x0000000100 # up_vec = dir, front rotated towards camera
#psf_billboard_3d        = 0x0000000200 # front_vec point to camera.
#psf_turn_to_velocity    = 0x0000000400
#psf_randomize_rotation  = 0x0000001000
#psf_randomize_size      = 0x0000002000
#psf_2d_turbulance       = 0x0000010000

####################################################################################################################
#   Each particle system contains the following fields:
#  
#  1) Particle system id (string): used for referencing particle systems in other files.
#     The prefix psys_ is automatically added before each particle system id.
#  2) Particle system flags (int). See header_particle_systems.py for a list of available flags
#  3) mesh-name.
####
#  4) Num particles per second:    Number of particles emitted per second.
#  5) Particle Life:    Each particle lives this long (in seconds).
#  6) Damping:          How much particle's speed is lost due to friction.
#  7) Gravity strength: Effect of gravity. (Negative values make the particles float upwards.)
#  8) Turbulance size:  Size of random turbulance (in meters)
#  9) Turbulance strength: How much a particle is affected by turbulance.
####
# 10,11) Alpha keys :    Each attribute is controlled by two keys and 
# 12,13) Red keys   :    each key has two fields: (time, magnitude)
# 14,15) Green keys :    For example scale key (0.3,0.6) means 
# 16,17) Blue keys  :    scale of each particle will be 0.6 at the
# 18,19) Scale keys :    time 0.3 (where time=0 means creation and time=1 means end of the particle)
#
# The magnitudes are interpolated in between the two keys and remain constant beyond the keys.
# Except the alpha always starts from 0 at time 0.
####
# 20) Emit Box Size :   The dimension of the box particles are emitted from.
# 21) Emit velocity :   Particles are initially shot with this velocity.
# 22) Emit dir randomness
# 23) Particle rotation speed: Particles start to rotate with this (angular) speed (degrees per second).
# 24) Particle rotation damping: How quickly particles stop their rotation
####################################################################################################################

#MOD begin
#burst_strength = total number of particles
#num_particles = particles emitted per second, lower value = more time needed to draw all particles

gun_smoke = [
	"prtcl_dust_b", #mesh
	3, #damping
	0, #gravity_strength
	2, #turbulance_strength
	0.25, #alpha
	0.5, #color_r
	0.5, #color_g
	0.5, #color_b
	100, #rotation_speed
	1] #rotation_damping
	
object_dust = [
	"prtcl_dust_b", #mesh
	2, #damping
	0.01, #gravity_strength
	2, #turbulance_strength
	0.5, #alpha
	1.0/255*185, #color_r
	1.0/255*155, #color_g
	1.0/255*117, #color_b
	100, #rotation_speed
	1] #rotation_damping
	
ground_dust = [
	"prtcl_dust_b", #mesh
	2, #damping
	0.01, #gravity_strength
	2, #turbulance_strength
	0.5, #alpha
	1.0/255*109, #color_r
	1.0/255*74, #color_g
	1.0/255*41, #color_b
	100, #rotation_speed
	1] #rotation_damping
	
ground_particle = [
	"prt_mud_1", #mesh
	0.3, #damping
	0.7, #gravity_strength
	0, #turbulance_strength
	1.0, #alpha
	1.0/255*61, #color_r
	1.0/255*42, #color_g
	1.0/255*26, #color_b
	50, #rotation_speed
	0.2] #rotation_damping
#MOD end

particle_systems = [
  
    ("game_rain", psf_billboard_2d|psf_global_emit_dir, "prtcl_rain",
     1000, 2, 0.1, 3, 5, 5,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 1.0), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (1.0, 1.0), (1.0, 1.0),   #scale keys
     (8, 8, 1),            #emit box size
     (0, 0, 0),               #emit velocity
     0.1,                       #emit dir randomness
     0,                       #rotation speed
     0.0                        #rotation damping
    ),
    ("game_snow", psf_billboard_3d|psf_global_emit_dir|psf_randomize_size|psf_randomize_rotation, "prt_mesh_snow_fall_1",
     1000, 3, 0.3, 0.5, 5, 10,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.5, 1.0), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 0.03), (1, 0.03),   #scale keys
     (8, 8, 1),           #emit box size
     (0, 0, 0),               #emit velocity
     1,                       #emit dir randomness
     200,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_blood", psf_billboard_3d |psf_randomize_size|psf_randomize_rotation,  "prt_mesh_blood_1",
     100, 1.2, 3, 0.5, 0, 0,        #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 0.5), (1, 0.0),          #alpha keys
     (0.1, 0.7), (1, 0.7),      #red keys
     (0.1, 0.7), (1, 0.7),       #green keys
     (0.1, 0.7), (1, 0.7),      #blue keys
     (0.0, 0.2),   (1, 0.4),  #scale keys
     (0, 0.05, 0),               #emit box size
     (0, 1.0, 0.3),                #emit velocity
     0.9,                       #emit dir randomness
     0,                         #rotation speed
     0,                         #rotation damping
    ),
    ("game_blood_2", psf_billboard_3d | psf_randomize_size|psf_randomize_rotation ,  "prt_mesh_blood_3",
     100, 1.2, 3, 0.3, 0, 0,        #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 0.5), (1, 0.0),        #alpha keys
     (0.1, 0.7), (1, 0.7),      #red keys
     (0.1, 0.7), (1, 0.7),       #green keys
     (0.1, 0.7), (1, 0.7),      #blue keys
     (0.0, 0.2),   (1, 0.4),    #scale keys
     (0.01, 0.2, 0.01),             #emit box size
     (0.2, 0.3, 0),                 #emit velocity
     0.3,                         #emit dir randomness
     150,                       #rotation speed
     0,                       #rotation damping
    ),
    
     ("game_hoof_dust", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_2d_turbulance, "prt_mesh_dust_1",#prt_mesh_dust_1
     4, 5.0,  10, 0.02, 10.0, 20.0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 0.2), (1, 0.0),        #alpha keys
     (0, 0.6), (1, 0.8),        #red keys
     (0, 0.5),(1, 0.7),         #green keys
     (0, 0.3),(1, 0.5),         #blue keys
     (0.0, 2.0),   (1.0, 5),   #scale keys
     (0.2, 0.3, 0.2),           #emit box size
     (0, 0, 3.0),                 #emit velocity
     0.5,                         #emit dir randomness
     130,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_hoof_dust_snow", psf_billboard_3d|psf_randomize_size, "prt_mesh_snow_dust_1",#prt_mesh_dust_1
     6, 2, 3.5, 1, 10.0, 0.0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.2, 1), (1, 1),        #alpha keys
     (0, 1), (1, 1),        #red keys
     (0, 1),(1, 1),         #green keys
     (0, 1),(1, 1),         #blue keys
     (0.5, 2),   (1.0, 5),   #scale keys
     (0.2, 1, 0.1),           #emit box size
     (0, 0, 1),                 #emit velocity
     2,                         #emit dir randomness
     0,                       #rotation speed
     0                        #rotation damping
    ),
     ("game_hoof_dust_mud", psf_billboard_2d|psf_randomize_size|psf_randomize_rotation|psf_2d_turbulance, "prt_mesh_mud_1",#prt_mesh_dust_1
     5, .7,  10, 3, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1), (1, 1),        #alpha keys
     (0, .7), (1, .7),        #red keys
     (0, 0.6),(1, 0.6),         #green keys
     (0, 0.4),(1, 0.4),         #blue keys
     (0.0, 0.2),   (1.0, 0.3),   #scale keys
     (0.15, 0.5, 0.1),           #emit box size
     (0, 0, 15),                 #emit velocity
     6,                         #emit dir randomness
     200,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_water_splash_1", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_emit_at_water_level, "prtcl_drop",
     20, 0.85, 0.25, 0.9, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.3, 0.5), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 0.3),   (1.0, 0.18),   #scale keys
     (0.3, 0.2, 0.1),           #emit box size
     (0, 1.2, 2.3),               #emit velocity
     0.3,                       #emit dir randomness
     50,                       #rotation speed
     0.5                        #rotation damping
    ),
    
    ("game_water_splash_2", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_emit_at_water_level, "prtcl_splash_b",
     30, 0.4, 0.7, 0.5, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.3, 1.0), (1, 0.3),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 0.25),   (1.0, 0.7),   #scale keys
     (0.4, 0.3, 0.1),           #emit box size
     (0, 1.3, 1.1),               #emit velocity
     0.1,                       #emit dir randomness
     50,                       #rotation speed
     0.5                        #rotation damping
    ),

    ("game_water_splash_3", psf_emit_at_water_level , "prt_mesh_water_wave_1",
     5, 2.0, 0, 0.0, 10.0, 0.0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.03, 0.2), (1, 0.0),        #alpha keys
     (1.0, 1.0), (1, 1.0),      #red keys
     (1.0, 1.0), (1, 1.0),      #green keys
     (1.0, 1.0), (1, 1.0),      #blue keys
     (0.0, 3),   (1.0, 10),   #scale keys
     (0.0, 0.0, 0.0),           #emit box size
     (0, 0, 0),                 #emit velocity
     0.0,                       #emit dir randomness
     0,                       #rotation speed
     0.5                        #rotation damping
    ),
    
     ("ladder_dust_6m", psf_billboard_3d, "prt_mesh_smoke_1",
     700, 0.9, 0, 0, 7, 7, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.25), (1, 0),          #alpha keys
     (0.0, 1), (1, 0.8),      #red keys
     (0.0, 1),(1, 0.8),      #green keys
     (0.0, 1), (1, 0.8),     #blue keys
     (0, 1),   (1, 2),        #scale keys
     (0.75, 0.75, 3.5),                 #emit box size  (6.5 is equal to (12m / 2) + 0.5)
     (0, 0, 0),               #emit velocity
     0.1,                        #emit dir randomness
     100,
     0.2,
    ),

     ("ladder_dust_8m", psf_billboard_3d, "prt_mesh_smoke_1",
     900, 0.9, 0, 0, 7, 7, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.25), (1, 0),          #alpha keys
     (0.0, 1), (1, 0.8),      #red keys
     (0.0, 1),(1, 0.8),      #green keys
     (0.0, 1), (1, 0.8),     #blue keys
     (0, 1),   (1, 2),        #scale keys
     (0.75, 0.75, 4.5),                 #emit box size  (6.5 is equal to (12m / 2) + 0.5)
     (0, 0, 0),               #emit velocity
     0.1,                        #emit dir randomness
     100,
     0.2,
    ),

     ("ladder_dust_10m", psf_billboard_3d, "prt_mesh_smoke_1",
     1100, 0.9, 0, 0, 7, 7, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.25), (1, 0),          #alpha keys
     (0.0, 1), (1, 0.8),      #red keys
     (0.0, 1),(1, 0.8),      #green keys
     (0.0, 1), (1, 0.8),     #blue keys
     (0, 1),   (1, 2),        #scale keys
     (0.75, 0.75, 5.5),                 #emit box size  (6.5 is equal to (12m / 2) + 0.5)
     (0, 0, 0),               #emit velocity
     0.1,                        #emit dir randomness
     100,
     0.2,
    ),

     ("ladder_dust_12m", psf_billboard_3d, "prt_mesh_smoke_1",
     1300, 0.9, 0, 0, 7, 7, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.25), (1, 0),          #alpha keys
     (0.0, 1), (1, 0.8),      #red keys
     (0.0, 1),(1, 0.8),      #green keys
     (0.0, 1), (1, 0.8),     #blue keys
     (0, 1),   (1, 2),        #scale keys
     (0.75, 0.75, 6.5),                 #emit box size  (6.5 is equal to (12m / 2) + 0.5)
     (0, 0, 0),               #emit velocity
     0.1,                        #emit dir randomness
     100,
     0.2,
    ),

     ("ladder_dust_14m", psf_billboard_3d, "prt_mesh_smoke_1",
     1500, 0.9, 0, 0, 7, 7, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 0.25), (1, 0),          #alpha keys
     (0.0, 1), (1, 0.8),      #red keys
     (0.0, 1),(1, 0.8),      #green keys
     (0.0, 1), (1, 0.8),     #blue keys
     (0, 1),   (1, 2),        #scale keys
     (0.75, 0.75, 7.5),                 #emit box size  (7.5 is equal to (14m / 2) + 0.5)
     (0, 0, 0),               #emit velocity
     0.1,                        #emit dir randomness
     100,
     0.2,
    ),

    ("ladder_straw_6m", psf_randomize_size | psf_randomize_rotation,  "prt_mesh_straw_1",
     700, 1, 2, 0.9, 10, 2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1), (1, 1),          #alpha keys
     (0.1, 0.6), (1, 0.6),      #red keys
     (0.1, 0.5),(1, 0.5),       #green keys
     (0.1, 0.4), (1, 0.4),      #blue keys
     (0.0, 0.3),   (1, 0.3),    #scale keys
     (0.75, 0.75, 3.5),           #emit box size
     (0, 0, 0),                 #emit velocity
     2.3,                       #emit dir randomness
     200,                       #rotation speed
     0,                       #rotation damping
    ),

    ("ladder_straw_8m", psf_randomize_size | psf_randomize_rotation,  "prt_mesh_straw_1",
     900, 1, 2, 0.9, 10, 2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1), (1, 1),          #alpha keys
     (0.1, 0.6), (1, 0.6),      #red keys
     (0.1, 0.5),(1, 0.5),       #green keys
     (0.1, 0.4), (1, 0.4),      #blue keys
     (0.0, 0.3),   (1, 0.3),    #scale keys
     (0.75, 0.75, 4.5),           #emit box size
     (0, 0, 0),                 #emit velocity
     2.3,                       #emit dir randomness
     200,                       #rotation speed
     0,                       #rotation damping
    ),

    ("ladder_straw_10m", psf_randomize_size | psf_randomize_rotation,  "prt_mesh_straw_1",
     1100, 1, 2, 0.9, 10, 2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1), (1, 1),          #alpha keys
     (0.1, 0.6), (1, 0.6),      #red keys
     (0.1, 0.5),(1, 0.5),       #green keys
     (0.1, 0.4), (1, 0.4),      #blue keys
     (0.0, 0.3),   (1, 0.3),    #scale keys
     (0.75, 0.75, 5.5),           #emit box size
     (0, 0, 0),                 #emit velocity
     2.3,                       #emit dir randomness
     200,                       #rotation speed
     0,                       #rotation damping
    ),

    ("ladder_straw_12m", psf_randomize_size | psf_randomize_rotation,  "prt_mesh_straw_1",
     1300, 1, 2, 0.9, 10, 2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1), (1, 1),          #alpha keys
     (0.1, 0.6), (1, 0.6),      #red keys
     (0.1, 0.5),(1, 0.5),       #green keys
     (0.1, 0.4), (1, 0.4),      #blue keys
     (0.0, 0.3),   (1, 0.3),    #scale keys
     (0.75, 0.75, 6.5),           #emit box size
     (0, 0, 0),                 #emit velocity
     2.3,                       #emit dir randomness
     200,                       #rotation speed
     0,                       #rotation damping
    ),

    ("ladder_straw_14m", psf_randomize_size | psf_randomize_rotation,  "prt_mesh_straw_1",
     1500, 1, 2, 0.9, 10, 2,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.1, 1), (1, 1),          #alpha keys
     (0.1, 0.6), (1, 0.6),      #red keys
     (0.1, 0.5),(1, 0.5),       #green keys
     (0.1, 0.4), (1, 0.4),      #blue keys
     (0.0, 0.3),   (1, 0.3),    #scale keys
     (0.75, 0.75, 7.5),           #emit box size
     (0, 0, 0),                 #emit velocity
     2.3,                       #emit dir randomness
     200,                       #rotation speed
     0,                       #rotation damping
    ),

    ("firearm_flashpan_fire", psf_billboard_3d|psf_randomize_rotation, "prt_sparks_1",
     10, 0.1, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1.0), (1, 1.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 0.2), (1, 1.0), #scale keys
     (0, 0, 0), #emit box size
     (-1.5, 0, 0), #emit velocity
     0.0, #emit dir randomness
     0, #rotation speed
     0.0 #rotation damping
    ),

    ("musket_muzzle_fire", psf_billboard_3d|psf_randomize_rotation, "prt_explosion_1",
     10, 0.1, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1.0), (1, 1.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 0.2), (1, 1.0), #scale keys
     (0, 0, 0), #emit box size
     (0, 3, 0), #emit velocity
     0.0, #emit dir randomness
     0, #rotation speed
     0.0 #rotation damping
    ),
    
    ("pistol_muzzle_fire", psf_billboard_3d|psf_randomize_rotation, "prt_explosion_1",
     10, 0.1, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1.0), (1, 1.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 0.2), (1, 1.0), #scale keys
     (0, 0, 0), #emit box size
     (0, 2.5, 0), #emit velocity
     0.0, #emit dir randomness
     0, #rotation speed
     0.0 #rotation damping
    ),

    ("firearm_flashpan_smoke", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, gun_smoke[0],
     50, 15, gun_smoke[1], gun_smoke[2], 5, gun_smoke[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.05, gun_smoke[4]), (1, 0.0), #alpha keys
     (0, gun_smoke[5]), (1, gun_smoke[5]), #red keys
     (0, gun_smoke[6]), (1, gun_smoke[6]), #green keys
     (0, gun_smoke[7]), (1, gun_smoke[7]), #blue keys
     (0, 0.5), (0.2, 1.5), #scale keys
     (0, 0, 0), #emit box size
     (-2, 0, 0), #emit velocity
     0.5, #emit dir randomness
     gun_smoke[8], #rotation speed
     gun_smoke[9], #rotation damping
    ),
    
    ("musket_muzzle_smoke", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, gun_smoke[0],
     50, 15, gun_smoke[1], gun_smoke[2], 5, gun_smoke[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.05, gun_smoke[4]), (1, 0.0), #alpha keys
     (0, gun_smoke[5]), (1, gun_smoke[5]), #red keys
     (0, gun_smoke[6]), (1, gun_smoke[6]), #green keys
     (0, gun_smoke[7]), (1, gun_smoke[7]), #blue keys
     (0, 0.5), (0.2, 1.5), #scale keys
     (0, 0, 0), #emit box size
     (0, 7, 0), #emit velocity
     0.5, #emit dir randomness
     gun_smoke[8], #rotation speed
     gun_smoke[9], #rotation damping
    ),

    ("pistol_muzzle_smoke", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, gun_smoke[0],
     50, 15, gun_smoke[1], gun_smoke[2], 5, gun_smoke[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.05, gun_smoke[4]), (1, 0.0), #alpha keys
     (0, gun_smoke[5]), (1, gun_smoke[5]), #red keys
     (0, gun_smoke[6]), (1, gun_smoke[6]), #green keys
     (0, gun_smoke[7]), (1, gun_smoke[7]), #blue keys
     (0, 0.5), (0.2, 1.5), #scale keys
     (0, 0, 0), #emit box size
     (0, 5, 0), #emit velocity
     0.5, #emit dir randomness
     gun_smoke[8], #rotation speed
     gun_smoke[9], #rotation damping
    ),

    ("cannon_flashpan_fire", psf_billboard_3d|psf_randomize_rotation, "prt_sparks_1",
     10, 0.1, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1.0), (1, 1.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 0.2), (1, 1), #scale keys
     (0, 0, 0), #emit box size
     (0, -1, 3), #emit velocity
     0.0, #emit dir randomness
     0, #rotation speed
     0.0 #rotation damping
    ),

    ("cannon_flashpan_smoke", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, gun_smoke[0],
     50, 15, gun_smoke[1], gun_smoke[2], 5, gun_smoke[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.05, gun_smoke[4]), (1, 0.0), #alpha keys
     (0, gun_smoke[5]), (1, gun_smoke[5]), #red keys
     (0, gun_smoke[6]), (1, gun_smoke[6]), #green keys
     (0, gun_smoke[7]), (1, gun_smoke[7]), #blue keys
     (0, 0.5), (0.2, 2), #scale keys
     (0, 0, 0), #emit box size
     (0, -0.8, 2.4), #emit velocity
     0.5, #emit dir randomness
     gun_smoke[8], #rotation speed
     gun_smoke[9], #rotation damping
    ),

    ("cannon_muzzle_fire", psf_billboard_3d|psf_randomize_rotation, "prt_explosion_1",
     5, 0.2, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0, 1.0), (1, 1.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 0.5), (1, 1.5), #scale keys
     (0, 0, 0), #emit box size
     (0, 4, 0), #emit velocity
     0.0, #emit dir randomness
     0, #rotation speed
     0.0 #rotation damping
    ),

    ("cannon_muzzle_smoke", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, gun_smoke[0],
     200, 15, gun_smoke[1], gun_smoke[2], 50, gun_smoke[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.05, gun_smoke[4]), (1, 0.0), #alpha keys
     (0, gun_smoke[5]), (1, gun_smoke[5]), #red keys
     (0, gun_smoke[6]), (1, gun_smoke[6]), #green keys
     (0, gun_smoke[7]), (1, gun_smoke[7]), #blue keys
     (0, 1), (0.2, 3), #scale keys
     (0.2, 0.2, 0.2), #emit box size
     (0, 20, 0), #emit velocity
     3.0, #emit dir randomness
     gun_smoke[8], #rotation speed
     gun_smoke[9], #rotation damping
    ),

    ("bullet_hit_particle", psf_billboard_3d|psf_randomize_rotation|psf_global_emit_dir, ground_particle[0],
     10, 2, ground_particle[1], ground_particle[2], 0.0, ground_particle[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, ground_particle[4]), (1, 0.0), #alpha keys
     (0.0, ground_particle[5]), (1, ground_particle[5]), #red keys
     (0.0, ground_particle[6]), (1, ground_particle[6]), #green keys
     (0.0, ground_particle[7]), (1, ground_particle[7]), #blue keys
     (0.0, 1.0), (1.0, 2.0), #scale keys
     (0, 0, 0), #emit box size
     (0, 0, 2), #emit velocity
     0.0, #emit dir randomness
     ground_particle[8], #rotation speed
     ground_particle[9] #rotation damping
    ),

    ("bullet_hit", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation|psf_global_emit_dir, ground_dust[0],
     30, 5, ground_dust[1], ground_dust[2], 0.5, ground_dust[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, ground_dust[4]), (1, 0), #alpha keys
     (0.0, ground_dust[5]), (1, ground_dust[5]), #red keys
     (0.0, ground_dust[6]), (1, ground_dust[6]), #green keys
     (0.0, ground_dust[7]), (1, ground_dust[7]), #blue keys
     (0, 0.3), (0.5, 1), #scale keys
     (0, 0, 0), #emit box size
     (0, 0, 1), #emit velocity
     0.1, #emit dir randomness
     ground_dust[8], #rotation speed
     ground_dust[9] #rotation damping
    ),
		
    ("bullet_hit_objects", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, object_dust[0],
     30, 5, object_dust[1], object_dust[2], 0.5, object_dust[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, object_dust[4]), (1, 0), #alpha keys
     (0.0, object_dust[5]), (1, object_dust[5]), #red keys
     (0.0, object_dust[6]), (1, object_dust[6]), #green keys
     (0.0, object_dust[7]), (1, object_dust[7]), #blue keys
     (0, 0.3), (0.5, 1), #scale keys
     (0, 0, 0), #emit box size
     (0, -1, 0), #emit velocity
     0.1, #emit dir randomness
     object_dust[8], #rotation speed
     object_dust[9] #rotation damping
    ),

    ("water_hit_a", psf_billboard_3d | psf_randomize_size | psf_randomize_rotation | psf_global_emit_dir, "prtcl_splash_b",
     40, 1, 0.5, 0.2, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1), (1, 0), #alpha keys
     (0.0, 1.0), (1, 1.0), #red keys
     (0.0, 1.0), (1, 1.0), #green keys
     (0.0, 1.0), (1, 1.0), #blue keys
     (0, 0.2), (1, 1), #scale keys
     (0, 0, 0), #emit box size
     (0, 0, 1.5), #emit velocity
     0, #emit dir randomness
     50, #rotation speed
     0.2 #rotation damping
    ),
		
    ("water_hit_b",psf_randomize_size | psf_randomize_rotation | psf_turn_to_velocity|psf_global_emit_dir, "prtcl_splash_b",
     10, 10, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1), (1, 0), #alpha keys
     (0.0, 1.0), (1, 1.0), #red keys
     (0.0, 1.0), (1, 1.0), #green keys
     (0.0, 1.0), (1, 1.0), #blue keys
     (0, 0.5), (1, 10), #scale keys
     (0.1, 0.1, 0), #emit box size
     (0, 0, -0.01), #emit velocity
     0, #emit dir randomness
     25, #rotation speed
     0.1 #rotation damping
    ),

    ("hit_explosion_big", psf_billboard_3d|psf_randomize_rotation, "prt_explosion_1",
     5, 0.2, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1.0, 0.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 2), (1, 4), #scale keys
     (0.0, 0.0, 0.0), #emit box size
     (0, 0, 2), #emit velocity
      0, #emit dir randomness
      0, #rotation speed
      0.0 #rotation damping
    ),

    ("hit_sparks_big", psf_billboard_3d|psf_randomize_rotation, "prt_sparks_1",
     10, 0.2, 0, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, 1.0), (1.0, 0.0), #alpha keys
     (0, 1.0), (1, 1.0), #red keys
     (0, 1.0), (1, 1.0), #green keys
     (0, 1.0), (1, 1.0), #blue keys
     (0, 2.0), (1, 4.0), #scale keys
     (0, 0, 0), #emit box size
     (0, 0, 10), #emit velocity
      0.0, #emit dir randomness
      0, #rotation speed
      0.0 #rotation damping
    ),

    ("hit_ground_big", psf_billboard_3d|psf_randomize_size|psf_randomize_rotation, ground_particle[0],
     1000, 3, ground_particle[1], ground_particle[2], 0.0, ground_particle[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, ground_particle[4]), (1, 0.0), #alpha keys
     (0.0, ground_particle[5]), (1, ground_particle[5]), #red keys
     (0.0, ground_particle[6]), (1, ground_particle[6]), #green keys
     (0.0, ground_particle[7]), (1, ground_particle[7]), #blue keys
     (0.0, 1.0), (1.0, 2.0), #scale keys
     (0.5, 0.5, 0.5), #emit box size
     (0, 0, 4), #emit velocity
     1.0, #emit dir randomness
     ground_particle[8], #rotation speed
     ground_particle[9] #rotation damping
    ),
    
    ("hit_object_dust_big", psf_randomize_size|psf_randomize_rotation|psf_billboard_3d, object_dust[0],
     20, 10, object_dust[1], object_dust[2], 0.5, object_dust[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, object_dust[4]), (1, 0), #alpha keys
     (0.0, object_dust[5]), (1, object_dust[5]), #red keys
     (0.0, object_dust[6]), (1, object_dust[6]), #green keys
     (0.0, object_dust[7]), (1, object_dust[7]), #blue keys
     (0.0, 2), (1, 5), #scale keys
     (1.0, 1.0, 1.0), #emit box size
     (0, 0, 1), #emit velocity
     0.25, #emit dir randomness
     object_dust[8], #rotation speed
     object_dust[9] #rotation damping
		),
		 
		("hit_ground_dust_big", psf_randomize_size|psf_randomize_rotation|psf_billboard_3d, ground_dust[0],
     20, 10, ground_dust[1], ground_dust[2], 0.5, ground_dust[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
     (0.0, ground_dust[4]), (1, 0), #alpha keys
     (0.0, ground_dust[5]), (1, ground_dust[5]), #red keys
     (0.0, ground_dust[6]), (1, ground_dust[6]), #green keys
     (0.0, ground_dust[7]), (1, ground_dust[7]), #blue keys
     (0.0, 2), (1, 5), #scale keys
     (1.0, 1.0, 1.0), #emit box size
     (0, 0, 1), #emit velocity
     0.25, #emit dir randomness
     ground_dust[8], #rotation speed
     ground_dust[9] #rotation damping
    ),
]

#MOD begin
for i in range(0, 2):
	if i == 0:
		prefix_1 = '_ground'
		array = ground_dust
	else:
		prefix_1 = '_object'
		array = object_dust
		
	for j in range(0, 3):
		if j == 0:
			prefix_2 = '_sml'
			size_1 = 0.3
			size_2 = 0.6
			box_size = 0.1
		elif j == 1:
			prefix_2 = '_med'
			size_1 = 0.6
			size_2 = 1.2
			box_size = 0.2
		else:
			prefix_2 = '_big'
			size_1 = 0.9
			size_2 = 1.8
			box_size = 0.3
		
		particle_systems +=(
			"missile_dust" + prefix_1 + prefix_2, psf_billboard_3d|psf_always_emit|psf_randomize_rotation, array[0],
			 30, 0.5, 0, 0, 0.5, array[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
			 (0.0, array[4]), (1, 0), #alpha keys
			 (0.0, array[5]), (1, array[5]), #red keys
			 (0.0, array[6]), (1, array[6]), #green keys
			 (0.0, array[7]), (1, array[7]), #blue keys
			 (0, size_1), (1, size_2), #scale keys
			 (box_size, 0, box_size), #emit box size
			 (0, 0, 0), #emit velocity
			 0.0, #emit dir randomness
			 array[8], #rotation speed
			 array[9]), #rotation damping
			 
for i in range(0, 2):
	if i == 0:
		prefix_1 = '_ground'
		array = ground_dust
	else:
		prefix_1 = '_object'
		array = object_dust
		
	for j in range(0, 3):
		if j == 0:
			prefix_2 = '_sml'
			size_1 = 0.5
			size_2 = 0.5
			box_size = 0.2
			velocity = 8
		elif j == 1:
			prefix_2 = '_med'
			size_1 = 1.0
			size_2 = 1.0
			box_size = 0.3
			velocity = 6
		else:
			prefix_2 = '_big'
			size_1 = 1.5
			size_2 = 1.5
			box_size = 0.4
			velocity = 4
		
		particle_systems +=(
			"launch_dust" + prefix_1 + prefix_2, psf_billboard_3d|psf_randomize_rotation, array[0],
			 20, 15, array[1], array[2], 0.5, array[3], #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
			 (0.0, array[4]), (1, 0), #alpha keys
			 (0.0, array[5]), (1, array[5]), #red keys
			 (0.0, array[6]), (1, array[6]), #green keys
			 (0.0, array[7]), (1, array[7]), #blue keys
			 (0, size_1), (1, size_2), #scale keys
			 (box_size, 0, box_size), #emit box size
			 (0, velocity, 0), #emit velocity
			 0.1, #emit dir randomness
			 array[8], #rotation speed
			 array[9]), #rotation damping
			 
for i in range(1, 9):
  particle_systems +=(
		"lightning_" + str(i), psf_billboard_2d, "lightning_" + str(i),
    4, 0.25, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
		(0.5, 1.0), (1, 0.0), #alpha keys
    (0, 1.0), (1, 1.0), #red keys
    (0, 1.0), (1, 1.0), #green keys
    (0, 1.0), (1, 1.0), #blue keys
    (0, 500),   (1, 500),   #scale keys
    (0.0, 0.0, 0.0),      #emit box size
    (0, 0, 0),               #emit velocity
    0.0,                       #emit dir randomness
    0,                       #rotation speed
    0.0                        #rotation damping
  ),
particle_systems +=(
	"upper_lightning", psf_billboard_3d|psf_randomize_rotation, "prt_mesh_fire_3",
	4, 0.25, 0, 0, 0, 0,     #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
	(0.5, 1.0), (1, 0.0), #alpha keys
	(0, 1.0), (1, 1.0), #red keys
	(0, 1.0), (1, 1.0), #green keys
	(0, 1.0), (1, 1.0), #blue keys
	(0, 500),   (1, 500),   #scale keys
	(0.0, 0.0, 0.0),      #emit box size
	(0, 0, 0),               #emit velocity
	0.0,                       #emit dir randomness
	0,                       #rotation speed
	0.0                        #rotation damping
	),
			 
prefix_2 = ['_xs','_s','_m','_l','_xl']
for i in range(0, 2):
	if i == 0:
		prefix_1 = 'fire'
		mesh = "prt_mesh_fire_1"
		count = 8
		life = 1
		damping = 0.0
		alpha_start = 0.25
		alpha_value = 1
		color_r_1 = 1.0
		color_r_2 = 0.9
		color_g_1 = 0.7
		color_g_2 = 0.3
		color_b_1 = 0.2
		color_b_2 = 0.0
		size_1 = 1.0/4
		size_2 = 1.0/8
		velocity = 0.5
		emit_box = 1.0/16
		rot_speed = 200
		rot_damp = 0.2
		
	else:
		prefix_1 = 'smoke'
		mesh = "prtcl_dust_b"
		count = 4
		life = 2
		damping = 0.0
		alpha_start = 0.5
		alpha_value = 0.5
		color_r_1 = 0.1
		color_r_2 = 0.1
		color_g_1 = 0.1
		color_g_2 = 0.1
		color_b_1 = 0.1
		color_b_2 = 0.1
		size_1 = 1.0/4
		size_2 = 1.0/2
		velocity = 0.5
		emit_box = 1.0/16
		rot_speed = 100
		rot_damp = 0.2
		
	for j in range(0, 5):
		# vel_z = velocity * (j + 1)
		# duration = life * (j + 1)
		# start = alpha_start / (j + 1)
		particle_systems +=(
			prefix_1 + prefix_2[j], psf_billboard_3d|psf_global_emit_dir|psf_always_emit|psf_randomize_size|psf_randomize_rotation, mesh,
			count, life, damping, 0, 0, 0, #num_particles, life, damping, gravity_strength, turbulance_size, turbulance_strength
			(alpha_start, alpha_value), (1, 0), #alpha keys
			(0.0, color_r_1), (1, color_r_2), #red keys
			(0.0, color_g_1), (1, color_g_2), #green keys
			(0.0, color_b_1), (1, color_b_2), #blue keys
			(0, size_1), (1, size_2), #scale keys
			(emit_box, emit_box, emit_box), #emit box size
			(0, 0, velocity), #emit velocity
			0, #emit dir randomness
			rot_speed, #rotation speed
			rot_damp), #rotation damping
		
		count *= 2
		emit_box *= 2 
		size_1 *= 1.5
		size_2 *= 1.5
		velocity *= 1.5
#MOD end