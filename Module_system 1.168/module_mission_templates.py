from header_common import *
from header_operations import *
from header_mission_templates import *
from header_animations import *
from header_sounds import *
from header_music import *
from header_items import *
from module_constants import *

####################################################################################################################
#   Each mission-template is a tuple that contains the following fields:
#  1) Mission-template id (string): used for referencing mission-templates in other files.
#     The prefix mt_ is automatically added before each mission-template id
#
#  2) Mission-template flags (int): See header_mission-templates.py for a list of available flags
#  3) Mission-type(int): Which mission types this mission template matches.
#     For mission-types to be used with the default party-meeting system,
#     this should be 'charge' or 'charge_with_ally' otherwise must be -1.
#     
#  4) Mission description text (string).
#  5) List of spawn records (list): Each spawn record is a tuple that contains the following fields:
#    5.1) entry-no: Troops spawned from this spawn record will use this entry
#    5.2) spawn flags.
#    5.3) alter flags. which equipment will be overriden
#    5.4) ai flags.
#    5.5) Number of troops to spawn.
#    5.6) list of equipment to add to troops spawned from here (maximum 8).
#  6) List of triggers (list).
#     See module_triggers.py for infomation about triggers.
#
#  Please note that mission templates is work in progress and can be changed in the future versions.
# 
####################################################################################################################

# pilgrim_disguise = [itm_staff, itm_throwing_daggers]#MOD edit
# af_castle_lord = af_override_horse | af_override_weapons| af_require_civilian

#MOD begin
common_mod = [

(0, 0, ti_once, 
[
	(tutorial_message_set_size, 17, 17),
	(tutorial_message_set_position, 500, 700),
	(tutorial_message_set_center_justify, 1),
	(tutorial_message_set_background, 1),

	# (assign, "$last_frame_time", 0),
], []),

	(ti_before_mission_start, 0, 0,
  [
#    (this_or_next|multiplayer_is_server),
#    (neg|game_in_multiplayer_mode),
  ],
  [
		(call_script, "script_init_on_round_start"),
		(replace_scene_props, "spr_cannon_1", "spr_cannon_base_1"),

		(try_begin),
			# (eq, "$g_replace_editor_props", 1),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			(call_script, "script_multiplayer_server_set_weather_time"),

			(multiplayer_is_dedicated_server),
			(assign, "$g_next_announcement_time", 30),
			(try_begin),#allways anable debug messages on dedicated servers
				(eq, "$g_show_debug_messages", 0),
				(assign, "$g_show_debug_messages", 1),
			(try_end),
			(eq, "$last_player_count", -1),#only once at the first time the server starts
			(assign, "$last_player_count", 0),

			(eq, "$g_allow_online_chars", 1),
			(str_store_string, s1, "str_server_security_token"),
			(str_store_server_name, s2),
			(str_store_string, s4, "@0"),
			(assign, reg2, "str_service_action_1"),
			(str_store_string, s10, "str_service_action_1"),
			(str_store_string, s10, "str_service_url_1"),
			# (str_encode_url, s10),
			(send_message_to_url, s10),
			
			(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
			(display_message, s0),
			(server_add_message_to_log, s0),
			#(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
		(try_end),
		
		(set_global_haze_amount, 0),
		(scene_set_day_time, "$g_day_time"),
		(troop_get_slot, "$g_fog_color", "trp_fog_color", "$g_day_time"),
		(store_add, ":sky_box", "$g_day_time", 1),
		(set_skybox, ":sky_box", ":sky_box"),
		
		(try_begin),
			(neg|multiplayer_is_dedicated_server),
			
			#set damage report on player
			(try_begin),
				(game_in_multiplayer_mode),
				(neq, "$g_show_damage_report", 0),
				(multiplayer_send_2_int_to_server, multiplayer_event_server_set_player_slot, slot_player_show_damage_report, "$g_show_damage_report"),
			(try_end),
			
			(scene_prop_get_instance, ":instance_no", "spr_?_ambient_sounds", 0),
			(try_begin),
				(prop_instance_is_valid, ":instance_no"),
				(prop_instance_get_variation_id, "$g_cur_scene_type", ":instance_no"),
				(try_begin),
					(this_or_next|eq, "$g_cur_scene_type", scene_type_plain),
					(this_or_next|eq, "$g_cur_scene_type", scene_type_plain_forest),
					(this_or_next|eq, "$g_cur_scene_type", scene_type_steppe),
					(eq, "$g_cur_scene_type", scene_type_steppe_forest),
					(try_begin),
						(assign, ":continue", 0),
						(try_begin),
							(game_in_multiplayer_mode),
							(try_begin),
								(neg|is_between, "$g_day_time", 4, 21),
								(assign, ":continue", 1),
							(try_end),
						(else_try),
							(is_currently_night),
							(assign, ":continue", 1),
						(try_end),
						(eq, ":continue", 1),
						(play_sound, "snd_plain_ambiance_night", 0),
					(else_try),
						(play_sound, "snd_plain_ambiance_day", 0),
						(play_sound, "snd_plain_birds", 0),
					(try_end),
				(else_try),
					(this_or_next|eq, "$g_cur_scene_type", scene_type_desert),
					(eq, "$g_cur_scene_type", scene_type_desert_forest), 
					(play_sound, "snd_desert_ambiance", 0),
				(try_end),

				(prop_instance_get_variation_id_2, "$g_cur_scene_water", ":instance_no"),
				(try_begin),
					(eq, "$g_cur_scene_water", scene_water_ocean),
					(play_sound, "snd_ocean_ambiance_distant", 0),
					(play_sound, "snd_ocean_ambiance", 0),
					(play_sound, "snd_ocean_gulls", 0),
				(else_try),
					(eq, "$g_cur_scene_water", scene_water_ocean_far),
					(play_sound, "snd_ocean_ambiance_distant", 0),
				(try_end),
			(try_end),
		(try_end),
	]),

	(ti_after_mission_start, 0, 0,
  [
    # (this_or_next|multiplayer_is_server),
    # (neg|game_in_multiplayer_mode),
  ],
  [
		#get scene size
		(get_scene_boundaries, pos10, pos11),
		(set_fixed_point_multiplier, 100),
    # (position_get_x, "$g_scene_min_x", pos10),
    # (position_get_y, "$g_scene_min_y", pos10),
		(position_get_x, "$g_scene_max_x", pos11),
    (position_get_y, "$g_scene_max_y", pos11),
		(val_add, "$g_scene_max_x", 2400),#2400 has been subtracted automatically
		(val_add, "$g_scene_max_y", 2400),
		
		(store_div, ":pos_x", "$g_scene_max_x", 2),
		(store_div, ":pos_y", "$g_scene_max_x", 2),
	
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			
			(init_position, pos0),
			(position_set_x, pos0, ":pos_x"),
			(position_set_y, pos0, ":pos_y"),
			(set_spawn_position, pos0),
			(spawn_scene_prop, "spr_clouds"),

	#   (try_for_range, ":cur_cannon_bit", "spr_cannon_barrel_1_32lb", "spr_cannons_end"),
				(scene_prop_get_num_instances, ":num_instances", "spr_cannon_base_1"),
				(try_for_range, ":cur_instance", 0, ":num_instances"),
					(scene_prop_get_instance, ":instance_id", "spr_cannon_base_1", ":cur_instance"),
					(assign, "$g_last_parent_prop", ":instance_id"),
					(prop_instance_get_position, pos0, ":instance_id"),#base pos

					(set_fixed_point_multiplier, 100),
					(try_for_range, ":i", 0, 20),
						(troop_get_slot, ":cur_child_prop", "trp_cannon_1_childs", ":i"),
						(gt, ":cur_child_prop", -1),
						(store_add, ":cur_child_pos", "trp_cannon_1_child_1_pos", ":i"),
						(troop_get_slot, ":pos_x", ":cur_child_pos", pos_x),
						(troop_get_slot, ":pos_y", ":cur_child_pos", pos_y),
						(troop_get_slot, ":pos_z", ":cur_child_pos", pos_z),
						(copy_position, pos_spawn, pos0),
						(position_move_x, pos_spawn, ":pos_x"),
						(position_move_y, pos_spawn, ":pos_y"),
						(position_move_z, pos_spawn, ":pos_z"),
						(set_spawn_position, pos_spawn),
						(spawn_scene_prop, ":cur_child_prop"),
					(try_end),
				(try_end),
	#   (try_end),
		(try_end),
		
		(try_begin),
			(assign, reg12, "$g_scene_max_x"),
			(assign, reg13, "$g_scene_max_y"),
			(eq, "$g_show_debug_messages", 1),
		  (display_message, "@[DEBUG] scene size in cm X:{reg12} Y:{reg13}"),
	  (try_end),

		# (try_begin),
			# (this_or_next|multiplayer_is_server),
			# (neg|game_in_multiplayer_mode),
			
			# (store_div, ":pos_x", "$g_scene_max_x", 2),
			# (store_div, ":pos_y", "$g_scene_max_y", 2),
			
			# (init_position, pos1),
			# (position_set_x, pos1, ":pos_x"),
			# (position_set_y, pos1, ":pos_y"),
			# (store_random_in_range, ":rot_z", 0, 360),
			# (position_rotate_z, pos1, ":rot_z"),
			# (set_spawn_position, pos1),
			# (spawn_scene_prop, "spr_clouds"),
		# (try_end),
		
		(game_in_multiplayer_mode),
		(neg|multiplayer_is_dedicated_server),
		(try_begin),
			(multiplayer_is_server),
			(call_script, "script_apply_weather"),
		(else_try),#only client side on dedicated servers
			(multiplayer_get_my_player, ":my_player_no"),
			(player_set_slot, ":my_player_no", slot_player_online_status, online_status_none),#reset slot
			(assign, "$g_player_first_connect", 1),
			# (start_presentation, "prsnt_multiplayer_connect_to_database"),
			(start_presentation, "prsnt_multiplayer_team_select"),
		(try_end),
	 ]),

#SLOT SYSTEM begin
#agent spawn
	(ti_on_agent_spawn, 0, 0, [],
  	[
    	(store_trigger_param_1, ":agent"),

    	(store_mission_timer_a_msec, ":time_ms"),

    	(store_random_in_range, ":time", 0, 1000),

    	(assign, reg0, ":agent"),
    	(assign, reg1, ":time"),

    	# (team_give_order, -1, grc_everyone, mordr_hold),
    	# (agent_get_team, reg2, ":agent"),
    	# (agent_get_group, reg3, ":agent"),
    	# (display_message, "@agent:{reg0} time:{reg1} team:{reg2} group:{reg3}"),

    	(val_add, ":time", ":time_ms"),
    	(agent_set_slot, ":agent", slot_agent_time_ms, ":time"),
	
		# (agent_is_active, ":agent"),
		# (agent_is_alive, ":agent"),
		(agent_is_human, ":agent"),

		(call_script, "script_agent_get_gender", ":agent"),#get the gender

    #slot system
		(try_begin),#server side only
			(this_or_next|multiplayer_is_server),
      (neg|game_in_multiplayer_mode),
    
      (assign, ":carry_weight", 0),
      (assign, ":armor_weight", 0),
			(assign, ":weight_lowers_wpf", 0),
			
			(assign, ":reduce_bolts", 0),
			(assign, ":reduce_bullets", 0),
			
			(assign, ":bolt_slot", -1),
			(assign, ":bullet_slot", -1),
			
			(assign, ":crossbow_item", -1),
			(assign, ":musket_item", -1),
			(assign, ":pistol_item", -1),

			(try_for_range, reg0, ek_item_0, ek_horse),
				(agent_get_item_slot, ":item", ":agent", reg0),
				(gt, ":item", -1),
				(try_begin),#weapon slots
					(is_between, reg0, ek_item_0, ek_head),
					(item_get_slot, ":carry_slot", ":item", slot_item_carry_slot),
					(store_add, ":agent_slot", slot_agent_carry_slot_0_used, ":carry_slot"),
					
					(try_begin),#slot already used
						(agent_slot_eq, ":agent", ":agent_slot", 1),
						(agent_unequip_item, ":agent", ":item", reg0 +1),
					(else_try),#slot is free
						(agent_set_slot, ":agent", ":agent_slot", 1),
						(item_get_type, ":item_type", ":item"),
							
						(try_begin),#store ammo type and amount
							(this_or_next|eq, ":item_type", itp_type_arrows),
							(this_or_next|eq, ":item_type", itp_type_bolts),
							(this_or_next|eq, ":item_type", itp_type_thrown),
							(eq, ":item_type", itp_type_bullets),
							(agent_set_slot, ":agent", slot_agent_spawn_ammo_type, ":item"),
							(try_begin),
								(eq, ":item_type", itp_type_bolts),
								(assign, ":bolt_slot", reg0),
							(else_try),
								(eq, ":item_type", itp_type_bullets),
								(assign, ":bullet_slot", reg0),
							(try_end),
						(else_try),
							(eq, ":item_type", itp_type_crossbow),
							(val_add, ":reduce_bolts", 1),
							(assign, ":crossbow_item", ":item"),
						(else_try),
							(this_or_next|eq, ":item_type", itp_type_pistol),
							(eq, ":item_type", itp_type_musket),
							(val_add, ":reduce_bullets", 1),
							(try_begin),
								(eq, ":item_type", itp_type_pistol),
								(assign, ":pistol_item", ":item"),
							(else_try),
								(assign, ":musket_item", ":item"),
							(try_end),
						(else_try),#store shield hp
							(eq, ":item_type", itp_type_shield),
							(item_get_hit_points, ":shield_hp", ":item"),
							(try_begin),
								(eq, ":carry_slot", 3),#on back
								(agent_set_slot, ":agent", slot_agent_cur_shield_hp, ":shield_hp"),
							(else_try),
								(agent_set_slot, ":agent", slot_agent_cur_buckler_hp, ":shield_hp"),
							(try_end),
						(try_end),
						
						(try_begin),
							(eq, ":carry_slot", 0),#cant sheath
							(agent_set_wielded_item, ":agent", ":item"),
						(try_end),
							
						#add item weight
						(set_fixed_point_multiplier, 10),
						(item_get_weight, ":item_weight", ":item"),
						(set_fixed_point_multiplier, 100),
						(val_add, ":carry_weight", ":item_weight"),
					(try_end),
						
				(else_try),#armor slots
					(set_fixed_point_multiplier, 10),
					(item_get_weight, ":item_weight", ":item"),
					(set_fixed_point_multiplier, 100),
    ###
    # (assign, reg50, ":item_weight"),
		# (assign, reg51, ":item"),
    # (display_message, "@item weight = {reg50} item = {reg51}", 0xCCCCCC),
		###
					(val_add, ":carry_weight", ":item_weight"),
					(val_add, ":armor_weight", ":item_weight"),
        (try_end),
      (try_end),
			
			#init ammo, unload weapon
			(try_begin),
				(eq, ":bolt_slot", -1),
				(try_begin),
					(gt, ":crossbow_item", -1),
					(agent_set_ammo, ":agent", ":crossbow_item", 0),
				(try_end),
			(else_try),
				(gt, ":reduce_bolts", 0),
				(gt, ":crossbow_item", -1),
				(agent_get_item_slot, ":bolt_item", ":agent", ":bolt_slot"),
				(item_get_max_ammo, ":ammo", ":bolt_item"),
				(val_sub, ":ammo", ":reduce_bolts"),
				(agent_set_ammo, ":agent", ":bolt_item", ":ammo"),
			(try_end),
			
			(try_begin),
				(eq, ":bullet_slot", -1),
				(try_begin),
					(gt, ":musket_item", -1),
					(agent_set_ammo, ":agent", ":musket_item", 0),
				(try_end),
				(try_begin),
					(gt, ":pistol_item", -1),
					(agent_set_ammo, ":agent", ":pistol_item", 0),
				(try_end),
			(else_try),
				(gt, ":reduce_bullets", 0),
				(this_or_next|gt, ":musket_item", -1),
				(gt, ":pistol_item", -1),
				(agent_get_item_slot, ":bullet_item", ":agent", ":bullet_slot"),
				(item_get_max_ammo, ":ammo", ":bullet_item"),
				(val_sub, ":ammo", ":reduce_bullets"),
				(agent_set_ammo, ":agent", ":bullet_item", ":ammo"),
			(try_end),
			
      (try_begin),
        (gt, ":carry_weight", 0),
        (agent_set_slot, ":agent", slot_agent_carry_weight, ":carry_weight"),
      (try_end),
      (try_begin),
        (gt, ":armor_weight", 0),
    ###
    #(assign, reg50, ":armor_weight"),
    #(display_message, "@armor weight = {reg50}", 0xCCCCCC),
		###
        (agent_set_slot, ":agent", slot_agent_armor_weight, ":armor_weight"),
        (neg|agent_is_non_player, ":agent"),#only on player
        (store_div, ":armor_weight_kg", ":armor_weight", 10),#set to kg
        (gt, ":armor_weight_kg", weight_limit),
				
				(store_add, ":end_loop_weight", ":armor_weight_kg", 1),
				(try_for_range, ":cur_weight", weight_limit +1, ":end_loop_weight"),
					(store_sub, ":cur_wpf_reduction", ":cur_weight", 1),
					(val_div, ":cur_wpf_reduction", 10),
					(val_add, ":weight_lowers_wpf", ":cur_wpf_reduction"),
				(try_end),
    ###
    # (assign, reg50, ":weight_lowers_wpf"),
    # (display_message, "@lowers wpf = {reg50}", 0xCCCCCC),
		###
      (try_end),
		(try_end),

    #serverside only
		(try_begin),
      (multiplayer_is_server),
			(try_begin),
				(neg|agent_is_non_player, ":agent"),#only on player
				(agent_get_player_id, ":player_no", ":agent"),
				(player_is_active, ":player_no"),
				
				(try_begin),
					(agent_has_item_equipped, ":agent", itm_surgeon_kit),
					(agent_get_team, ":agent_team", ":agent"),
					(get_max_players, ":num_players"),
					(try_for_range, ":cur_player", 1, ":num_players"),
						(player_is_active, ":cur_player"),
						(neq, ":player_no", ":cur_player"),
						(player_get_agent_id, ":cur_agent", ":cur_player"),
						(agent_is_active, ":cur_agent"),
						(agent_is_alive, ":cur_agent"),
						(agent_get_team, ":cur_agent_team", ":cur_agent"),
						(eq, ":agent_team", ":cur_agent_team"),
						(agent_slot_ge, ":cur_agent", slot_agent_bleed_interval, 1),
						(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_agent_set_bleed_interval, ":cur_agent", 1),
					(try_end),
				(try_end),
				
				#store inventory on spawn for upkeep calculation
				(try_begin),
					(multiplayer_is_dedicated_server),
					(player_slot_eq, ":player_no", slot_player_use_troop, "trp_online_character"),
					(player_get_slot, ":spawn_count", ":player_no", slot_player_round_spawn_count),
					(val_add, ":spawn_count", 1),
					(player_set_slot, ":player_no", slot_player_round_spawn_count, ":spawn_count"),
					#(assign, reg1, ":spawn_count"),
					#(display_message, "@spawn count:{reg1}"),
					
					(store_mul, ":array_offset", ":player_no", 9),
					
					# (str_clear, s21),###########################
					(try_for_range, ":inv_slot", ek_item_0, ek_food),
						(try_begin),#fix
							(lt, ":inv_slot", ek_horse),
							(agent_get_item_slot, ":item", ":agent", ":inv_slot"),
						(else_try),
							(player_get_item_id, ":item", ":player_no", ":inv_slot"),
						(try_end),
						
						(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_item", ":array_offset"),
						(val_add, ":cur_array", ":inv_slot"),
						(troop_set_slot, ":cur_array", ":spawn_count", ":item"),
						
						# (assign, reg1, ":cur_array"),###########################
						# (try_begin),
							# (gt, ":item", -1),
							# (str_store_item_name, s30, ":item"),
						# (else_try),
							# (str_store_string, s30, "@-1"),
						# (try_end),
						# (str_store_string, s21, "@{s21},{s30} array:{reg1}"),
						
						(store_add, ":cur_array", "trp_player_0_life_inv_slot_0_usage", ":array_offset"),
						(val_add, ":cur_array", ":inv_slot"),
						(troop_set_slot, ":cur_array", ":spawn_count", 0),#reset usage
						#
						# (assign, reg1, ":inv_slot"),
						# (assign, reg2, ":item"),
						# (display_message, "@Slot:{reg1} Item:{reg2}"),
						#
					(try_end),
					
					# (str_store_player_username, s29, ":player_no"),###########################
					# (assign, reg0, ":player_no"),###########################
					# (assign, reg1, ":spawn_count"),###########################
					# (str_store_string, s20, "@{s29} ({reg1}) = SPAWN {reg1} ITEMS: {s21}"),
					# (server_add_message_to_log, s20),
					# (display_message, s20),
				(try_end),
				
				(store_mission_timer_a, ":cur_time"),
				(val_add, ":cur_time", 5),
				(troop_set_slot, "trp_team_next_order_time", ":player_no", ":cur_time"),
		
        (set_show_messages, 0),
        (team_give_order, ":player_no", grc_everyone, mordr_hold),
        (team_give_order, ":player_no", grc_archers, mordr_advance),
        (team_give_order, ":player_no", grc_cavalry, mordr_fall_back),
        (team_give_order, ":player_no", grc_cavalry, mordr_mount), #Mount cavalry...
        (set_show_messages, 1),
		
				(player_get_troop_id, ":player_troop", ":player_no"),
				(gt, ":player_troop", -1),
				
				(player_get_slot, ":used_troop", ":player_no", slot_player_use_troop),
				(try_begin),
					(eq, ":used_troop", "trp_online_character"),
					(troop_get_slot, ":used_troop", ":player_troop", slot_troop_online_troop_backup),
					(player_get_slot, ":next_build", ":player_no", slot_player_online_next_build),
					(player_set_slot, ":player_no", slot_player_online_cur_build, ":next_build"),
				(try_end),
					
				(player_set_slot, ":player_no", slot_player_last_used_troop, ":used_troop"),
		  
				(try_for_range, reg0, 0, 3),
					(try_begin),
						(eq, reg0, 0),
						(assign, ":range_begin", ca_strength),
						(assign, ":range_end", ca_intelligence),
						(assign, ":event_type", multiplayer_event_client_set_attribute),
					(else_try),
						(eq, reg0, 1),
						(assign, ":range_begin", wpt_one_handed_weapon),
						(assign, ":range_end", wpt_firearm + 1),
						(assign, ":event_type", multiplayer_event_client_set_proficiency),
					(else_try),
						(eq, reg0, 2),
						(assign, ":range_begin", "skl_horse_archery"),
						(assign, ":range_end", "skl_reserved_17"),
						(assign, ":event_type", multiplayer_event_client_set_skill),
					(try_end),
		
					(try_for_range, ":cur_type", ":range_begin", ":range_end"),
						(assign, ":continue", 0),
						(try_begin),
							(eq, reg0, 0),
							(assign, ":continue", 1),
							(store_attribute_level, ":cur_value", ":used_troop", ":cur_type"),
							(store_attribute_level, ":last_value", ":player_troop", ":cur_type"),
						(else_try),
							(eq, reg0, 1),
							(assign, ":continue", 1),
							(store_proficiency_level, ":cur_value", ":used_troop", ":cur_type"),
							(store_proficiency_level, ":last_value", ":player_troop", ":cur_type"),
							(val_sub, ":cur_value", ":weight_lowers_wpf"),
							(try_begin),
								(neg|is_between, ":used_troop", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),#already done for these
								(this_or_next|is_between, ":cur_type", wpt_archery, wpt_throwing),
								(eq, ":cur_type", wpt_firearm),
								(try_begin),
									(eq, ":cur_type", wpt_archery),
									(store_skill_level, ":skl_wpt", "skl_power_pull", ":used_troop"),
									(val_mul, ":skl_wpt", pp_archery_bonus),
								(else_try),
									(eq, ":cur_type", wpt_crossbow),
									(store_skill_level, ":skl_wpt", "skl_power_pull", ":used_troop"),
									(val_mul, ":skl_wpt", pp_crossbow_bonus),
								(else_try),
									(store_skill_level, ":skl_wpt", "skl_power_reload", ":used_troop"),
									(val_mul, ":skl_wpt", pr_firearm_bonus),
								(try_end),
								(val_add, ":cur_value", ":skl_wpt"),
							(try_end),
						(else_try),
							(this_or_next|is_between, ":cur_type", "skl_horse_archery", "skl_shield"),
							(this_or_next|is_between, ":cur_type", "skl_weapon_master", "skl_reserved_10"),
							(is_between, ":cur_type", "skl_power_throw", "skl_reserved_16"),
							(assign, ":continue", 1),
							(store_skill_level, ":cur_value", ":cur_type", ":used_troop"),
							(store_skill_level, ":last_value", ":cur_type", ":player_troop"),
						(try_end),
					
						(eq, ":continue", 1),
						(neq, ":cur_value", ":last_value"),
						(call_script, "script_multiplayer_set_troop_value", ":player_troop", ":event_type", ":cur_type", ":cur_value"),
					(try_end),
				(try_end),
			(try_end),
		(try_end),

		(try_begin),
			(neg|multiplayer_is_server),
			(assign, ":carry_weight", 0),
			(set_fixed_point_multiplier, 10),
			(try_for_range, reg0, 0, 8),
				(agent_get_item_slot, ":item", ":agent", reg0),
				(gt, ":item", -1),
				(item_get_weight, ":item_weight", ":item"),
				(val_add, ":carry_weight", ":item_weight"),
			(try_end),
			(set_fixed_point_multiplier, 100),
		(try_end),

		(agent_set_slot, ":agent", slot_agent_accuracy_modifier, 100),
		
		(assign, ":horse_speed_modifier", 100),
		(try_begin),
			(gt, ":carry_weight", 0),
			(store_div, ":horse_mod", ":carry_weight", weight_divider_run_horse),
			(val_sub, ":horse_speed_modifier", ":horse_mod"),
		(try_end),
		(agent_set_horse_speed_factor, ":agent", ":horse_speed_modifier"),

		#(call_script, "script_agent_walk", ":agent", walk_by_limiter),#force agent to walk on spawn
		(agent_set_slot, ":agent", slot_agent_use_scene_prop, -1),
		(agent_set_slot, ":agent", slot_agent_next_action_time, -1),
	  
		(agent_get_troop_id, ":agent_troop", ":agent"),
		(try_begin),
			(gt, ":agent_troop", -1),
			
			(store_skill_level, ":max_hp", "skl_ironflesh", ":agent_troop"),
			(val_mul, ":max_hp", 2),
			(val_add, ":max_hp", 100),
			(agent_set_max_hit_points, ":agent", ":max_hp", 1),
			
			(try_begin),
				(neg|troop_slot_ge, ":agent_troop", slot_troop_base_stamina, agent_base_stamina),
				(call_script, "script_set_base_stamina", ":agent_troop"),
			(try_end),
			(troop_get_slot, ":base_stamina", ":agent_troop", slot_troop_base_stamina),
			(agent_set_slot, ":agent", slot_agent_cur_stamina, ":base_stamina"),
			(agent_set_slot, ":agent", slot_agent_max_stamina, ":base_stamina"),
		(try_end),
  ]),

	# (ti_on_item_wielded, 0, 0,
	# [
 #    ###
 #    # (display_message, "@ti_on_item_wielded"),
	# 	###
 #    (this_or_next|multiplayer_is_server),
 #    (neg|game_in_multiplayer_mode),
 #  ],
 #  [
 #    (store_trigger_param_1, ":agent"),
 #    (store_trigger_param_2, ":item_no"),

	# 	# (try_begin),
	# 		# (multiplayer_is_server),
	# 		# (neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
 #      # (neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
	# 		# (eq, ":item_no", itm_surgeon_kit),
	# 		# (neg|agent_is_non_player, ":agent"),
	# 		# (agent_get_player_id, ":player", ":agent"),
	# 		# (player_is_active, ":player"),
	# 		# (agent_get_team, ":agent_team", ":agent"),
			
	# 		# (get_max_players, ":num_players"),
	# 		# (try_for_range, ":cur_player", 1, ":num_players"),
	# 			# (player_is_active, ":cur_player"),
	# 			# (neq, ":player", ":cur_player"),
	# 			# (player_get_agent_id, ":cur_agent", ":cur_player"),
	# 			# (agent_is_active, ":cur_agent"),
	# 			# (agent_is_alive, ":cur_agent"),
	# 			# (agent_get_team, ":cur_agent_team", ":cur_agent"),
	# 			# (eq, ":agent_team", ":cur_agent_team"),
	# 			# (agent_get_slot, ":bleed_interval", ":cur_agent", slot_agent_bleed_interval),
	# 			# (multiplayer_send_2_int_to_player, ":player", multiplayer_event_client_agent_set_bleed_interval, ":cur_agent", ":bleed_interval"),
	# 		# (try_end),
	# 	# (try_end),
		
	# 	(item_get_type, ":item_type", ":item_no"),
	# 	(this_or_next|is_between, ":item_type", itp_type_bow, itp_type_goods),
	# 	(this_or_next|eq, ":item_type", itp_type_pistol),
	# 	(eq, ":item_type", itp_type_musket),
		
	# 	(agent_get_horse, ":horse", ":agent"),
	# 	(try_begin),
	# 		(agent_is_active, ":horse"),
	# 		(item_get_food_quality, ":acc_mod", ":item_no"),
	# 	(else_try),
	# 		(assign, ":acc_mod", 100),
	# 	(try_end),
	# 	(try_begin),
	# 		(neg|agent_slot_eq, ":agent", slot_agent_accuracy_modifier, ":acc_mod"),
	# 		(agent_set_slot, ":agent", slot_agent_accuracy_modifier, ":acc_mod"),
	# 		(agent_set_accuracy_modifier, ":agent", ":acc_mod"),
	# 	(try_end),
		
	# 	# (assign, reg1, ":acc_mod"),
	# 	# (display_message, "@{reg1}"),
	# ]),
	
#item unwield
	(ti_on_item_unwielded, 0, 0,
  [
    ###
    # (display_message, "@ti_on_item_unwielded"),
		###
    (this_or_next|multiplayer_is_server),
    (neg|game_in_multiplayer_mode),
  ],
  [
		(store_trigger_param_1, ":agent"),
		(store_trigger_param_2, ":item_id"),

		(neg|agent_is_non_player, ":agent"),
		(item_slot_eq, ":item_id", slot_item_carry_slot, 0),#cant sheath

		(assign, ":end_slot", ek_head),
		(try_for_range, reg0, ek_item_0, ":end_slot"),
			(agent_get_item_slot, ":item", ":agent", reg0),
			(eq, ":item", ":item_id"),
			(assign, ":end_slot", -1),#break loop
				
			(item_get_type, ":item_type", ":item_id"),
			(try_begin),
				(this_or_next|is_between, ":item_type", itp_type_crossbow, itp_type_thrown +1),
				(is_between, ":item_type", itp_type_pistol, itp_type_musket +1),
				(agent_get_ammo_for_slot, ":cur_ammo", ":agent", reg0),
				(item_get_max_ammo, ":max_ammo", ":item_id"),
				(neq, ":cur_ammo", ":max_ammo"),
				(try_begin),
					(neq, ":item_type", itp_type_thrown),#fix
					(call_script, "script_agent_spawn_item", ":agent", ":item_id", 1),
				(try_end),
			(else_try),
				(call_script, "script_agent_spawn_item", ":agent", ":item_id", 0),
			(try_end),
			
			(agent_unequip_item, ":agent", ":item_id", reg0 +1),
			(agent_set_slot, ":agent", slot_agent_carry_slot_0_used, 0),
			(call_script, "script_agent_set_weight", ":agent", ":item_id", agent_sub_carry_weight),
		(try_end),
  ]),

#item pick up
	(ti_on_item_picked_up, 0, 0,
  [
    ###
    # (display_message, "@ti_on_item_picked_up"),
		###
    (this_or_next|multiplayer_is_server),
    (neg|game_in_multiplayer_mode),
  ],
  [
    (store_trigger_param_1, ":agent"),
    (store_trigger_param_2, ":item_id"),
		(store_trigger_param_3, ":instance_id"),

		(item_get_slot, ":item_id_carry_slot", ":item_id", slot_item_carry_slot),
		(store_add, ":agent_carry_slot", slot_agent_carry_slot_0_used, ":item_id_carry_slot"),

		(try_begin),
			(agent_slot_eq, ":agent", ":agent_carry_slot", 1),#slot filled
			(assign, ":item_replaced", -1),
			(assign, ":end_slot", ek_head),
			(try_for_range, reg0, ek_item_0, ":end_slot"),
				(agent_get_item_slot, ":item", ":agent", reg0),
				(gt, ":item", -1),
				(item_slot_eq, ":item", slot_item_carry_slot, ":item_id_carry_slot"),

				(assign, ":unequip_slot", -1),
				(try_begin),#item with same slot
					(eq, ":item_replaced", -1),
					(assign, ":equip_slot", reg0 +1),
					(assign, ":item_replaced", ":item"),
					
					(item_get_type, ":replace_item_type", ":item_replaced"),
					(try_begin),
						(this_or_next|is_between, ":replace_item_type", itp_type_arrows, itp_type_bolts +1),
						(this_or_next|is_between, ":replace_item_type", itp_type_crossbow, itp_type_thrown +1),
						(is_between, ":replace_item_type", itp_type_pistol, itp_type_bullets +1),
						(agent_get_ammo_for_slot, ":cur_ammo", ":agent", reg0),
						(item_get_max_ammo, ":max_ammo", ":item_replaced"),
						(neq, ":cur_ammo", ":max_ammo"),
						(assign, ":spawn_type", 1),
					(else_try),
						(assign, ":spawn_type", 0),
					(try_end),
				(else_try),#equipped item
					(assign, ":unequip_slot", reg0 +1),
					(item_get_type, ":item_id_type", ":item_id"),
					(try_begin),
						(this_or_next|is_between, ":item_id_type", itp_type_arrows, itp_type_bolts +1),
						(this_or_next|is_between, ":item_id_type", itp_type_crossbow, itp_type_thrown +1),
						(is_between, ":item_id_type", itp_type_pistol, itp_type_bullets +1),
						(agent_get_ammo_for_slot, ":cur_value", ":agent", reg0),
					(else_try),
						(eq, ":item_id_type", itp_type_shield),
						(scene_prop_get_hit_points, ":cur_value", ":instance_id"),
					(try_end),
					(assign, ":end_slot", -1),#break loop
				(try_end),
				# (assign, reg1, ":item"),
				# (display_message, "@slot {reg0} item {reg1}"),
			(try_end),
			
			(try_begin),
				(gt, ":unequip_slot", -1),# -1 is ammo refilled
				(call_script, "script_agent_spawn_item", ":agent", ":item_replaced", ":spawn_type"),
				(agent_unequip_item, ":agent", ":item_replaced", ":equip_slot"),
				(agent_unequip_item, ":agent", ":item_id", ":unequip_slot"),
				(agent_equip_item, ":agent", ":item_id", ":equip_slot"),#equip the item to this slot
				
				(call_script, "script_agent_set_weight", ":agent", ":item_replaced", agent_sub_carry_weight),
				(call_script, "script_agent_set_weight", ":agent", ":item_id", agent_add_carry_weight),
				
				(try_begin),
					(this_or_next|is_between, ":item_id_type", itp_type_arrows, itp_type_bolts +1),
					(this_or_next|is_between, ":item_id_type", itp_type_crossbow, itp_type_thrown +1),
					(is_between, ":item_id_type", itp_type_pistol, itp_type_bullets +1),
					(agent_set_ammo, ":agent", ":item_id", ":cur_value"),
				(else_try),
					(eq, ":item_id_type", itp_type_shield),
					(try_begin),
						(item_slot_eq, ":item_id", slot_item_carry_slot, 3),#on back
						(agent_set_slot, ":agent", slot_agent_cur_shield_hp, ":cur_value"),
					(else_try),
						(agent_set_slot, ":agent", slot_agent_cur_buckler_hp, ":cur_value"),
					(try_end),
				(try_end),
				
				(try_begin),
					(neq, ":item_id_type", itp_type_arrows),
					(neq, ":item_id_type", itp_type_bolts),
					(neq, ":item_id_type", itp_type_bullets),
					(agent_set_wielded_item, ":agent", ":item_id"),
				(try_end),
				
				(try_begin),#upkeep
					(multiplayer_is_dedicated_server),
					(this_or_next|is_between, ":replace_item_type", itp_type_arrows, itp_type_bolts +1),
					(this_or_next|eq, ":replace_item_type", itp_type_thrown),
					(this_or_next|eq, ":replace_item_type", itp_type_bullets),
					(eq, ":replace_item_type", itp_type_shield),
					(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":agent", ":item_replaced", 0),
				(try_end),
			(try_end),
			
		(else_try),
			(agent_set_slot, ":agent", ":agent_carry_slot", 1),
			(call_script, "script_agent_set_weight", ":agent", ":item_id", agent_add_carry_weight),
    (try_end),
  ]),

#item drop
	(ti_on_item_dropped, 0, 0,
  [
  ###
  # (display_message, "@ti_on_item_dropped"),
	###
  #(this_or_next|multiplayer_is_server),
  #(neg|game_in_multiplayer_mode),
  ],
  [
    (store_trigger_param_1, ":agent"),
    (store_trigger_param_2, ":item_id"),
		(store_trigger_param_3, ":dropped_prop"),
    
		(item_get_slot, ":carry_slot", ":item_id", slot_item_carry_slot),
		(store_add, ":agent_slot", slot_agent_carry_slot_0_used, ":carry_slot"),
		(agent_set_slot, ":agent", ":agent_slot", 0),
		(call_script, "script_agent_set_weight", ":agent", ":item_id", agent_sub_carry_weight),
		
		(try_begin),#upkeep
			(multiplayer_is_dedicated_server),
			(neg|agent_is_non_player, ":agent"),
			(item_get_type, ":item_type", ":item_id"),
			(this_or_next|is_between, ":item_type", itp_type_arrows, itp_type_bolts +1),
			(this_or_next|eq, ":item_type", itp_type_thrown),
			(this_or_next|eq, ":item_type", itp_type_bullets),
			(eq, ":item_type", itp_type_shield),
			(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":agent", ":item_id", 0),
		(try_end),
		
		(try_begin),
			(item_slot_eq, ":item_id", slot_item_carry_slot, 3),#on back
			(agent_get_slot, ":cur_hitpoints", ":agent", slot_agent_cur_shield_hp),
		(else_try),
			(agent_get_slot, ":cur_hitpoints", ":agent", slot_agent_cur_buckler_hp),
		(try_end),
		(try_begin),
			(eq, ":cur_hitpoints", 0),
			(item_get_hit_points, ":cur_hitpoints", ":item_id"),
		(try_end),
		(scene_prop_set_hit_points, ":dropped_prop", ":cur_hitpoints"),
		#(assign, reg1, ":cur_hitpoints"),
		#(display_message, "@drop hp {reg1}"),
		
		#deploy shield
		(agent_get_crouch_mode, ":crouch", ":agent"),
		(eq, ":crouch", 1),
		(this_or_next|is_between, ":item_id", itm_tab_shield_pavise_b, itm_tab_shield_pavise_c +1),
		(this_or_next|is_between, ":item_id", itm_tab_shield_pavise_b +loom_1_item, itm_tab_shield_pavise_c +loom_1_item +1),
		(this_or_next|is_between, ":item_id", itm_tab_shield_pavise_b +loom_2_item, itm_tab_shield_pavise_c +loom_2_item +1),
		(is_between, ":item_id", itm_tab_shield_pavise_b +loom_3_item, itm_tab_shield_pavise_c +loom_3_item +1),
			
		(set_fixed_point_multiplier, 100),
		(agent_get_position, pos_calc, ":agent"),
		(position_move_y, pos_calc, 70),
		(position_set_z_to_ground_level, pos_calc),
		(call_script, "script_get_angle_of_ground_at_pos", 0, 1, 25, 25, 15, 15),
	  
		(try_begin),
			(is_between, reg0, -30, 31),
			(is_between, reg1, -30, 31),
					
			(copy_position, pos_spawn, pos_calc),
			
			(position_rotate_x, pos_spawn, reg0),
			(position_rotate_y, pos_spawn, reg1),
			
			(assign, ":items_begin", itm_tab_shield_pavise_b),
			(assign, ":items_end", itm_tab_shield_pavise_c +1),
			(assign, ":props_begin", "spr_pavise_prop_b"),
			(assign, ":range_end", ek_head),
			(try_for_range, reg0, ek_item_0, ":range_end"),
				(try_begin),
					(eq, reg0, 1),
					(assign, ":items_begin", itm_tab_shield_pavise_b +loom_1_item),
					(assign, ":items_end", itm_tab_shield_pavise_c +loom_1_item +1),
					(assign, ":props_begin", "spr_pavise_prop_b_+1"),
				(else_try),
					(eq, reg0, 2),
					(assign, ":items_begin", itm_tab_shield_pavise_b +loom_2_item),
					(assign, ":items_end", itm_tab_shield_pavise_c +loom_2_item +1),
					(assign, ":props_begin", "spr_pavise_prop_b_+2"),
				(else_try),
					(eq, reg0, 3),
					(assign, ":items_begin", itm_tab_shield_pavise_b +loom_3_item),
					(assign, ":items_end", itm_tab_shield_pavise_c +loom_3_item +1),
					(assign, ":props_begin", "spr_pavise_prop_b_+3"),
				(try_end),
					
				(is_between, ":item_id", ":items_begin", ":items_end"),
				(store_sub, ":offset", ":item_id", ":items_begin"),
				(store_add, ":shield_prop", ":props_begin", ":offset"),
				(assign, ":range_end", -1), #break loop
			(try_end),
			
			(assign, "$agent_deployed_shield", ":agent"),
			(assign, "$next_spawn_prop_hp", ":cur_hitpoints"),
			(call_script, "script_spawn_new_or_get_free_scene_prop", ":shield_prop"),

			# (prop_instance_get_position, pos_move, ":dropped_prop"),
			# (set_fixed_point_multiplier, 1),#set to m
			# (position_set_z, pos_move, -100),
			# (prop_instance_set_position, ":dropped_prop", pos_move),
			(scene_prop_set_prune_time, ":dropped_prop", 0),
    (try_end),
  ]),
#SLOT SYSTEM end

	(ti_on_agent_mount, 0, 0,
  [
  ##
  # (display_message, "@ti_on_agent_mount"),
	##
  (this_or_next|multiplayer_is_server),
  (neg|game_in_multiplayer_mode),
  ],
  [
    (store_trigger_param_1, ":agent"),
    # (store_trigger_param_2, ":horse_agent"),
		
		(agent_get_wielded_item, ":item", ":agent", 0),
		(gt, ":item", -1),
		(item_get_type, ":item_type", ":item"),
		# (try_begin),
		# 	(this_or_next|is_between, ":item_type", itp_type_bow, itp_type_goods),
		# 	(this_or_next|eq, ":item_type", itp_type_pistol),
		# 	(eq, ":item_type", itp_type_musket),
		# 	(item_get_food_quality, ":acc_mod", ":item"),
		# 	(neg|agent_slot_eq, ":agent", slot_agent_accuracy_modifier, ":acc_mod"),
		# 	(agent_set_slot, ":agent", slot_agent_accuracy_modifier, ":acc_mod"),
		# 	(agent_set_accuracy_modifier, ":agent", ":acc_mod"),
		# (else_try),
			(is_between, ":item_type", itp_type_one_handed_wpn, itp_type_arrows),
			(neg|item_has_capability, ":item", itc_horseback),
			# (call_script, "script_workaround_agent_attack_action", ":agent"),#doesnt work here
			# (eq, reg0, 1),#readying_attack
			(agent_unequip_item, ":agent", ":item", 0),
			(agent_equip_item, ":agent", ":item", 0),
			(agent_set_wielded_item, ":agent", ":item"),
		# (try_end),
		# (assign, reg1, ":acc_mod"),
		# (display_message, "@{reg1}"),
  ]),

	(ti_on_agent_dismount, 0, 0,
  [
  ##
  # (display_message, "@ti_on_agent_dismount"),
	##
  (multiplayer_is_server),
  ],
  [
    (store_trigger_param_1, ":agent"),
    (store_trigger_param_2, ":horse_agent"),
		
		# (agent_get_wielded_item, ":item", ":agent", 0),
		# (gt, ":item", -1),
		# (item_get_type, ":item_type", ":item"),
		# (try_begin),
		# 	(this_or_next|is_between, ":item_type", itp_type_bow, itp_type_goods),
		# 	(this_or_next|eq, ":item_type", itp_type_pistol),
		# 	(eq, ":item_type", itp_type_musket),
		# 	(neg|agent_slot_eq, ":agent", slot_agent_accuracy_modifier, 100),
		# 	(agent_set_slot, ":agent", slot_agent_accuracy_modifier, 100),
		# 	(agent_set_accuracy_modifier, ":agent", 100),
		# 	# (display_message, "@100"),
		# (try_end),
		
		#upkeep
		# (try_begin),
			(multiplayer_is_dedicated_server),
			(neg|agent_is_non_player, ":agent"),
			(agent_get_item_id, ":item_id", ":horse_agent"),
			(store_agent_hit_points, ":cur_usage", ":horse_agent", 1),
			(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":agent", ":item_id", ":cur_usage"),
		# (try_end),
  ]),

	(0, 0.2, 0.5,
  [
    (neg|multiplayer_is_dedicated_server),
		# (this_or_next|game_key_released, gk_attack),#WSE
		(this_or_next|game_key_clicked, gk_attack),
		#REWORK
    (this_or_next|game_key_clicked, gk_defend),
    (this_or_next|game_key_clicked, gk_jump),
    (this_or_next|game_key_clicked, gk_kick),
    (game_key_clicked, gk_crouch),

		(call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (agent_is_active, ":player_agent"),
    (agent_is_alive, ":player_agent"),
    (agent_slot_eq, ":player_agent", slot_agent_use_scene_prop, -1),
	
		(assign, ":continue", 0),#avoid spaming
    (try_begin),
      # (game_key_released, gk_attack),#WSE
			#REWORK
			(game_key_clicked, gk_attack),
      (neg|game_key_is_down, gk_defend),
      #(agent_get_attack_action, ":attack_action", ":player_agent"),
			(call_script, "script_workaround_agent_attack_action", ":player_agent"),
			(try_begin),
        (neq, reg0, 2),#releasing_attack
        (assign, reg39, stamina_attack),
				(assign, ":continue", 1),
			(try_end),
		(else_try),
      (game_key_clicked, gk_defend),
      (neg|game_key_is_down, gk_attack),
      (agent_get_defend_action, ":defend_action", ":player_agent"),
			(try_begin),
				(neq, ":defend_action", 1),#parrying
        (neq, ":defend_action", 2),#blocking
				(agent_get_wielded_item, ":item_r", ":player_agent", 0),
				(try_begin),
					(gt, ":item_r", -1),
					(item_get_type, ":item_r_type", ":item_r"),
					(try_begin),
						(is_between, ":item_r_type", itp_type_one_handed_wpn, itp_type_arrows),
						(neg|item_has_property, ":item_r", itp_no_parry),
						(assign, ":continue", 1),
					(try_end),
				(else_try),
					(assign, ":continue", 1),
				(try_end),
				(eq, ":continue", 1),
        (assign, reg39, stamina_defend),
			(try_end),
    (else_try),
			(agent_get_animation, ":anim", ":player_agent", 0),
			(try_begin),
        (game_key_clicked, gk_jump),
        (neq, ":anim", "anim_jump"),
        (neq, ":anim", "anim_ride_jump"),
				(assign, reg39, stamina_jump),
        (assign, ":continue", 1),
      (else_try),
        (game_key_clicked, gk_kick),
        (neq, ":anim", "anim_kick_right_leg"),
				(assign, reg39, stamina_kick),
        (assign, ":continue", 1),
      (else_try),
	    (game_key_clicked, gk_crouch),
        (neq, ":anim", "anim_crouch_to_stand"),
        (assign, reg39, stamina_crouch),
				(assign, ":continue", 1),
			(try_end),
    (try_end),
		(eq, ":continue", 1),
  ],
  [
		(call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (assign, ":continue", 0),
    (try_begin),
      (eq, reg39, stamina_attack),
      #(agent_get_attack_action, ":attack_action", ":player_agent"),
			(call_script, "script_workaround_agent_attack_action", ":player_agent"),
			(try_begin),
				(this_or_next|eq, reg0, 1),#readying_attack #only works on long holding
        (eq, reg0, 2),#releasing_attack
        (assign, ":continue", 1),
			(try_end),
		(else_try),
      (eq, reg39, stamina_defend),
      #(agent_get_defend_action, ":defend_action", ":player_agent"),
			#(try_begin),
        #(this_or_next|eq, ":defend_action", 1),#parrying
        #(eq, ":defend_action", 2),#blocking #only works on long holding
      (assign, ":continue", 1),
	  #(try_end),
    (else_try),
      (agent_get_animation, ":anim", ":player_agent", 0),
      (try_begin),
        (eq, reg39, stamina_jump),
        (this_or_next|eq, ":anim", "anim_jump"),
        (eq, ":anim", "anim_ride_jump"),
        (assign, ":continue", 1),
      (else_try),
        (eq, reg39, stamina_kick),
        (eq, ":anim", "anim_kick_right_leg"),
        (assign, ":continue", 1),
      (else_try),
        (eq, reg39, stamina_crouch),
        (eq, ":anim", "anim_crouch_to_stand"),
        (assign, ":continue", 1),
      (try_end),
    (try_end),

    (eq, ":continue", 1),
    (call_script, "script_agent_lose_stamina", ":player_agent", reg39),
  ]),

#additional damage system
	(ti_on_agent_hit, 0, 0, [],
	[
		(store_trigger_param, ":victim_agent", 1),
		(store_trigger_param, ":dealer_agent", 2),
		(store_trigger_param, ":raw_damage", 3),
		(store_trigger_param, ":hit_bone", 4),
		(store_trigger_param, ":missile", 5),
		# (store_trigger_param, ":raw_damage", 6),#WSE
		(assign, ":weapon", reg0),

		# (assign, reg0, ":raw_damage"),
		# (display_message, "@dmg:{reg0}"),

		(try_begin),
			(gt, ":weapon", -1),
			(item_get_type, ":weapon_type", ":weapon"),
		(else_try),
			(assign, ":weapon_type", -1),
		(try_end),

		#reverse bone damage
		(assign, ":reverse", 0),
		(try_begin),
			(agent_is_human, ":victim_agent"),
			(try_begin),
				(eq, ":hit_bone", hb_head),
				(assign, ":reverse", 1),
			# (else_try),
			# 	(is_between, ":hit_bone", hb_thigh_l, hb_foot_r +1),
			# 	(assign, ":reverse", 2),
			(try_end),
		(else_try),
			(eq, ":hit_bone", hrsb_head),
			(assign, ":reverse", 1),
		(try_end),

		(try_begin),
			(neq, ":reverse", 0),
			(val_mul, ":raw_damage", 100),
			(try_begin),
				(eq, ":reverse", 1),
				(try_begin),
					(neg|is_between, ":weapon_type", itp_type_one_handed_wpn, itp_type_polearm +1),
					(val_div, ":raw_damage", 175),
					(val_mul, ":raw_damage", 100),
				(try_end),
				(val_div, ":raw_damage", 120),
			(else_try),
				(val_div, ":raw_damage", 90),
			(try_end),
		(try_end),

		# #reverse missile speed factor
		# (try_begin),
		# 	(neq, ":weapon_type", -1),
		# 	(neq, ":weapon_type", itp_type_shield),
		# 	(neg|is_between, ":weapon_type", itp_type_one_handed_wpn, itp_type_polearm +1),
		# 	(item_get_missile_speed, ":v0", ":weapon"),
		# 	(val_mul, ":v0", 1200),
		# 	(val_div, ":v0", 238),#max calc value without clipping over 500 with headshot bonus
		# 	(val_mul, ":raw_damage", ":v0"),
		# 	(val_div, ":raw_damage", 100),#bring into 0.1 scale
		# (try_end),

		# (assign, reg0, ":raw_damage"),
		# (assign, reg2, ":hit_bone"),
		# (assign, reg3, ":weapon"),
		# (store_mission_timer_a_msec, reg4),
		# (display_message, "@dmg:{reg0} bone:{reg2} weapon:{reg3} time:{reg4}"),

		(assign, ":no_calc", 0),

		# (try_begin),#charge damage to horse
		# 	(neq, ":victim_agent", ":dealer_agent"),
		# 	(neg|agent_is_human, ":dealer_agent"),
		# 	(ge, ":momentum", 4),
		# 	(store_div, ":charge_damage", ":momentum", 4),
		# 	(agent_deliver_damage_to_agent, ":dealer_agent", ":dealer_agent", ":charge_damage"),
		# (try_end),
		
		(try_begin),#fall damage
			(neg|agent_is_human, ":victim_agent"),
			(agent_get_rider, ":victim_rider", ":victim_agent"),
			(eq, ":victim_rider", ":dealer_agent"),
			(assign, ":fall_damage", 1),
		(else_try),
			(eq, ":dealer_agent", ":victim_agent"),
			(assign, ":fall_damage", 1),
		(else_try),
			(assign, ":fall_damage", 0),
		(try_end),
		(try_begin),
			(eq, ":fall_damage", 1),
			# (display_message, "@fall damage"),
			(assign, ":no_calc", 1),
			(agent_get_position, pos1, ":victim_agent"),
			(set_fixed_point_multiplier, 1),#set to m
			(position_get_z, ":cur_z", pos1),
			(set_fixed_point_multiplier, 100),
			(try_begin),#under water
				(lt, ":cur_z", 0),
				(store_mul, ":sub_damage", ":cur_z", ":cur_z"),
				(val_mul, ":sub_damage", 2),
				(try_begin),
					(ge, ":sub_damage", ":raw_damage"),
					(assign, ":raw_damage", 0),
				(else_try),
					(val_sub, ":raw_damage", ":sub_damage"),
				(try_end),
			(try_end),
    		
    		#fall damage for rider
			(ge, ":raw_damage", 4),
			(neg|agent_is_human, ":victim_agent"),#horse
			(store_div, ":rider_damage", ":raw_damage", 4),#25%
			(agent_deliver_damage_to_agent, ":dealer_agent", ":dealer_agent", ":rider_damage", itm_animal_to_human),
		(try_end),	

		(assign, ":surgeon_score", 0),
		(try_begin),#medi pack
      		(eq, ":weapon", itm_surgeon_kit),
			(assign, ":no_calc", 1),
			
			(assign, ":raw_damage", 0),
			(store_agent_hit_points, ":agent_hp", ":victim_agent", 0),
			# (agent_get_slot, ":hp_lost", ":victim_agent", slot_agent_bleed_hp_lost),
			# (assign, reg1, ":hp_lost"),
			# (display_message, "@hp lost: {reg1}"),
			(try_begin),
				(neq, ":agent_hp", 100),
				(assign, ":surgeon_score", score_bonus_surgeon),
				(agent_set_slot, ":victim_agent", slot_agent_bleed_hp_lost, 0),#reset
				(val_add, ":agent_hp", score_bonus_surgeon),
				(agent_set_hit_points, ":victim_agent", ":agent_hp", 0),
				(try_begin),#stop bleeding
					(agent_slot_ge, ":victim_agent", slot_agent_bleed_time, 1),
					(agent_set_slot, ":victim_agent", slot_agent_bleed_time, 0),
					(call_script, "script_server_set_bleed_interval", ":victim_agent", 0),
				(try_end),
			(else_try),#do not loose a dressing if not bleeding
				(agent_is_active, ":dealer_agent"),
				(agent_is_alive, ":dealer_agent"),
				(assign, ":end_item_slot", ek_head),
				(try_for_range, ":cur_item_slot", ek_item_0, ":end_item_slot"),
					(agent_get_item_slot, ":cur_item", ":dealer_agent", ":cur_item_slot"),
					(eq, ":cur_item", itm_surgeon_kit),
					(agent_get_ammo_for_slot, ":ammo", ":dealer_agent", ":cur_item_slot"),
					(item_get_max_ammo, ":max_ammo", ":cur_item"),
					(try_begin),
						(lt, ":ammo", ":max_ammo"),
						(val_add, ":ammo", 1),
						(agent_set_ammo, ":dealer_agent", ":cur_item", ":ammo"),
					(try_end),
					(assign, ":end_item_slot", -1),#break loop
				(try_end),
			(try_end),
		(try_end),
		
		(try_begin),
			(eq, ":no_calc", 1),
			(assign, ":damage", ":raw_damage"),
		(else_try),
			(try_begin),
				(agent_is_human, ":victim_agent"),
				(assign, ":armor", 0),
				(try_begin),
					(eq, ":hit_bone", hb_head),
					(agent_get_item_slot, ":cur_item", ":victim_agent", ek_head),
					(try_begin),
						(gt, ":cur_item", -1),
						(item_get_head_armor, ":armor", ":cur_item"),
					(try_end),
				(else_try),
					(is_between, ":hit_bone", hb_thigh_l, hb_spine),
					(agent_get_item_slot, ":cur_item", ":victim_agent", ek_foot),
					(try_begin),
						(gt, ":cur_item", -1),
						(item_get_leg_armor, ":armor", ":cur_item"),
					(try_end),
				(else_try),
					(agent_get_item_slot, ":cur_item", ":victim_agent", ek_body),
					(try_begin),
						(gt, ":cur_item", -1),
						(item_get_body_armor, ":armor", ":cur_item"),
					(try_end),
					(agent_get_item_slot, ":cur_item", ":victim_agent", ek_gloves),
					(try_begin),
						(gt, ":cur_item", -1),
						(item_get_body_armor, ":value", ":cur_item"),
						(val_add, ":armor", ":value"),
					(try_end),
				(try_end),
			(else_try),
				(agent_get_item_id, ":cur_item", ":victim_agent"),
				(item_get_body_armor, ":armor", ":cur_item"),
			(try_end),
			
			(copy_position, pos_hit, pos0),
			(call_script, "script_calc_attack_penetration", ":weapon", ":raw_damage", ":armor", ":dealer_agent", ":missile"),
			(assign, ":damage", reg0),

			(try_begin),
				(agent_is_human, ":victim_agent"),
				(try_begin),
					(eq, ":hit_bone", hb_head),
					(val_mul, ":damage", 2),
				(else_try),
					(neg|this_or_next|eq, ":hit_bone", hb_abdomen),
					(neg|is_between, ":hit_bone", hb_spine, hb_thorax),
					(val_div, ":damage", 2),
				(try_end),
			(else_try),
				(try_begin),
					(eq, ":hit_bone", hrsb_head),
					(val_mul, ":damage", 2),
				(else_try),
					(neg|is_between, ":hit_bone", hrsb_pelvis, hrsb_neck_3 +1),
					(val_div, ":damage", 2),
				(try_end),
			(try_end),

			# (try_begin),
			# 	(agent_is_human, ":victim_agent"),
			# 	(try_begin),
			# 		(eq, ":hit_bone", hb_head),
			# 		(store_mul, ":chance", ":damage", 3),
			# 	(else_try),
			# 		(this_or_next|is_between, ":hit_bone", hb_thigh_l, hb_foot_l),
			# 		(this_or_next|is_between, ":hit_bone", hb_thigh_r, hb_foot_r),
			# 		(is_between, ":hit_bone", hb_shoulder_l, hb_item_r +1),
			# 		(assign, ":chance", ":damage"),
			# 	(else_try),
			# 		(store_mul, ":chance", ":damage", 2),
			# 	(try_end),
			# (else_try),
			# 	(try_begin),
			# 		(eq, ":hit_bone", hrsb_head),
			# 		(store_mul, ":chance", ":damage", 3),
			# 	(else_try),
			# 		(is_between, ":hit_bone", hrsb_l_clavicle, hrsb_tail_2 +1),
			# 		(assign, ":chance", ":damage"),
			# 	(else_try),
			# 		(store_mul, ":chance", ":damage", 2),
			# 	(try_end),
			# (try_end),

			# #bleeding
			# (store_random_in_range, ":rand", 0, 101),
			# (try_begin),
			# 	(lt, ":rand", ":chance"),
			# 	(agent_set_slot, ":victim_agent", slot_agent_last_attacker_agent, ":dealer_agent"),
			# 	(agent_get_slot, ":bleed_interval", ":victim_agent", slot_agent_bleed_interval),
			# 	(try_begin),#first time
			# 		(eq, ":bleed_interval", 0),
			# 		(assign, ":bleed_interval", 30),
			# 	(try_end),
			# 	(val_sub, ":bleed_interval", 1),
			# 	(val_max, ":bleed_interval", 1),
			# 	(call_script, "script_server_set_bleed_interval", ":victim_agent", ":bleed_interval"),
				
			# 	(store_mission_timer_a, ":bleed_time"),
			# 	(val_add, ":bleed_time", ":bleed_interval"),
			# 	(agent_set_slot, ":victim_agent", slot_agent_bleed_time, ":bleed_time"),
			# (try_end),
		(try_end),
    	
		
    	(try_begin),
			(gt, ":damage", 0),
			(try_begin),#apply sound to living horses
				(neg|agent_is_human, ":victim_agent"),#horse
				(store_agent_hit_points, ":hit_points", ":victim_agent", 1),
				(try_begin),
					(gt, ":hit_points", ":damage"),
					(agent_play_sound, ":victim_agent", "snd_horse_low_whinny"),
				(try_end),
			(else_try),#if agent gets hit.. stop all scene prop actions and presentations
				(agent_get_slot, ":instance_id", ":victim_agent", slot_agent_use_scene_prop),
				(prop_instance_is_valid, ":instance_id"),
				(call_script, "script_control_scene_prop", ":instance_id", prop_action_2),
			
				(multiplayer_is_server),
				(get_max_players, ":num_players"),
				(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
					(player_is_active, ":cur_player_no"),
					(multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_control_scene_prop, ":instance_id", prop_action_2), #CANCEL#
				(try_end),
			(try_end),
    	(try_end),

    	(try_begin),
			(multiplayer_is_server),
		
			#upkeep
			(try_begin),
				(multiplayer_is_dedicated_server),
				(this_or_next|neg|agent_is_non_player, ":victim_agent"),
				(neg|agent_is_non_player, ":dealer_agent"),
				(gt, ":damage", 0),
				# (assign, reg1, ":hit_bone"),
				# (assign, reg3, ":damage"),
				# (display_message, "@bone:{reg1} final dmg:{reg3}"),
				(try_begin),
					(gt, ":cur_item", -1),
					(neg|agent_is_non_player, ":victim_agent"),
					(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":victim_agent", ":cur_item", ":damage"),
				(try_end),
				(try_begin),
					(neg|agent_is_non_player, ":dealer_agent"),
					(this_or_next|eq, ":weapon_type", itp_type_one_handed_wpn),
					(this_or_next|eq, ":weapon_type", itp_type_two_handed_wpn),
					(eq, ":weapon_type", itp_type_polearm),
					(call_script, "script_multiplayer_server_player_set_cur_item_usage", ":dealer_agent", ":weapon", ":damage"),
				(try_end),
			(try_end),
			
			#score
			(try_begin),
				(this_or_next|gt, ":damage", 0),
				(gt, ":surgeon_score", 0),
				(assign, ":victim_player", -1),
				(assign, ":dealer_player", -1),
				(assign, ":victim_team", -1),
				(assign, ":dealer_team", -1),
				(try_begin),
					(agent_is_human, ":victim_agent"),
					(agent_get_team, ":victim_team", ":victim_agent"),
					(try_begin),
						(neg|agent_is_non_player, ":victim_agent"),
						(agent_get_player_id, ":victim_player", ":victim_agent"),
					(try_end),
				(else_try),
					(agent_get_rider, ":victim_rider", ":victim_agent"),
					(agent_is_active, ":victim_rider"),
					(agent_is_alive, ":victim_rider"),
					(agent_get_team, ":victim_team", ":victim_rider"),
					(neg|agent_is_non_player, ":victim_rider"),
					(agent_get_player_id, ":victim_player", ":victim_rider"),
				(try_end),
				
				(try_begin),
					(agent_is_human, ":dealer_agent"),
					(agent_get_team, ":dealer_team", ":dealer_agent"),
					(try_begin),
						(neg|agent_is_non_player, ":dealer_agent"),
						(agent_get_player_id, ":dealer_player", ":dealer_agent"),
					(try_end),
				(else_try),
					(agent_get_rider, ":dealer_rider", ":dealer_agent"),
					(agent_is_active, ":dealer_rider"),
					(agent_is_alive, ":dealer_rider"),
					(agent_get_team, ":dealer_team", ":dealer_rider"),
					(neg|agent_is_non_player, ":dealer_rider"),
					(agent_get_player_id, ":dealer_player", ":dealer_rider"),
				(try_end),
				
				(assign, ":leader_player", -1),
				(try_begin),
					(agent_is_non_player, ":dealer_agent"),
					(agent_get_group, ":leader_player", ":dealer_agent"),
				(try_end),
				
				(try_begin),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
					
					(neq, ":victim_agent", ":dealer_agent"),
					(neq, ":victim_team", -1),#no horse without rider
					(this_or_next|neq, ":victim_player", ":dealer_player"),
					(neq, ":victim_player", ":leader_player"),
					(this_or_next|player_is_active, ":dealer_player"),
					(player_is_active, ":leader_player"),
					
					(store_agent_hit_points, ":damage_score", ":victim_agent", 1),
					(try_begin),
						(lt, ":damage", ":damage_score"),
						(assign, ":damage_score", ":damage"),
					(try_end),
					(val_add, ":damage_score", ":armor"),
					
					(assign, ":score_mod", 0),
					(try_begin),
						(eq, ":victim_team", ":dealer_team"),#same team sub score
						(val_sub, ":score_mod", ":damage_score"),
						(val_add, ":score_mod", ":surgeon_score"),
					(else_try),
						(val_add, ":score_mod", ":damage_score"),
					(try_end),
					
					(try_begin),#is player
						(player_is_active, ":dealer_player"),
						(assign, ":leader_player", ":dealer_player"),
						(try_begin),#less score on bots
							(agent_is_non_player, ":victim_agent"),
							(val_div, ":score_mod", score_victim_bot_divider),
						(try_end),
					(else_try),#is bot
						(val_div, ":score_mod", score_dealer_bot_divider),
						(gt, ":score_mod", 0),
						(player_get_slot, ":bot_count", ":leader_player", slot_player_last_bot_count),
						(try_begin),
							(gt, ":bot_count", 0),
							(val_div, ":score_mod", ":bot_count"),
							(val_max, ":score_mod", 1),
						(else_try),
							(assign, ":score_mod", 0),
						(try_end),
					(try_end),
					
					(neq, ":score_mod", 0),
					(player_get_slot, ":player_score", ":leader_player", slot_player_score),
					(val_add, ":player_score", ":score_mod"),
					(player_set_slot, ":leader_player", slot_player_score, ":player_score"),
					
					(get_max_players, ":num_players"),
					(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
						(player_is_active, ":player_no"),
						(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_score_count, ":leader_player", ":player_score"),
					(try_end),
				(try_end),

				(gt, ":damage", 0),
				#team attacking
				(try_begin),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
					
					(neg|agent_is_non_player, ":dealer_agent"),
					(neq, ":dealer_agent", ":victim_agent"),
					(agent_get_horse, ":dealer_horse", ":dealer_agent"),
					(neq, ":dealer_horse", ":victim_agent"),
					(eq, ":dealer_team", ":victim_team"),
					
					(neg|player_is_admin, ":dealer_player"),
					(player_get_unique_id, ":unique_id", ":dealer_player"),
					(store_div, ":offset", ":unique_id", 1048576),
					(store_mul, ":id_sub", ":offset", 1048576),
					(val_sub, ":unique_id", ":id_sub"),
					
					(store_add, ":array_hits", "trp_unquie_id_team_hits_0", ":offset"),
					(troop_get_slot, ":cur_team_hits", ":array_hits", ":unique_id"),
					(val_add, ":cur_team_hits", 1),
					(troop_set_slot, ":array_hits", ":unique_id", ":cur_team_hits"),
					
					(store_add, ":array_kicks", "trp_unquie_id_team_hit_kicks_0", ":offset"),
					(troop_get_slot, ":cur_hit_kicks", ":array_kicks", ":unique_id"),
					
					(assign, reg1, ":cur_team_hits"),
					(assign, reg2, "$g_team_hits_til_kick"),
					(assign, reg3, ":cur_hit_kicks"),
					(assign, reg4, "$g_team_hit_kicks_til_ban"),
					(str_store_string, s0, "@Watch your hits! Team hits until kick ({reg1}/{reg2}). Kicks until ban ({reg3}/{reg4})."),
					(str_store_string, s0, "str_server_s0"),
					(call_script, "script_multiplayer_send_message_to_player", ":dealer_player", color_server_message),
					(try_begin),
						(ge, ":cur_team_hits", "$g_team_hits_til_kick"),
						(val_add, ":cur_hit_kicks", 1),
						(str_store_player_username, s1, ":dealer_player"),
						(try_begin),
							(ge, ":cur_hit_kicks", "$g_team_hit_kicks_til_ban"),
							(ban_player, ":dealer_player", 1, 0),
							(str_store_string, s0, "@{s1} has been banned for excessive team hitting."),
						(else_try),
							(kick_player, ":dealer_player"),
							(str_store_string, s0, "@{s1} has been kicked for excessive team hitting."),
						(try_end),
						(troop_set_slot, ":array_kicks", ":unique_id", 0),#reset
						(troop_set_slot, ":array_hits", ":unique_id", 0),#reset
						
						(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
						(server_add_message_to_log, s0),
					(try_end),
				(try_end),
				
				#send damage report to victim player
				(assign, reg0, ":damage"),
				(try_begin),
					(player_is_active, ":victim_player"),
					(player_slot_eq, ":victim_player", slot_player_show_damage_report, 1),
					(try_begin),#receive damage
						(agent_is_human, ":victim_agent"),
						(neg|agent_is_non_player, ":victim_agent"),
						(str_store_string, s0, "str_received_damage"),
					(else_try),#horse receive damage
						(agent_get_rider, ":victim_rider", ":victim_agent"),
						(gt, ":victim_rider", -1),
						(neg|agent_is_non_player, ":victim_rider"),
						(str_store_string, s0, "str_horse_received_damage"),
					(try_end),
					(call_script, "script_multiplayer_send_message_to_player", ":victim_player", color_victim_damage),
				(try_end),

				#send damage report to dealer player
				(try_begin),
					(player_is_active, ":dealer_player"),
					(player_slot_eq, ":dealer_player", slot_player_show_damage_report, 1),
					(neq, ":dealer_agent", ":victim_agent"),
					(agent_get_horse, ":dealer_horse", ":dealer_agent"),
					(neq, ":dealer_horse", ":victim_agent"),
					(try_begin),
						(agent_is_human, ":dealer_agent"),
						(neg|agent_is_non_player, ":dealer_agent"),
						(try_begin),#human
							(agent_is_human, ":victim_agent"),
							(str_store_string, s0, "str_delivered_damage_to_player"),
						(else_try),#horse
							(str_store_string, s0, "str_delivered_damage_to_horse"),
						(try_end),
					(else_try),#horse deliver damage
						(agent_get_rider, ":dealer_rider", ":dealer_agent"),
						(gt, ":dealer_rider", -1),
						(neg|agent_is_non_player, ":dealer_rider"),
						(str_store_string, s0, "str_horse_delivered_damage"),
					(try_end),
					(call_script, "script_multiplayer_send_message_to_player", ":dealer_player", color_dealer_damage),
				(try_end),
			(try_end),
		(try_end),

		#set final trigger damage
		(set_trigger_result, ":damage"),
  ]),
	
	(ti_on_agent_killed_or_wounded, 0, 0,
	[
		(multiplayer_is_server),
	],
  [
  	# (try_begin),
  	# 	(eq, "$g_disable_hud", 0),
  	# 	(assign, "$g_disable_hud", 1),
  	# 	(set_show_messages, 0),
  	# (else_try),
  	# 	(assign, "$g_disable_hud", 0),
  	# 	(set_show_messages, 1),
  	# (try_end),


    (store_trigger_param_1, ":dead_agent_no"),
		#(store_trigger_param_2, ":killer_agent_no"),
		#(store_trigger_param_3, ":is_wounded"),
		(try_begin),
			(neg|agent_is_non_player, ":dead_agent_no"),
			(agent_get_player_id, ":player_no", ":dead_agent_no"),
			(player_is_active, ":player_no"),
			(call_script, "script_multiplayer_server_player_set_final_item_usage", ":player_no"),
		(try_end),
  ]),

	(0, 2, ti_once, 
  [
    (neg|game_in_multiplayer_mode),
  ],
  [
    #(team_get_faction, ":faction_0", 0),
	#(team_get_faction, ":faction_1", 1),
	
    (assign, ":cur_pos", pos58),
		(assign, ":end", 8),
		(try_for_range, ":end_team", 0, ":end"),
			(num_active_teams_le, ":end_team"),
			(assign, ":end", -1),#break loop
		(try_end),
    (try_for_range, ":cur_team", 0, ":end_team"),
      #(set_show_messages, 0),
      (team_give_order, ":cur_team", grc_everyone, mordr_hold),
      (team_give_order, ":cur_team", grc_archers, mordr_advance),
      (team_give_order, ":cur_team", grc_cavalry, mordr_fall_back),
      (team_give_order, ":cur_team", grc_cavalry, mordr_mount), #Mount cavalry...
      #(set_show_messages, 1),
			(team_get_order_position, ":cur_pos", ":cur_team", grc_everyone),
			(val_add, ":cur_pos", 1),
		(try_end),
		# (get_distance_between_positions_in_meters, ":dist", pos58, pos59),
		# (assign, reg1, ":dist"),
		# (display_message, "@distance {reg1}"),
		#(team_set_order_position, <team_no>, <division>, <position>),
  ]),

#timer_1
	(0.1, 0, 0, [],
  [ 
		(store_mission_timer_a_msec, ":cur_time"),

			(set_fixed_point_multiplier, 100),
			
			#missile flight
			(scene_prop_get_num_instances, ":num_instances", "spr_round_shot_missile"),
			(try_for_range, ":cur_instance", 0, ":num_instances"),
				(scene_prop_get_instance, ":missile_instance_id", "spr_round_shot_missile", ":cur_instance"),
				(scene_prop_slot_eq, ":missile_instance_id", scene_prop_is_used, 1),#used

				(prop_instance_is_animating, ":animating", ":missile_instance_id"),
				(try_begin),#get target position if moving
					(eq, ":animating", 1),
					(prop_instance_get_animation_target_position, pos_move, ":missile_instance_id"),
				(else_try),
					(prop_instance_get_position, pos_move, ":missile_instance_id"),
				(try_end),
				
				(position_get_x, ":missile_pos_x", pos_move),
				(position_get_y, ":missile_pos_y", pos_move),

				(try_begin),
					(this_or_next|lt, ":missile_pos_x", 0),
					(this_or_next|gt, ":missile_pos_x", "$g_scene_max_x"),
					(this_or_next|lt, ":missile_pos_y", 0),
					(gt, ":missile_pos_y", "$g_scene_max_y"),
					(call_script, "script_move_scene_prop_under_ground", ":missile_instance_id", 1),
					
				(else_try),
					(position_get_distance_to_terrain, ":dist_to_ground", pos_move),
					
					(scene_prop_get_slot, ":time", ":missile_instance_id", scene_prop_time),
					(scene_prop_get_slot, ":user_agent", ":missile_instance_id", scene_prop_user_agent),
					
					# (scene_prop_get_slot, ":cur_x_vel", ":missile_instance_id", scene_prop_move_x),
					(scene_prop_get_slot, ":cur_y_vel", ":missile_instance_id", scene_prop_move_y),
					(scene_prop_get_slot, ":cur_z_vel", ":missile_instance_id", scene_prop_move_z),
						
					(store_mul, ":y_vel_pow", ":cur_y_vel", ":cur_y_vel"),
					(store_mul, ":z_vel_pow", ":cur_z_vel", ":cur_z_vel"),
					(store_add, ":cur_vel", ":y_vel_pow", ":z_vel_pow"),
					
					(val_mul, ":cur_vel",	1000),
					(set_fixed_point_multiplier, 1000),
					(store_sqrt, ":cur_vel", ":cur_vel"),
					(val_div, ":cur_vel", 1000),
					(set_fixed_point_multiplier, 100),
					
					#energy and damage
					(scene_prop_get_slot, ":weight", ":missile_instance_id", scene_prop_weight),
					(store_div, ":weight_factor", ":weight", 2),
					(store_mul, ":velocity_factor", ":cur_vel", ":cur_vel"),
					(store_mul, ":cur_energy", ":weight_factor", ":velocity_factor"),
					(val_div, ":cur_energy", 1000000),
					# (store_div, ":raw_damage", ":cur_energy", 4),# 1/4 of the kinetic energy (same for items)
					(assign, ":final_damage", ":cur_energy"),
					
					# (assign,reg20,":time"),
					# (assign,reg21,":cur_vel"),
					# (assign,reg22,":cur_x_vel"),
					# (assign,reg23,":cur_y_vel"),
					# (assign,reg24,":cur_z_vel"),
					# (assign, reg25, ":dist_to_ground"),
					# (display_message,"@time:{reg20} vel:{reg21} x_vel:{reg22} y_vel:{reg23} z_vel:{reg24} dist_to_ground:{reg25}"),
					
					#calculating ball angle
					(try_begin),
						(neq, ":cur_y_vel", 0),
						(neq, ":cur_z_vel", 0),
						(store_mul, ":calc_z", ":cur_z_vel", 100),
						(store_div, ":div_value", ":calc_z", ":cur_y_vel"),
						(store_atan, ":ball_angle", ":div_value"),
					(else_try),
						(eq, ":cur_y_vel", 0),
						(try_begin),
							(ge, ":cur_z_vel", 0),
							(assign, ":ball_angle", 9000),
						(else_try),
							(assign, ":ball_angle", -9000),
						(try_end),
					(else_try),
						(assign, ":ball_angle", 0),
					(try_end),
					
					(copy_position, pos35, pos_move),
					(copy_position, pos36, pos_move),
					(position_rotate_x_floating, pos35, ":ball_angle"),#apply x rotation
					
					(assign, ":hitted_instance", -1),
					(try_begin),
						(ge, ":dist_to_ground", 0),
						(store_div, ":ray_length", ":cur_vel", 10),#set to cm
						(cast_ray, ":hitted_instance", pos40, pos35, ":ray_length"),
					(try_end),
					
					#hit something
					(try_begin),
						(this_or_next|lt, ":dist_to_ground", 0),
						(gt, ":hitted_instance", -1),
						
						(assign, ":hit_type", 1),
						(call_script, "script_move_scene_prop_under_ground", ":missile_instance_id", 1),
						(scene_prop_get_slot, ":radius", ":missile_instance_id", scene_prop_radius),
		
						(try_begin),#ground hit
							(lt, ":dist_to_ground", 0),
							(assign, ":collision_type", material_earth),
							(assign, ":ray_length", 0),
							(assign, ":hit_type", 2),

							#get hit pos on y axis
							(store_div, ":move_y", ":cur_y_vel", -10),
							(store_div, ":move_z", ":cur_z_vel", -10),
							(copy_position, pos_calc, pos36),
							(position_move_y, pos_calc, ":move_y"),
							(position_move_z, pos_calc, ":move_z"),
							(position_get_distance_to_terrain, ":distance", pos_calc),
							(val_sub, ":distance", ":dist_to_ground"),
							(try_begin),#division fix
								(eq, ":distance", 0),
								(assign, ":distance", 1),
							(try_end),
							(store_mul, ":cur_y_vel_1000", ":cur_y_vel", 1000),
							(store_div, ":hit_pos_y", ":cur_y_vel_1000", ":distance"),
							(val_mul, ":hit_pos_y", ":dist_to_ground"),
							(val_div, ":hit_pos_y", 10000),

							(copy_position, pos_hit, pos36),
							(position_move_y, pos_hit, ":hit_pos_y"),#move to hit pos on y axis
							(position_get_distance_to_terrain, ":distance", pos_hit),
							(val_mul, ":distance", -1),
							(val_add, ":distance", ":radius"),
							(position_move_z, pos_hit, ":distance"),#move to hit pos on z axis
	
							(copy_position, pos_calc, pos_hit),
							(call_script, "script_get_angle_of_ground_at_pos", 1, 100, 10, 10, 100, 0),
							(assign, ":x_rot_ground", reg0),
							(assign, ":y_rot_ground", reg1),

							(store_sub, ":ball_angle_new", ":ball_angle", ":x_rot_ground"),
							(val_abs, ":ball_angle_new"),
							(assign, ":angle_difference", ":ball_angle_new"),
							(val_add, ":ball_angle_new", ":x_rot_ground"),
							
							(copy_position, pos_spawn, pos_hit),#backup for front missiles
							(try_begin),#goes in the other direction
								(gt, ":ball_angle_new", 9000),
								(store_sub, ":difference", ":ball_angle_new", 9000),
								(store_sub, ":ball_angle_new", 9000, ":difference"),
								(position_rotate_z, pos_spawn, 180),
								(val_div, ":y_rot_ground", 3),
							(else_try),
								(val_div, ":y_rot_ground", -3),
							(try_end),
							(try_begin),#apply z rotation
								(neq, ":y_rot_ground", 0),
								(position_rotate_z_floating, pos_spawn, ":y_rot_ground"),
							(try_end),
							
							#energy for new missile
							(store_sub, ":energy_loss", 9000, ":angle_difference"),#10% reduction guaranteed
							(store_mul, ":init_energy", ":cur_energy", ":energy_loss"),
							(val_div, ":init_energy", 10000),

							# (assign, reg10, ":ball_angle"),
							# (assign, reg11, ":x_rot_ground"),
							# (assign, reg12, ":y_rot_ground"),
							# (assign, reg13, ":ball_angle_new"),
							# (assign, reg14, ":angle_difference"),
							# (assign, reg21, ":cur_vel"),
							# (assign, reg23, ":cur_y_vel"),
							# (assign, reg24, ":cur_z_vel"),
							# (assign, reg25, ":dist_to_ground"),
							# (assign, reg26, ":hit_pos_y"),
							# (display_message,"@old:{reg10} x:{reg11} y:{reg12} new:{reg13} diff:{reg14} vel:{reg21} y_vel:{reg23} z_vel:{reg24} dist_to_ground:{reg25} hit_pos_y:{reg26}"),
							
						(else_try),#prop hit
							(get_distance_between_positions, ":ray_length", pos35, pos40),
							(assign, ":init_energy", 0),
							(position_copy_origin, pos_hit, pos40),
							(position_copy_rotation, pos_hit, pos35),
							
							(store_mul, ":move_back", ":radius", -1),
							(position_move_y, pos_hit, ":move_back"),
							
							(scene_prop_get_slot, ":collision_type", ":hitted_instance", scene_prop_material),
							(prop_instance_get_scene_prop_kind, ":hitted_instance_kind", ":hitted_instance"),
							(try_begin),
								(is_between, ":hitted_instance_kind", destructible_props_begin, destructible_props_end),
									
								#calculate damage
								(scene_prop_get_slot, ":resistance", ":hitted_instance", scene_prop_resistance),
								(val_sub, ":final_damage", ":resistance"),
								(val_max, ":final_damage", 0),
								
								(gt, ":final_damage", 0),
								(scene_prop_get_hit_points, ":cur_hp", ":hitted_instance"),
								
								(try_begin),
									(multiplayer_is_server),
									(agent_is_active, ":user_agent"),
									(agent_get_player_id, ":dealer_player", ":user_agent"),
									(player_is_active, ":dealer_player"),
									
									#send damage report to dealer player
									(assign, reg0, ":final_damage"),
									(try_begin),
										(player_slot_eq, ":dealer_player", slot_player_show_damage_report, 1),
										(str_store_string, s0, "str_delivered_damage_to_object"),
										(call_script, "script_multiplayer_send_message_to_player", ":dealer_player", color_dealer_damage),
									(try_end),
									
									#set score
									(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
									(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
									
									(assign, ":cur_score", ":cur_hp"),
									(try_begin),
										(lt, ":final_damage", ":cur_score"),
										(assign, ":cur_score", ":final_damage"),
									(try_end),
									(val_add, ":cur_score", ":resistance"),
									(val_div, ":cur_score", score_artillery_divider),
									
									(gt, ":cur_score", 0),
									(player_get_slot, ":player_score", ":dealer_player", slot_player_score),
									(val_add, ":player_score", ":cur_score"),
									(player_set_slot, ":dealer_player", slot_player_score, ":player_score"),
									
									(get_max_players, ":num_players"),
									(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
										(player_is_active, ":player_no"),
										(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_score_count, ":dealer_player", ":player_score"),
									(try_end),
								(try_end),
								
								(try_begin),
									(gt, ":final_damage", ":cur_hp"),
									(assign, ":hit_type", 2),
									(assign, ":ball_angle_new", ":ball_angle"),
									
									#energy for new missile
									(store_add, ":energy_loss", ":cur_hp", ":resistance"),
									(val_mul, ":energy_loss", 10),
									(store_sub, ":init_energy", ":cur_energy", ":energy_loss"),
									(val_max, ":init_energy", 0),
								(try_end),
								
								(try_begin),#only destructible by artillery
									(troop_slot_eq, "trp_prop_destructible", ":hitted_instance_kind", 0),
									(this_or_next|multiplayer_is_server),
									(neg|game_in_multiplayer_mode),
									(call_script, "script_server_missile_hit_scene_prop", ":hitted_instance", ":final_damage"),
								(try_end),
								
								(this_or_next|multiplayer_is_server),
								(neg|game_in_multiplayer_mode),
								(prop_instance_receive_damage, ":hitted_instance", ":user_agent", ":final_damage"),
							(try_end),
							(position_copy_rotation, pos_hit, pos36),#reset rotation
							(copy_position, pos_spawn, pos_hit),#backup for front missiles
						(try_end),
							
						#shot hit missiles
						(store_sub, ":lost_energy", ":cur_energy", ":init_energy"),#use the lost energy of the shot
						(store_div, ":num_missiles", ":lost_energy", 4000),
						
						(store_mul, ":max_angle", ":num_missiles", 200),
						(val_clamp, ":max_angle", 1500, 9001),
						
						(val_div, ":num_missiles", 3),
						(val_max, ":num_missiles", 1),
						(store_div, ":angle_decr", ":max_angle", ":num_missiles"),
						
						(try_begin),
							(eq, ":collision_type", material_earth),
							(assign, ":start_item", itm_ground_sml),
							(assign, ":end_item", itm_ground_big +1),
							(assign, ":sound", "snd_hit_ground_big"),
							
							(store_div, ":burst_strenght", ":lost_energy", 3000),
							(val_clamp, ":burst_strenght", 1, 101),
							
							(store_mul, ":particle_angle", ":ball_angle", -1),
							(val_add, ":particle_angle", ":ball_angle_new"),
							(val_div, ":particle_angle", 2),
							
							(copy_position, pos_calc, pos_hit),
							(position_rotate_x_floating, pos_calc, ":particle_angle"),
							(particle_system_burst_no_sync, "psys_hit_ground_big", pos_calc, ":burst_strenght"),
							
							# (assign, reg10, ":burst_strenght"),
							# (display_message, "@strength {reg10}"),
							
						(else_try),
							(eq, ":collision_type", material_stone),
							(assign, ":start_item", itm_stone_sml),
							(assign, ":end_item", itm_stone_big +1),
							(assign, ":sound", "snd_hit_stone_big"),
						(else_try),
							(eq, ":collision_type", material_wood),
							(assign, ":start_item", itm_wood_sml),
							(assign, ":end_item", itm_wood_big +1),
							(assign, ":sound", "snd_hit_wood_big"),
						(else_try),
							(assign, ":start_item", 0),
							(assign, ":end_item", 3),
							(assign, ":sound", "snd_hit_ground_big"),
						(try_end),
						
						(try_for_range, ":i", 0, ":hit_type"),
							(try_begin),#backward
								(eq, ":i", 0),
								(copy_position, pos11, pos_hit),
								(position_rotate_z, pos11, 180),
								(store_mul, ":missile_angle", ":ball_angle", -1),
							(else_try),#forward
								(copy_position, pos11, pos_spawn),
								(assign, ":missile_angle", ":ball_angle_new"),
							(try_end),
							(position_rotate_x_floating, pos11, ":missile_angle"),
							
							(try_begin),
								(eq, ":collision_type", material_earth),
								(particle_system_burst_no_sync, "psys_hit_ground_big", pos11, ":burst_strenght"),
							(try_end),
							
							(try_for_range, ":cur_item", ":start_item", ":end_item"),
								(try_begin),
									(neg|multiplayer_is_dedicated_server),
									(try_begin),
										(this_or_next|eq, ":cur_item", 0),#no missile
										(this_or_next|eq, ":cur_item", itm_stone_sml),
										(eq, ":cur_item", itm_wood_sml),
										(assign, ":cur_particle", "psys_launch_dust_object_sml"),
									(else_try),
										(this_or_next|eq, ":cur_item", 1),#no missile
										(this_or_next|eq, ":cur_item", itm_stone_med),
										(eq, ":cur_item", itm_wood_med),
										(assign, ":cur_particle", "psys_launch_dust_object_med"),
									(else_try),
										(this_or_next|eq, ":cur_item", 2),#no missile
										(this_or_next|eq, ":cur_item", itm_stone_big),
										(eq, ":cur_item", itm_wood_big),
										(assign, ":cur_particle", "psys_launch_dust_object_big"),
									
									(else_try),
										(eq, ":cur_item", itm_ground_sml),
										(assign, ":cur_particle", "psys_launch_dust_ground_sml"),
									(else_try),
										(eq, ":cur_item", itm_ground_med),
										(assign, ":cur_particle", "psys_launch_dust_ground_med"),
									(else_try),
										(eq, ":cur_item", itm_ground_big),
										(assign, ":cur_particle", "psys_launch_dust_ground_big"),
									(try_end),
								(try_end),
								
								(try_begin),#calculate velocity
									(gt, ":start_item", 0),
									(set_fixed_point_multiplier, 100),
									(item_get_weight, ":weight_factor", ":cur_item"),
									(val_div, ":weight_factor", 2),
									(val_max, ":weight_factor", 1),
									(store_mul, ":energy_factor", ":lost_energy", 25),#1/4 energy... reduces velocity
									(store_div, ":max_v0", ":energy_factor", ":weight_factor"),
									(store_sqrt, ":max_v0", ":max_v0"),
									(store_div, ":min_v0", ":max_v0", 2),
									
									# (assign, reg10, ":max_v0"),
									# (display_message, "@v0: {reg10}"),
								(try_end),
								(store_add, ":rot_max", ":max_angle", 1),
								
								(try_for_range, ":unused", 0, ":num_missiles"),
									(copy_position, pos12, pos11),
									(store_random_in_range, ":shot_rot_y", 0, 36000),
									(store_random_in_range, ":shot_rot_x", 0, ":rot_max"),
									
									(set_fixed_point_multiplier, 100),
									(position_rotate_y_floating, pos12, ":shot_rot_y"),
									(position_rotate_x_floating, pos12, ":shot_rot_x"),
									
									(try_begin),
										(this_or_next|multiplayer_is_server),
										(neg|game_in_multiplayer_mode),
										(gt, ":start_item", 0),
										(store_random_in_range, ":shot_v0", ":min_v0", ":max_v0"),
										(store_sub, ":angle_factor", 10000, ":shot_rot_x"),#great angle = low velocity
										(val_mul, ":shot_v0", ":angle_factor"),
										(val_div, ":shot_v0", 10000),
										(add_missile, ":user_agent", pos12, ":shot_v0", ":cur_item", 0, ":cur_item", 0),
									(try_end),
									
									(try_begin),
										(neg|multiplayer_is_dedicated_server),
										(particle_system_burst_no_sync, ":cur_particle", pos12, 10),
									(try_end),
									(val_sub, ":rot_max", ":angle_decr"),
								(try_end),
							(try_end),
						(try_end),
						
						(try_begin),
							(this_or_next|multiplayer_is_server),
							(neg|game_in_multiplayer_mode),
							(call_script, "script_server_missile_hit", ":user_agent", missile_roundshot, 500, 250),
							
							#init new missile
							(eq, ":hit_type", 2),
							(store_mul, ":needed_energy", ":weight", 10),
							(ge, ":init_energy", ":needed_energy"),
							
							(store_div, ":weight_factor", ":weight", 2),
							(val_max, ":weight_factor", 1),
							(store_mul, ":energy_factor", ":init_energy", 100),
							(store_div, ":init_vel", ":energy_factor", ":weight_factor"),
							(val_mul, ":init_vel", 100),
							(store_sqrt, ":init_vel", ":init_vel"),
							
							# make rotation fixed point. higher multiplier = more accuracy
							(val_mul, ":ball_angle_new", 100),
							(set_fixed_point_multiplier, 10000),
							# y += Speed * Math.Cos(angle);
							(store_cos, ":cos_of_angle", ":ball_angle_new"),
							(store_mul,	":init_y_vel", ":cos_of_angle",	":init_vel"),
							(val_div,	":init_y_vel", 10000),
							# z += speed * Math.Sin(angle);
							(store_sin, ":sin_of_angle", ":ball_angle_new"),
							(store_mul,	":init_z_vel", ":sin_of_angle", ":init_vel"),
							(val_div,	":init_z_vel", 10000),
							(set_fixed_point_multiplier, 100),
							
							(call_script, "script_spawn_new_or_get_free_scene_prop", "spr_round_shot_missile"),
							(call_script, "script_prop_missile_init", reg0, ":init_y_vel", ":init_z_vel", ":user_agent"),
							
							# (assign, reg20, ":cur_energy"),
							# (assign, reg21, ":init_energy"),
							# (assign, reg22, ":cur_vel"),
							# (assign, reg23, ":init_vel"),
							# (display_message,"@cur_energy:{reg20} init_energy:{reg21} cur_vel:{reg22} init_vel:{reg23}"),
						(try_end),
						
						(try_begin),#client side only
							(neg|multiplayer_is_dedicated_server),
							(position_copy_origin, pos_calc, pos_hit),
							(call_script, "script_reserve_sound_channel", ":sound"),
							(eq, "$g_show_shot_distance", 1),#shot distance
							(call_script, "script_client_get_player_agent"),
							(eq, reg0, ":user_agent"),
							(position_copy_origin, pos1, pos_hit),
							(call_script, "script_shot_distance", ":user_agent"),
						(try_end),
					
					#move_missile
					(else_try),
						#friction
						(scene_prop_get_slot, ":friction", ":missile_instance_id", scene_prop_friction),#k= 0.00X *v0
						(val_mul, ":friction", ":cur_vel"),
						(val_mul, ":friction", ":cur_vel"),

						(store_mul, ":cur_y_vel_1000", ":cur_y_vel", 1000),
						(store_mul, ":cur_z_vel_1000", ":cur_z_vel", 1000),
						
						(val_max, ":cur_vel", 1),
						(store_div, ":y_multi", ":cur_y_vel_1000", ":cur_vel"),
						(store_div, ":z_multi", ":cur_z_vel_1000", ":cur_vel"),
						
						(store_mul, ":friction_y", ":friction", ":y_multi"),
						(val_div, ":friction_y", 1000000000),
						(val_sub, ":cur_y_vel", ":friction_y"),

						(store_mul, ":friction_z", ":friction", ":z_multi"),
						(val_div, ":friction_z", 1000000000),
						(val_sub, ":cur_z_vel", ":friction_z"),
						
						#gravity
						(val_sub, ":cur_z_vel", 98),#-9.8 cm per 0.1 sec
						
						#get cm
						# (store_div, ":move_x", ":cur_x_vel", 10),
						(store_div, ":move_y", ":cur_y_vel", 10),
						(store_div, ":move_z", ":cur_z_vel", 10),
						
						#get mm
						# (store_mod, ":move_x_mm", ":cur_x_vel", 10),
						(store_mod, ":move_y_mm", ":cur_y_vel", 10),
						(store_mod, ":move_z_mm", ":cur_z_vel", 10),
						
						#round mm to cm
						# (try_begin),
							# (ge, ":move_x_mm", 5),
							# (val_add, ":move_x", 1),
						# (try_end),
						(try_begin),
							(ge, ":move_y_mm", 5),
							(val_add, ":move_y", 1),
						(try_end),
						(try_begin),
							(ge, ":move_z_mm", 5),
							(val_add, ":move_z", 1),
						(try_end),
						
						# (position_move_x, pos_move, ":move_x"),
						(position_move_y, pos_move, ":move_y"),
						(position_move_z, pos_move, ":move_z"),
						
						(try_begin),#only animate client side
							(neg|multiplayer_is_dedicated_server),
							(prop_instance_animate_to_position, ":missile_instance_id", pos_move, 10),
						(else_try),
							(prop_instance_set_position, ":missile_instance_id", pos_move, 1),
						(try_end),
						
						(scene_prop_set_slot, ":missile_instance_id", scene_prop_move_y, ":cur_y_vel"),
						(scene_prop_set_slot, ":missile_instance_id", scene_prop_move_z, ":cur_z_vel"),
						(val_add, ":time", 1),
						(scene_prop_set_slot, ":missile_instance_id", scene_prop_time, ":time"),
					(try_end),
					
					#check for agents
					(try_begin),
						(this_or_next|multiplayer_is_server),#server side only
						(neg|game_in_multiplayer_mode),
						
						(gt, ":ray_length", 0),
						(position_move_z, pos35, -90, 1),#for body check
						(try_for_agents, ":cur_agent", pos35, ":ray_length"),
							(agent_is_active, ":cur_agent"),
							(agent_is_alive, ":cur_agent"),
							(agent_get_position, pos40, ":cur_agent"),
							(get_distance_between_positions, ":distance_ball_agent", pos35, pos40),
								
							(le, ":distance_ball_agent", ":ray_length"),
							(position_transform_position_to_local, pos45, pos35, pos40),
							(position_get_x, ":x_value", pos45),
							(position_get_y, ":y_value", pos45),
							(position_get_z, ":z_value", pos45),
								
							# (assign,reg22,":x_value"),
							# (assign,reg23,":y_value"),
							# (assign,reg24,":z_value"),
							# (display_message,"@x_value: {reg22}  y_value: {reg23}  z_value: {reg24}"),
							
							(is_between, ":x_value", -50, 51),
							(is_between, ":y_value", -50, ":ray_length"),
							(is_between, ":z_value", -100, 101),
							
							(agent_deliver_damage_to_agent_advanced, ":unused", ":user_agent", ":cur_agent", ":final_damage", itm_weapon_round_shot),
						(try_end),
					(try_end),
				(try_end),
			(try_end),

		
		#clientside only
    (try_begin),
      (neg|multiplayer_is_dedicated_server),
			(call_script, "script_client_stamina_change_1", 0),

			(try_begin),#update camera
				(gt, "$custom_camera", 0),
				(call_script, "script_client_custom_camera", "$custom_camera"),
			(try_end),
			
			#rain/snow
			(try_begin),
				(gt, "$g_precipitation_strength", 0),
				(mission_cam_get_position, pos17),
				(position_move_y, pos17, 350, 0),
				(position_move_z, pos17, 350, 1),
				(try_begin),
					(this_or_next|eq, "$g_cur_scene_type", scene_type_snow),
					(eq, "$g_cur_scene_type", scene_type_snow_forest),
					(particle_system_burst_no_sync, "psys_game_snow", pos17, "$g_precipitation_strength"),
				(else_try),
					(particle_system_burst_no_sync, "psys_game_rain", pos17, "$g_precipitation_strength"),
				(try_end),
			(try_end),
			
			(try_begin),
				(gt, "$g_wind_strength", 0),
				(scene_prop_get_num_instances, ":num_instances", "spr_windmill_fan_turning"),
				(try_for_range, ":cur_instance", 0, ":num_instances"),
					(scene_prop_get_instance, ":instance_no", "spr_windmill_fan_turning", ":cur_instance"),
					(prop_instance_get_position, pos1, ":instance_no"),
					# (store_mul, ":angle", "$g_wind_strength", 5),
					(position_rotate_y, pos1, "$g_wind_strength"),
					(prop_instance_animate_to_position, ":instance_no", pos1, 10),# animate time
				(try_end),
			(try_end),
		(try_end),
	
		#serverside only
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			
			#check for artillery fire
			(scene_prop_get_num_instances, ":num_instances", "spr_cannon_barrel_1_32lb"),
			(try_for_range, ":cur_instance", 0, ":num_instances"),
				(scene_prop_get_instance, ":instance_id", "spr_cannon_barrel_1_32lb", ":cur_instance"),
				(scene_prop_slot_eq, ":instance_id", scene_prop_is_used, 1),#used
				(scene_prop_slot_eq, ":instance_id", scene_prop_usage_state, 6),
				
				(scene_prop_slot_ge, ":instance_id", scene_prop_next_action_time, 0),
				(neg|scene_prop_slot_ge, ":instance_id", scene_prop_next_action_time, ":cur_time"),
				(scene_prop_set_slot, ":instance_id", scene_prop_next_action_time, -1),

        (call_script, "script_control_scene_prop", ":instance_id", prop_action_3),
				(get_max_players, ":num_players"),                       
				(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
          (player_is_active, ":cur_player_no"),
          (multiplayer_send_2_int_to_player, ":cur_player_no", multiplayer_event_client_control_scene_prop, ":instance_id", prop_action_3),
        (try_end),
			(try_end),
			
			#deal damage to prop if needed
			(try_begin),
				(gt, "$spawn_prop_different_hp", -1),
				(gt, "$next_spawn_prop_hp", -1),
				#(assign, reg1, "$next_spawn_prop_hp"),
				#(display_message, "@hp {reg1}"),
				(scene_prop_get_max_hit_points, ":damage_hp", "$spawn_prop_different_hp"),
				(val_sub, ":damage_hp", "$next_spawn_prop_hp"),
				(try_begin),
					(gt, ":damage_hp", 1),
					(prop_instance_receive_damage, "$spawn_prop_different_hp", "$agent_deployed_shield", ":damage_hp"),
				(try_end),
				(assign, "$next_spawn_prop_hp", -1),	
				(assign, "$spawn_prop_different_hp", -1),
			(try_end),
			
			(try_for_agents, ":cur_agent"),
				(agent_is_active, ":cur_agent"),
				(agent_is_alive, ":cur_agent"),
				
				#doesnt work for some reason
				# (call_script, "script_workaround_agent_attack_action", ":cur_agent"),
				# (try_begin),
					# (eq, reg0, 5),#reloading
					# (agent_get_animation_progress, reg0, ":cur_agent", 1),
					# (lt, reg0, 75),
					# (display_message, "@{reg0}"),
					# (set_fixed_point_multiplier, 100),
					# (agent_set_animation, ":cur_agent", "anim_reload_musket"),
					# (agent_set_animation_progress, ":cur_agent", 75),
				# (try_end),
				
				(try_begin),
					(agent_slot_ge, ":cur_agent", slot_agent_interval_damage, 1),
					(call_script, "script_agent_interval_damage", ":cur_agent"),
				(try_end),
				(try_begin),
					(agent_is_human, ":cur_agent"),
					(call_script, "script_agent_speed_limiter", ":cur_agent"),

					# (try_begin),
					# 	(agent_get_wielded_item, ":item_r", ":cur_agent", 0),
					# 	(gt, ":item_r", -1),
					# 	(item_get_type, ":item_type", ":item_r"),
					# 	(is_between, ":item_type", itp_type_one_handed_wpn, itp_type_polearm +1),
					# 	(call_script, "script_workaround_agent_attack_action", ":cur_agent"),
					# 	(eq, reg0, 2),#releasing
						
					# 	(agent_get_bone_position, pos1, ":cur_agent", hb_item_r, 1),
					# 	(assign, ":next_slot", slot_agent_last_item_pos_x),
					# 	(try_for_range, ":cur_slot", slot_agent_cur_item_pos_x, slot_agent_last_item_pos_x),
					# 		(agent_get_slot, reg0, ":cur_agent", ":cur_slot"),
					# 		(agent_set_slot, ":cur_agent", ":next_slot", reg0),#backup
					# 		(try_begin),
					# 			(eq, ":cur_slot", slot_agent_cur_item_pos_x),
					# 			(position_get_x, reg0, pos1),
					# 		(else_try),
					# 			(eq, ":cur_slot", slot_agent_cur_item_pos_y),
					# 			(position_get_y, reg0, pos1),
					# 		(else_try),
					# 			(eq, ":cur_slot", slot_agent_cur_item_pos_z),
					# 			(position_get_z, reg0, pos1),
					# 		(else_try),
					# 			(eq, ":cur_slot", slot_agent_cur_item_rot_x),
					# 			(position_get_rotation_around_x, reg0, pos1),
					# 		(else_try),
					# 			(eq, ":cur_slot", slot_agent_cur_item_rot_y),
					# 			(position_get_rotation_around_y, reg0, pos1),
					# 		(else_try),
					# 			(position_get_rotation_around_z, reg0, pos1),
					# 		(try_end),
					# 		(agent_set_slot, ":cur_agent", ":cur_slot", reg0),
					# 		(val_add, ":next_slot", 1),
					# 	(try_end),
					# (try_end),
					

					(agent_get_animation, ":anim_lower", ":cur_agent", 0),
					# (call_script, "script_workaround_agent_attack_action", ":cur_agent"),
					# (assign, ":attack_action", reg0),
					
					# #power-strike damage modifier
					# (agent_get_troop_id, ":dealer_troop", ":cur_agent"),
					# (try_begin),
						# (gt, ":dealer_troop", -1),
						# (try_begin),
							# (eq, ":anim_lower", "anim_kick_right_leg"),
							# (assign, ":modify", 1),
						# (else_try),
							# (is_between, ":attack_action", 1, 3),
							# (agent_get_wielded_item, ":item_id", ":cur_agent", 0),
							# (eq, ":item_id", -1),#no weapon
							# (assign, ":modify", 1),
						# (else_try),
							# (assign, ":modify", 0),
						# (try_end),
						
						# (store_skill_level, ":damage_modifier", "skl_power_strike", ":dealer_troop"),
						# (try_begin),
							# (eq, ":modify", 1),
							# (try_begin),
								# (gt, ":damage_modifier", 0),
								# (val_mul, ":damage_modifier", ps_damage_bonus),
								# (val_add, ":damage_modifier", 100),
							# (else_try),
								# (assign, ":damage_modifier", 100),
							# (try_end),
						# (else_try),
							# (try_begin),
								# (gt, ":damage_modifier", 0),
								# (store_mul, ":new_modifier", ":damage_modifier", ps_damage_bonus),
								# (val_add, ":new_modifier", 100),
								# (val_mul, ":damage_modifier", 8),
								# (val_add, ":damage_modifier", 100),
								# (store_div, ":damage_modifier", 100000, ":damage_modifier"),
								# (val_mul, ":damage_modifier", ":new_modifier"),
								# (val_div, ":damage_modifier", 1000),
							# (else_try),
								# (assign, ":damage_modifier", 100),
							# (try_end),
						# (try_end),
						
						# (try_begin),
							# (neg|agent_slot_eq, ":cur_agent", slot_agent_damage_modifier, ":damage_modifier"),
							# (agent_set_slot, ":cur_agent", slot_agent_damage_modifier, ":damage_modifier"),
							# (agent_set_damage_modifier, ":cur_agent", ":damage_modifier"),
							# # (assign, reg1, ":damage_modifier"),
							# # (display_message, "@mod: {reg1}"),
						# (try_end),
					# (try_end),
					
					#shield bash
					(try_begin),
						(eq, ":anim_lower", "anim_shield_bash"),
						(try_begin),
							(agent_slot_eq, ":cur_agent", slot_agent_nudge_state, 0),
							(neg|agent_slot_ge, ":cur_agent", slot_agent_nudge_time, ":cur_time"),
							(agent_get_troop_id, ":dealer_troop", ":cur_agent"),
							(gt, ":dealer_troop", -1),
							(agent_get_wielded_item, ":item_id", ":cur_agent", 1),
							(gt, ":item_id", -1),
							(agent_set_slot, ":cur_agent", slot_agent_nudge_state, 1),
							(assign, ":final_radius", 50),#shieldbash radius calculation
							(item_get_shield_height, ":shield_radius", ":item_id"),
						
							(try_begin),
								(gt, ":shield_radius", ":final_radius"),
								(assign, ":final_radius", ":shield_radius"),
							(try_end),
							
							(set_fixed_point_multiplier, 100),
							(agent_get_position, pos20, ":cur_agent"),
							(copy_position, pos21, pos20),
							(position_move_y, pos21, 80),
							
							(assign, ":closest_agent", -1),
							(assign, ":closest_dist", ":final_radius"),
							
							(try_for_agents, ":agent"),
								(agent_is_active, ":agent"),
								(agent_is_alive,":agent"),
								(neq, ":agent", ":cur_agent"),
								
								(agent_get_position, pos22, ":agent"),
								(get_distance_between_positions, ":distance", pos22, pos21),
								(lt, ":distance", ":closest_dist"),
								(assign, ":closest_dist", ":distance"),
								(assign, ":closest_agent", ":agent"),
							(try_end),
							
							(try_begin),#fix duels
								(eq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
								(gt, ":closest_agent", -1),
								(neg|agent_slot_eq, ":cur_agent", slot_agent_in_duel_with, ":closest_agent"),
								(assign, ":closest_agent", -1),
							(try_end),
							
							(try_begin),
								(gt, ":closest_agent", -1),
								(get_distance_between_positions, ":ray_length", pos22, pos20),
								
								(copy_position, pos24, pos20),
								(position_move_z, pos24, 130),
								
								(try_begin),# -1 is ground
									(cast_ray, ":hitted_instance", pos23, pos24, ":ray_length"),
								(else_try),
									(assign, ":hitted_instance", -2),
								(try_end),
								
								(eq, ":hitted_instance", -2),
								(agent_get_wielded_item, ":item_r", ":closest_agent", 0),
								(agent_get_wielded_item, ":item_l", ":closest_agent", 1),
								(try_begin),
									(this_or_next|gt, ":item_r", -1),
									(gt, ":item_l", -1),
									(agent_get_defend_action, ":defend_action", ":closest_agent"),
									(eq, ":defend_action", 2), #Blocking.
									(neg|position_is_behind_position, pos22, pos20),
									(agent_play_sound, ":closest_agent", "snd_shield_hit_wood_wood"),
									
								(else_try),
									(item_get_thrust_damage, ":damage", ":item_id"),
									# (store_skill_level, ":skl", "skl_power_strike", ":dealer_troop"),
									# (try_begin),
									# 	(gt, ":skl", 0),
									# 	(store_mul, ":skl_bonus",":skl", ps_damage_bonus),
									# 	(val_add, ":skl_bonus", 100),
									# 	(val_mul, ":damage", ":skl_bonus"),
									# 	(val_div, ":damage", 100),
									# (try_end),
							
									# (store_attribute_level, ":strength", ":dealer_troop", ca_strength),
									# (val_div, ":strength", 5),
									# (val_add, ":damage", ":strength"),
									
									(agent_deliver_damage_to_agent_advanced, ":delivered_damage", ":cur_agent", ":closest_agent", ":damage", ":item_id"),
									(agent_play_sound, ":closest_agent", "snd_wooden_hit_low_armor_high_damage"),

									(agent_is_human, ":closest_agent"),
									(try_begin),
										(lt, ":delivered_damage", 1),
										(store_random_in_range, ":cur_anim", "anim_strike_chest_left", "anim_strike_legs_left"),
										(agent_set_animation, ":closest_agent", ":cur_anim", 1),
									(try_end),
									
									(store_agent_hit_points,":hp",":closest_agent",1),
									(try_begin),#male
										(agent_slot_eq, ":closest_agent", slot_agent_gender, tf_male),
										(try_begin),
											(gt, ":hp", 0),
											(agent_play_sound, ":closest_agent", "snd_man_hit"),
										(else_try),
											(neg|multiplayer_is_dedicated_server),
											(agent_play_sound, ":closest_agent", "snd_man_die"),
										(try_end),
									(else_try),#female
										(try_begin),
											(gt, ":hp", 0),
											(agent_play_sound, ":closest_agent", "snd_woman_hit"),
										(else_try),
											(neg|multiplayer_is_dedicated_server),
											(agent_play_sound, ":closest_agent", "snd_woman_die"),
										(try_end),
									(try_end),
								(try_end),
							(try_end),
						(try_end),
					(else_try),#reset
						(agent_slot_eq, ":cur_agent", slot_agent_nudge_state, 1),
						(agent_set_slot, ":cur_agent", slot_agent_nudge_state, 0),
					(try_end),
					
					#volley fire
					(try_begin),
						(agent_is_non_player, ":cur_agent"),
						(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
						(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
						(neg|game_in_multiplayer_mode),
						
						(try_begin),
							(game_in_multiplayer_mode),
							(agent_get_group, ":cur_team", ":cur_agent"),
						(else_try),
							(agent_get_team, ":cur_team", ":cur_agent"),
						(try_end),
						(agent_get_division, ":cur_class", ":cur_agent"),
						
						(store_add, ":cur_array", "trp_group_0_order_volley", ":cur_class"),
						(try_begin),#volley fire
							(troop_slot_eq, ":cur_array", ":cur_team", 1),
							(agent_get_wielded_item, ":weapon", ":cur_agent", 0),
							(try_begin),
								(gt, ":weapon", 0),
								(item_get_type, ":weapon_type", ":weapon"),
								(try_begin),
									(this_or_next|eq, ":weapon_type", itp_type_bow),
									(this_or_next|eq, ":weapon_type", itp_type_crossbow),
									(eq, ":weapon_type", itp_type_musket),
									
									(try_begin),
										(agent_slot_ge, ":cur_agent", slot_agent_next_action_time, 0),
										(neg|agent_slot_ge, ":cur_agent", slot_agent_next_action_time, ":cur_time"),
										##
										#(assign, reg50, ":cur_agent"),
										#(assign, reg51, ":cur_time"),
										#(agent_get_slot, reg52, ":cur_agent", slot_agent_volley_fire_state),
										#(display_message, "@agent:{reg50} action time:{reg51} volley state:{reg52}"),
										##
										#free
										(try_begin),
											(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
											(store_random_in_range, ":next_action_time", volley_ready_min, volley_ready_max +1),
											(val_add, ":next_action_time", ":cur_time"),
											(agent_set_slot, ":cur_agent", slot_agent_next_action_time, ":next_action_time"),
											(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_ready),
										#ready
										(else_try),
											(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_ready),
											(agent_set_attack_action, ":cur_agent", 0, 1),#ready and hold
											(store_random_in_range, ":next_action_time", volley_release_min, volley_release_max +1),
											(val_add, ":next_action_time", ":cur_time"),
											(agent_set_slot, ":cur_agent", slot_agent_next_action_time, ":next_action_time"),
											(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_release),
											#(assign, reg60, ":next_action_time"),
											#(display_message, "@shot at {reg60}", 0xCCCCCC),
										#release
										(else_try),
											(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_release),
											(agent_set_attack_action, ":cur_agent", 0, 0),#shot
											(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_free),#reset
											(agent_set_slot, ":cur_agent", slot_agent_next_action_time, -1),#reset
										#nothing
										(else_try),
											(agent_set_slot, ":cur_agent", slot_agent_next_action_time, -1),#reset
										(try_end),
										
									(else_try),#reload weapon outside action time
										(this_or_next|eq, ":weapon_type", itp_type_crossbow),
										(eq, ":weapon_type", itp_type_musket),
										(agent_get_item_cur_ammo, ":loaded_ammo", ":cur_agent", 0),
										(try_begin),#reload unloaded weapon
											(neg|agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_reload),
											(try_begin),
												(eq, ":loaded_ammo", 0),
												(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_reload),
												(agent_set_attack_action, ":cur_agent", 0, 1),#ready and hold
												###
												#(assign, reg50, ":cur_agent"),
												#(assign, reg51, ":cur_time"),
												#(display_message, "@agent:{reg50} starts reloading time:{reg51}"),
												###
											(try_end),
										(else_try),
											(eq, ":loaded_ammo", 1),
											(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_free),
											(agent_set_attack_action, ":cur_agent", -2, 0),#cancel attack action
											###
											#(assign, reg50, ":cur_agent"),
											#(assign, reg51, ":cur_time"),
											#(display_message, "@agent:{reg50} weapon loaded time:{reg51}"),
											###
										(try_end),
									(try_end),
									
								(else_try),#switched weapon while preparing volley
									(neg|agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
									(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_free),#reset
									(agent_set_attack_action, ":cur_agent", -2, 0),#cancel attack action
									###
									#(assign, reg50, ":cur_agent"),
									#(display_message, "@agent:{reg50} no action"),
									###
								(try_end),
							(try_end),
							
						(else_try),#no volley fire
							(neg|agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
							(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_free),#reset
							(agent_set_attack_action, ":cur_agent", 0, 0),
							###
							#(assign, reg50, ":cur_agent"),
							#(display_message, "@agent:{reg50} no volley"),
							###
						(try_end),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
  ]),
  
#timer_2
	(1, 0, 0, [],
  [
###
# (store_random_in_range, ":x", -100, 101),
# (store_random_in_range, ":y", -100, 101),
# (store_random_in_range, ":z", -100, 101),

# (set_fixed_point_multiplier, 100),
# (set_shader_param_float4, "@vSunDir", ":x", ":y", ":z", 0),

# (display_message, "@random"),

# (rebuild_shadow_map),
###

		#timer fix
		(store_mission_timer_a, ":cur_time"),
		(store_sub, ":time_difference", ":cur_time", "$g_timer_a_last_value"),
		(assign, "$g_timer_a_last_value", ":cur_time"),
		##
		#(assign, reg50, ":cur_time"),
		#(assign, reg51, ":time_difference"),
		#(display_message, "@time:{reg50} difference:{reg51}"),
		##
		(assign, ":real_time", -1),
		(try_begin),
			(gt, ":time_difference", 1),
			(store_sub, ":real_time", ":cur_time", ":time_difference"),
			(val_add, ":real_time", 1),
		(try_end),
		
		#round end
		(try_begin),
			(eq, "$g_round_ended", 1),
			(val_add, "$g_init_on_round_end", 1),
		(else_try),
			(assign, "$g_init_on_round_end", 0),
		(try_end),
			
	  #clientside only
    (try_begin),
      (neg|multiplayer_is_dedicated_server),
			
			(try_begin),
				(eq, "$g_disable_hud", 0),
				(call_script, "script_client_get_player_agent"),
				(assign, ":agent", reg0),
				(agent_is_active, ":agent"),
				(agent_is_alive, ":agent"),
				(try_begin),
					(neg|is_presentation_active, "prsnt_mod_hud"),
					(start_presentation, "prsnt_mod_hud"),
				(try_end),
				(try_begin),
					(agent_has_item_equipped, ":agent", itm_surgeon_kit),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
					(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
					(neg|is_presentation_active, "prsnt_surgeon_hud"),
					(start_presentation, "prsnt_surgeon_hud"),
				(try_end),
			(try_end),
	  
			(call_script, "script_client_stamina_change_1", 1),
			# (try_begin),
			# 	(game_key_is_down, gk_attack),
			# 	(neg|game_key_is_down, gk_defend),
			# 	(call_script, "script_client_bow_draw_timer"),
			# (try_end),
	  
			(try_begin),
				(game_in_multiplayer_mode),
				(neg|multiplayer_is_server),
				(call_script, "script_agent_drowning", -1),#-1 for player
      (try_end),
	  
			(try_begin),
				(eq, "$g_show_movement_speed", 1),
				(call_script, "script_client_speed_calculation"),
			(try_end),

			# (get_player_agent_no, ":player_agent"),
			# (agent_is_active, ":player_agent"),
			# (agent_is_alive, ":player_agent"),
			# (agent_get_animation_progress, reg1, ":player_agent", 1), #Stores <agent_no>'s channel [<channel_no>] animation progress (in %%) into <destination>
			# (display_message, "@{reg1}"),
		(try_end),
	
		#fire props
		(assign, ":radius", 20),
		(assign, ":center_z", 10),
		(assign, ":particle_1", "psys_fire_xs"),
		(assign, ":particle_2", "psys_smoke_xs"),
		(store_add, ":loop_end", "spr_fire_xl", 1),
		(try_for_range, ":cur_prop", "spr_fire_xs", ":loop_end"),
			(scene_prop_get_num_instances, ":num_instances", ":cur_prop"),
			(try_for_range, ":cur_instance", 0, ":num_instances"),
				(scene_prop_get_instance, ":instance_id", ":cur_prop", ":cur_instance"),
				(try_begin),
					(scene_prop_slot_eq, ":instance_id", scene_prop_is_used, 1),
					(try_begin),
						(neg|multiplayer_is_dedicated_server),
						(scene_prop_slot_eq, ":instance_id", scene_prop_smoke_effect_done, 0),
						(scene_prop_set_slot, ":instance_id", scene_prop_smoke_effect_done, 1),
						(init_position, pos0),
						(prop_instance_add_particle_system, ":instance_id", ":particle_1", pos0),
						(prop_instance_add_particle_system, ":instance_id", ":particle_2", pos0),
						(try_begin),
							(eq, ":cur_prop", "spr_fire_xs"),
							(prop_instance_play_sound, ":instance_id", "snd_fire_sml", 0),
						(else_try),
							(eq, ":cur_prop", "spr_fire_xl"),
							(prop_instance_play_sound, ":instance_id", "snd_fire_big", 0),
						(else_try),
							(prop_instance_play_sound, ":instance_id", "snd_fire_med", 0),
						(try_end),
					(try_end),
					(try_begin),
						(this_or_next|multiplayer_is_server),
						(neg|game_in_multiplayer_mode),
						(try_begin),
							(neg|scene_prop_slot_eq, ":instance_id", scene_prop_prune_time, -1),
							(neg|scene_prop_slot_ge, ":instance_id", scene_prop_prune_time, ":cur_time"),
							(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
							(call_script, "script_move_scene_prop_under_ground", ":instance_id", 0),
						(else_try),
							(prop_instance_get_position, pos_hit, ":instance_id"),
							(position_move_z, pos_hit, ":center_z"),
							(scene_prop_get_slot, ":burner_agent", ":instance_id", scene_prop_user_agent),
							(call_script, "script_server_missile_hit", ":burner_agent", missile_fire, 250, ":radius"),
						(try_end),
					(try_end),
				(else_try),
					(neg|multiplayer_is_dedicated_server),
					(scene_prop_slot_eq, ":instance_id", scene_prop_smoke_effect_done, 1),
					(scene_prop_set_slot, ":instance_id", scene_prop_smoke_effect_done, 0),
					(prop_instance_stop_all_particle_systems, ":instance_id"),
					(prop_instance_stop_sound, ":instance_id"),
				(try_end),
			(try_end),
			(val_mul, ":radius", 2),
			(val_mul, ":center_z", 2),
			(val_add, ":particle_1", 1),
			(val_add, ":particle_2", 1),
		(try_end),
	
		#serverside only
		(try_begin),
			(this_or_next|multiplayer_is_server),
			(neg|game_in_multiplayer_mode),
			
			#grenades
			(scene_prop_get_num_instances, ":num_instances", "spr_grenade"),
			(try_for_range, ":cur_instance", 0, ":num_instances"),
				(scene_prop_get_instance, ":instance_id", "spr_grenade", ":cur_instance"),
				(try_begin),
					(scene_prop_slot_eq, ":instance_id", scene_prop_is_used, 1),
					(neg|scene_prop_slot_eq, ":instance_id", scene_prop_prune_time, -1),
					(neg|scene_prop_slot_ge, ":instance_id", scene_prop_prune_time, ":cur_time"),
					(scene_prop_set_slot, ":instance_id", scene_prop_prune_time, -1),
					(prop_instance_get_position, pos_hit, ":instance_id"),
					(call_script, "script_multiplayer_server_explosion_at_position"),
					(scene_prop_get_slot, ":shooter_agent_no", ":instance_id", scene_prop_user_agent),
					(call_script, "script_server_missile_hit", ":shooter_agent_no", missile_bomb, 500, 300),
					(scene_prop_set_slot, ":instance_id", scene_prop_user_agent, -1),
					(call_script, "script_move_scene_prop_under_ground", ":instance_id", 0),
				(try_end),
			(try_end),
			
			#server announcement
			(try_begin),
				(multiplayer_is_server),
				(eq, "$g_server_announcements", 1),
				(le, "$g_next_announcement_time", ":cur_time"),
				(val_add, "$g_next_announcement_time", "$g_multiplayer_announcement_interval"),
				(str_store_string, s0, "str_server_announcement"),
				(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
			(try_end),
			
			#upkeep
			(try_begin),
				(multiplayer_is_dedicated_server),
				(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
				(eq, "$g_init_on_round_end", 1),
				(call_script, "script_multiplayer_server_init_upkeep", -1),
			(try_end),
			
			#thunder storm
			(try_begin),
			  (gt, "$g_thunderstorm", 0),
			  (store_random_in_range, ":random_val", 0, 10),
			  (try_begin),
				  (le, ":random_val", "$g_thunderstorm"),
					(store_div, ":max_x", "$g_scene_max_x", 200),
					(store_div, ":max_y", "$g_scene_max_y", 200),
					(val_add, ":max_x", 3001),
					(val_add, ":max_y", 3001),
					(store_sub, ":min_x", ":max_x", 6001),
					(store_sub, ":min_y", ":max_y", 6001),
					
					(store_random_in_range, "$g_thunderbolt_x", ":min_x", ":max_x"),
					(store_random_in_range, "$g_thunderbolt_y", ":min_y", ":max_y"),
					
					# (assign, reg1, ":min_x"),
					# (assign, reg2, ":max_x"),
					# (assign, reg3, ":min_y"),
					# (assign, reg4, ":max_y"),
					# (assign, reg5, "$g_thunderbolt_x"),
					# (assign, reg6, "$g_thunderbolt_y"),
					# (display_message, "@minX:{reg1} maxX:{reg2} minY:{reg3} maxY:{reg4} posX:{reg5} posY:{reg6}"),
					
					(val_mul, "$g_thunderbolt_x", 100),
					(val_mul, "$g_thunderbolt_y", 100),
					
					(store_random_in_range, ":ligthning_type", 0, 4),
					(try_begin),
						(gt, ":ligthning_type", 0),
						(store_random_in_range, "$g_thunderbolt_z", 62500, 87501),
					(else_try),
						(assign, "$g_thunderbolt_z", 0),
						(try_begin),
							(is_between, "$g_thunderbolt_x", 0, "$g_scene_max_x"),
							(is_between, "$g_thunderbolt_y", 0, "$g_scene_max_y"),
							(store_div, ":pos_x_m", "$g_thunderbolt_x", 100),
							(store_div, ":pos_y_m", "$g_thunderbolt_y", 100),

							(store_sub, ":min_hit_x", ":pos_x_m", 40),
							(store_add, ":max_hit_x", ":pos_x_m", 41),
							(store_sub, ":min_hit_y", ":pos_y_m", 40),
							(store_add, ":max_hit_y", ":pos_y_m", 41),
							
							(init_position, pos53),
							(set_fixed_point_multiplier, 100),
							(position_set_z, pos53, 25000),
							(assign, ":closest_dist", 25000),
							
							(try_for_range, ":i_x", ":min_hit_x", ":max_hit_x"),
								(store_mul, ":i_x_cm", ":i_x", 100),
								(try_for_range, ":i_y", ":min_hit_y", ":max_hit_y"),
									(store_mul, ":i_y_cm", ":i_y", 100),
									(position_set_x, pos53, ":i_x_cm"),
									(position_set_y, pos53, ":i_y_cm"),
									(position_get_distance_to_ground_level, ":cur_dist", pos53),
									(lt, ":cur_dist", ":closest_dist"),
									(assign, "$g_thunderbolt_x", ":i_x_cm"),
									(assign, "$g_thunderbolt_y", ":i_y_cm"),
									(assign, ":closest_dist", ":cur_dist"),
								(try_end),
							(try_end),
							(store_sub, "$g_thunderbolt_z", 25000, ":closest_dist"),
						(try_end),
					(try_end),
					
					(call_script, "script_thunderbolt_hit"),
					(try_begin),
						(multiplayer_is_server),
						(get_max_players, ":num_players"),
						(try_for_range, ":cur_player_no", 1, ":num_players"), #0 is server so starting from 1
							(player_is_active, ":cur_player_no"),
							(multiplayer_send_3_int_to_player, ":cur_player_no", multiplayer_event_return_thunderbolt, "$g_thunderbolt_x", "$g_thunderbolt_y", "$g_thunderbolt_z"),
						(try_end),
					(try_end),
				(try_end),
			(try_end),

			(assign, ":max_dist", 0),
			(assign, ":min_dist", 10000),#set min at first
			
			(try_for_agents, ":cur_agent"),
				(agent_is_active, ":cur_agent"),
				(agent_is_alive, ":cur_agent"),
				
				(call_script, "script_agent_drowning", ":cur_agent"),
				(try_begin),
					(neg|agent_slot_eq, ":cur_agent", slot_agent_bleed_time, 0),
					(neg|agent_slot_ge, ":cur_agent", slot_agent_bleed_time, ":cur_time"),
					(call_script, "script_agent_bleeding", ":cur_agent"),
				(try_end),
				
				#kick leechers when not being moved for ages
				(try_begin),
					(multiplayer_is_dedicated_server),
					(neg|agent_is_non_player, ":cur_agent"),
					(agent_get_player_id, ":cur_player", ":cur_agent"),
					(player_is_active, ":cur_player"),
					(neg|player_is_admin, ":cur_player"),
					(agent_get_speed, pos12, ":cur_agent"),
					(position_normalize_origin, ":speed", pos12),
					(try_begin),
						(lt, ":speed", 1),
						(agent_get_slot, ":elapsed_time", ":cur_agent", slot_agent_sec_since_movement),
						(val_add, ":elapsed_time", 1),
						(agent_set_slot, ":cur_agent", slot_agent_sec_since_movement, ":elapsed_time"),
						(try_begin),
							(gt, ":elapsed_time", 300),
							(agent_set_slot, ":cur_agent", slot_agent_sec_since_movement, 0),#reset
							(str_store_player_username, s1, ":cur_player"),
							(str_store_string, s0, "@{s1} has been kicked for leeching."),
							(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
							(server_add_message_to_log, s0),
							(kick_player, ":cur_player"),
						(try_end),
					(else_try),
						(neg|agent_slot_eq, ":cur_agent", slot_agent_sec_since_movement, 0),
						(agent_set_slot, ":cur_agent", slot_agent_sec_since_movement, 0),
					(try_end),
				(try_end),
			
				#NEW AI
				(agent_is_human, ":cur_agent"),
				(agent_is_non_player, ":cur_agent"),
				(agent_get_team, ":cur_team", ":cur_agent"),
				(agent_get_division, ":cur_class", ":cur_agent"),
				
				(assign, ":cur_group", -2),
				(try_begin),
					(game_in_multiplayer_mode),
					(agent_get_group, ":cur_group", ":cur_agent"),
				(else_try),
					(assign, ":cur_group", ":cur_team"),
				(try_end),
				
				###
				#(assign, reg52, ":cur_team"),
				#(display_message, "@agent team:{reg52}"),
				###
			
				(agent_ai_get_look_target, ":target_agent", ":cur_agent"),
				(agent_is_active, ":target_agent"),
				(agent_is_alive, ":target_agent"),
				(agent_is_human, ":target_agent"),
				(agent_get_team, ":target_team", ":target_agent"),

				(teams_are_enemies, ":cur_team", ":target_team"),
				(agent_get_position, pos10, ":cur_agent"),
				(agent_get_position, pos11, ":target_agent"),
				#(agent_get_look_position, pos11, ":cur_agent"),
				(get_distance_between_positions_in_meters, ":dist", pos10, pos11),
			
				#max range message
				(try_begin),
					(gt, ":dist", ":max_dist"),
					(assign, ":max_dist", ":dist"),
				(try_end),
				(try_begin),
					(lt, ":dist", ":min_dist"),
					(assign, ":min_dist", ":dist"),
				(try_end),
			
				#AI walking
				(assign, ":walk", 0),
				(store_add, ":cur_array", "trp_group_0_order_run", ":cur_class"),
				(try_begin),
					(this_or_next|eq, ":cur_group", -1),#no commander
					(troop_slot_eq, ":cur_array", ":cur_group", 0),
					(try_begin),
						(this_or_next|eq, ":cur_class", grc_infantry),
						(eq, ":cur_class", grc_archers),
						(store_random_in_range, ":rand", 50, 61),
						(gt, ":dist", ":rand"),
						(assign, ":walk", 1),
					(else_try),
						(eq, ":cur_class", grc_cavalry),
						(store_random_in_range, ":rand", 75, 86),
						(gt, ":dist", ":rand"),
						(assign, ":walk", 1),
					(try_end),
				(try_end),
				(try_begin),
					(eq, ":walk", 0),
					(store_agent_hit_points, ":agent_hp", ":cur_agent", 1),
					(le, ":agent_hp", agent_hp_cant_run),
					(assign, ":walk", 1),
				(try_end),
				(try_begin),
					(eq, ":walk", 0),
					(agent_slot_eq, ":cur_agent", slot_agent_is_walking, 1),
					(agent_slot_eq, ":cur_agent", slot_agent_key_walk, 1),
					(agent_set_slot, ":cur_agent", slot_agent_is_walking, 0),
					(agent_set_slot, ":cur_agent", slot_agent_key_walk, 0),
					(call_script, "script_agent_reset_speed", ":cur_agent"),
				(else_try),
					(eq, ":walk", 1),
					(agent_slot_eq, ":cur_agent", slot_agent_is_walking, 0),
					(agent_slot_eq, ":cur_agent", slot_agent_key_walk, 0),
					(call_script, "script_agent_walk", ":cur_agent", walk_by_key),   
				(try_end),

				#fire ammo
				(store_add, ":cur_array", "trp_group_0_order_fire", ":cur_class"),
				(agent_get_slot, ":ammo_id", ":cur_agent", slot_agent_spawn_ammo_type),
				(item_get_type, ":ammo_type", ":ammo_id"),
				(try_begin),
					(is_between, ":ammo_type", itp_type_arrows, itp_type_shield),
					(try_begin),
						(troop_slot_eq, ":cur_array", ":cur_group", 1),
						(assign, ":last_item", ":ammo_id"),
						(store_add, ":next_item", ":ammo_id", 1),
					(else_try),
					  (store_add, ":last_item", ":ammo_id", 1),
						(assign, ":next_item", ":ammo_id"),
					(try_end),
					
					(try_begin),
						(agent_has_item_equipped, ":cur_agent", ":last_item"),
						(assign, ":end_item_slot", 4),
						(try_for_range, ":cur_item_slot", 0, ":end_item_slot"),
							(agent_get_item_slot, ":cur_item", ":cur_agent", ":cur_item_slot"),
							(eq, ":cur_item", ":last_item"),
							(agent_get_ammo_for_slot, ":last_ammo_amount", ":cur_agent", ":cur_item_slot"),
							(store_add, ":slot_no", ":cur_item_slot", 1),
							(agent_unequip_item, ":cur_agent", ":last_item", ":slot_no"),
							(agent_equip_item, ":cur_agent", ":next_item", ":slot_no"),
							(agent_set_ammo, ":cur_agent", ":next_item", ":last_ammo_amount"),
							(assign, ":end_item_slot", -1),#break loop
						(try_end),
					(try_end),
				(try_end),
				
				#volley fire
				(store_add, ":cur_array", "trp_group_0_order_volley", ":cur_class"),
				(try_begin),
					(troop_slot_eq, ":cur_array", ":cur_group", 1),
					(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
					(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
					(neg|game_in_multiplayer_mode),
					(agent_get_wielded_item, ":weapon", ":cur_agent", 0),
					(try_begin),
						#(eq, ":cur_class", grc_archers),
						(gt, ":weapon", 0),
						(item_get_type, ":weapon_type", ":weapon"),
						
						(this_or_next|eq, ":weapon_type", itp_type_bow),
						(this_or_next|eq, ":weapon_type", itp_type_crossbow),
						(eq, ":weapon_type", itp_type_musket),

						(assign, ":action_state", 0),
						(assign, ":end_item_type", itp_type_bullets),
						(try_for_range, ":cur_item_type", itp_type_bow, ":end_item_type"),
							(eq, ":cur_item_type", ":weapon_type"),
							(try_begin),
								(eq, ":cur_item_type", itp_type_bow),
								(assign, ":cur_team_array_1", "trp_team_volley_check_time_bow"),
								(assign, ":cur_team_array_2", "trp_team_volley_shot_time_bow"),
							(else_try),
								(eq, ":cur_item_type", itp_type_crossbow),
								(assign, ":cur_team_array_1", "trp_team_volley_check_time_crossbow"),
								(assign, ":cur_team_array_2", "trp_team_volley_shot_time_crossbow"),			  
							(else_try),
								(eq, ":cur_item_type", itp_type_musket),
								(assign, ":cur_team_array_1", "trp_team_volley_check_time_musket"),
								(assign, ":cur_team_array_2", "trp_team_volley_shot_time_musket"),
							(try_end),
							(try_begin),
								(this_or_next|troop_slot_eq, ":cur_team_array_1", ":cur_group", ":cur_time"),
								(troop_slot_eq, ":cur_team_array_1", ":cur_group", ":real_time"),
								(assign, ":action_state", 1),
							(else_try),
								(this_or_next|troop_slot_eq, ":cur_team_array_2", ":cur_group", ":cur_time"),
								(troop_slot_eq, ":cur_team_array_2", ":cur_group", ":real_time"),
								(assign, ":action_state", 2),
							(try_end),
							(assign, ":end_item_type", -1),#break loop
						(try_end),
						###
						#(assign, reg50, ":cur_group"),
						#(assign, reg51, ":cur_item_type"),
						#(assign, reg52, ":action_state"),
						#(display_message, "@agent team:{reg50}  item_type:{reg51}  action_state:{reg52}"),
						###
						#(agent_get_attack_action, ":attack_action", ":cur_agent"),
						#(call_script, "script_workaround_agent_attack_action", ":cur_agent"),
						#(assign, ":attack_action", reg0),
							
						(try_begin),#volley check
							(eq, ":action_state", 1),
							(try_begin),
								#(neq, ":attack_action", 2),#releasing_attack
								#(neq, ":attack_action", 5),#reloading
								#(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
								(item_get_missile_speed, ":item_v0", ":weapon"),
								
								(try_begin),
									(eq, ":weapon_type", itp_type_bow),
									(store_mul, ":max_range", ":item_v0", range_arrow_ms),
								(else_try),
									(eq, ":weapon_type", itp_type_crossbow),
									(store_mul, ":max_range", ":item_v0", range_bolt_ms),
								(else_try),
									(store_mul, ":max_range", ":item_v0", range_bullet_ms),
								(try_end),
								###
								#(assign, reg60, ":max_range"),
								#(display_message, "@agent call 2b max range: {reg60}", 0xCCCCCC),
								###
								
								(troop_get_slot, ":team_archer_amount", "trp_team_archer_amount", ":cur_group"),
								(val_add, ":team_archer_amount", 1),
								(troop_set_slot, "trp_team_archer_amount", ":cur_group", ":team_archer_amount"),
										
								(agent_get_speed, pos12, ":cur_agent"),
								(position_normalize_origin, ":speed", pos12),
								(try_begin),
									(lt, ":speed", 1),
									(le, ":dist", ":max_range"),
									###
									# (display_message, "@agent call 3b", 0xCCCCCC),
									###
									(troop_get_slot, ":team_archer_target_in_range_amount", "trp_team_archer_target_in_range_amount", ":cur_group"),
									(val_add, ":team_archer_target_in_range_amount", 1),
									(troop_set_slot, "trp_team_archer_target_in_range_amount", ":cur_group", ":team_archer_target_in_range_amount"),
								(try_end),
							(try_end),
				
						(else_try),#volley shot
							(eq, ":action_state", 2),
							#(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
							(store_mul, ":next_action_time", ":cur_time", 1000),
							(val_add, ":next_action_time", 1000),
							(agent_set_slot, ":cur_agent", slot_agent_next_action_time, ":next_action_time"),
							#(try_begin),
								#(neq, ":attack_action", 2),#releasing_attack
								#(neq, ":attack_action", 5),#reloading
								#(agent_slot_eq, ":cur_agent", slot_agent_volley_fire_state, volley_free),
								#(agent_set_slot, ":cur_agent", slot_agent_volley_fire_state, volley_ready),
								#(assign, ":radomize_action_time", 1),
									###
									# (assign, reg50, ":cur_agent"),
									# (assign, reg51, ":next_action_time"),
									# (display_message, "@agent:{reg50} next action time:{reg51}"),
									###
							#(try_end),
						(try_end),
					(try_end),
				(try_end),
			(try_end),
		(try_end),
	  
		(this_or_next|multiplayer_is_server),
		(neg|game_in_multiplayer_mode),
		(try_begin),
			(game_in_multiplayer_mode),
			(get_max_players, ":end_team"),
		(else_try),
			(assign, ":end", 8),
			(try_for_range, ":end_team", 0, ":end"),
				(num_active_teams_le, ":end_team"),
				(assign, ":end", -1),#break loop
			(try_end),
			# (assign, reg1, ":end_team"),
			# (display_message, "@num teams {reg1}"),
		(try_end),
		
		(try_for_range, ":cur_team", 0, ":end_team"),
			(assign, ":team_active", 0),
			(try_begin),
				(game_in_multiplayer_mode),#multiplayer
				(try_begin),
					(player_is_active, ":cur_team"),
					(assign, ":team_active", 1),
				(try_end),
			(else_try),
				(assign, ":team_active", 1),
			(try_end), 

			(eq, ":team_active", 1),
			# (assign, reg50, ":cur_team"),
			# (display_message, "@team:{reg50} active"),
			
			(assign, ":times_applied", 0),
			(try_for_range, ":array", "trp_group_0_order_volley", "trp_group_9_order_volley"),
				(troop_slot_eq, ":array", ":cur_team", 1),
				
				(try_begin),
					(eq, ":times_applied", 0),
					(try_for_range, ":cur_array", "trp_team_volley_check_time_bow", "trp_team_volley_shot_time_bow"),
						(store_add, ":cur_array_2", ":cur_array", 3),
						(try_begin),
							(this_or_next|troop_slot_eq, ":cur_array", ":cur_team", ":cur_time"),
							(troop_slot_eq, ":cur_array", ":cur_team", ":real_time"),
							
							(troop_get_slot, ":team_archer_amount", "trp_team_archer_amount", ":cur_team"),
								###
								# (assign, reg50, ":cur_team"),
								# (assign, reg51, ":cur_array"),
								# (assign, reg52, ":team_archer_amount"),
								# (display_message, "@team:{reg50} group:{reg51} ready archers:{reg52}"),
								###		
							(try_begin),
								(ge, ":team_archer_amount", 1),
										###
										# (assign, reg50, ":cur_team"),
										# (assign, reg51, ":cur_array"),
										# (display_message, "@team:{reg50} group:{reg51} volley check 1"),
										###
								(try_begin),
									(gt, ":team_archer_amount", 1),
									(val_div, ":team_archer_amount", 2),
								(try_end),
								(troop_slot_ge, "trp_team_archer_target_in_range_amount", ":cur_team", ":team_archer_amount"),
										###
										# (assign, reg50, ":cur_team"),
										# (assign, reg51, ":cur_array"),
										# (display_message, "@team:{reg50} group:{reg51} shot"),
										###
								(try_begin),
									(eq, ":cur_array", "trp_team_volley_check_time_bow"),
									(assign, ":min", volley_interval_bow_min),
									(assign, ":max", volley_interval_bow_max),
								(else_try),
									(eq, ":cur_array", "trp_team_volley_check_time_crossbow"),
									(assign, ":min", volley_interval_crossbow_min),
									(assign, ":max", volley_interval_crossbow_max),
								(else_try),
									(assign, ":min", volley_interval_musket_min),
									(assign, ":max", volley_interval_musket_max),
								(try_end),
								(store_random_in_range, ":next_team_check_time", ":min", ":max"),
								(store_add, ":next_team_shot_time", ":cur_time", 1),#shot next second
								(troop_set_slot, ":cur_array_2", ":cur_team", ":next_team_shot_time"),
								(assign, ":times_applied", 1),
								#message
								###
								(try_begin),
									(eq, "$get_url_response", 0),
									(eq, "$g_show_debug_messages", 1),
									(assign, reg50, ":cur_time"),
									(assign, reg51, ":cur_team"),
									(assign, reg52, ":cur_array"),
									(assign, reg53, ":next_team_check_time"),
									#(assign, reg54, ":action_number"),
									(assign, reg55, ":max_dist"),
									(assign, reg56, ":min_dist"),
									(store_add, ":avg_dist", ":max_dist", ":min_dist"),
									(store_div, reg57, ":avg_dist", 2),
									(display_message, "@[DEBUG] time:{reg50} +1=shot | team:{reg51}/group:{reg52} | next volley in:{reg53} ^distance: max={reg55}m min={reg56}m avg={reg57}m", 0xCCCCCC),
								(try_end),
								###
							(else_try),
								(assign, ":next_team_check_time", 5),
							(try_end),
								
							(val_add, ":next_team_check_time", ":cur_time"),
							(try_begin),
								(neq, ":next_team_check_time", ":cur_time"),
								(troop_set_slot, ":cur_array", ":cur_team", ":next_team_check_time"),
									###
										# (assign, reg50, ":cur_time"),
										# (assign, reg51, ":cur_team"),
										# (assign, reg52, ":cur_array"),
										# (assign, reg53, ":next_team_check_time"),
										# (display_message, "@time:{reg50} team:{reg51} group:{reg52} next check time:{reg53}"),
									###
							(try_end),
						(try_end),	
					(try_end),
					#reset slot
					(troop_set_slot, "trp_team_archer_amount", ":cur_team", 0),
					(troop_set_slot, "trp_team_archer_target_in_range_amount", ":cur_team", 0),
				(try_end),
				
				# (assign, ":min_time_bow", ":cur_time"),
				# (assign, ":max_time_bow", ":cur_time"),
				# (assign, ":min_time_crossbow", ":cur_time"),
				# (assign, ":max_time_crossbow", ":cur_time"),
				# (assign, ":min_time_musket", ":cur_time"),
				# (assign, ":max_time_musket", ":cur_time"),
				# (try_for_range, ":cur_array", "trp_team_volley_shot_time_bow", "trp_team_archer_amount"),
					# (troop_get_slot, ":action_time", ":cur_array", ":cur_team"),
					# (store_add, ":min_time", ":action_time", 3),
					# (store_add, ":max_time", ":action_time", 7),
					# (try_begin),
						# (eq, ":cur_array", "trp_team_volley_shot_time_bow"),
						# (assign, ":min_time_bow", ":min_time"),
						# (assign, ":max_time_bow", ":max_time"),
					# (else_try),
						# (eq, ":cur_array", "trp_team_volley_shot_time_crossbow"),
						# (assign, ":min_time_crossbow", ":min_time"),
						# (assign, ":max_time_crossbow", ":max_time"),
					# (else_try),
						# (assign, ":min_time_musket", ":min_time"),
						# (assign, ":max_time_musket", ":max_time"),
					# (try_end),
				# (try_end),
				
				# (try_begin),
					# (this_or_next|is_between, ":cur_time", ":min_time_bow", ":max_time_bow"),
					# (this_or_next|is_between, ":cur_time", ":min_time_crossbow", ":max_time_crossbow"),
					# (is_between, ":cur_time", ":min_time_musket", ":max_time_musket"),
					# (assign, ":order", mordr_fire_at_will),
				# (else_try),
					# (assign, ":order", mordr_hold_fire),
				# (try_end),
				
				# (store_sub, ":cur_class", ":array", "trp_group_0_order_volley"),
				# (team_get_hold_fire_order, ":cur_order", ":cur_team", ":cur_class"),
				# (try_begin),
					# (eq, ":order", mordr_fire_at_will),
					# (eq, ":cur_order", aordr_hold_your_fire),
					# (set_show_messages, 0),
					# (team_give_order, ":cur_team", ":cur_class", ":order"),
					# (set_show_messages, 1),
				# (else_try),
					# (eq, ":order", mordr_hold_fire),
					# (eq, ":cur_order", aordr_fire_at_will),
					# (set_show_messages, 0),
					# (team_give_order, ":cur_team", ":cur_class", ":order"),
					# (set_show_messages, 1),
				# (try_end),
			(try_end),
		(try_end),
  ]),

#movement_type_limiter
	(0, 0, 0.5,
  [
    (neg|multiplayer_is_dedicated_server),
		(call_script, "script_cf_no_action_menus_active"),
		
    (this_or_next|game_key_clicked, gk_character_window),
    (game_key_clicked, gk_crouch),
    
		(call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (agent_is_active, ":player_agent"),
    (agent_is_alive, ":player_agent"),
    (agent_slot_eq, ":player_agent", slot_agent_is_sprinting, 0),
  ],
  [
    (call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (assign, ":script_id", -1),
		(assign, ":event_id", -1),
    (assign, ":message", -1),
    (try_begin),
      (game_key_clicked, gk_character_window),#walk
      (assign, ":script_id", "script_agent_walk"),
			(assign, ":event_id", multiplayer_event_agent_walk),
      (assign, ":message", walk_by_key),
    (else_try),
      (game_key_clicked, gk_crouch),#sprint
      (this_or_next|game_key_is_down, gk_move_forward),
			(this_or_next|game_key_is_down, gk_move_backward),
			(this_or_next|game_key_is_down, gk_move_left),
			(game_key_is_down, gk_move_right),
			(try_begin),
				(agent_slot_eq, ":player_agent", slot_agent_is_walking, 0),
				(agent_slot_ge, ":player_agent", slot_agent_cur_stamina, agent_sprint_stamina +1),

	    #		(agent_get_attack_action, ":attack_action", ":player_agent"),
# 				(call_script, "script_workaround_agent_attack_action", ":player_agent"),
# 				(agent_get_defend_action, ":defend_action", ":player_agent"),
# #     	 	(agent_get_wielded_item, ":weapon_r", ":player_agent", 0),#right hand
# #      		(agent_get_wielded_item, ":weapon_l", ":player_agent", 1),#left hand
# 				(neq, reg0, 1),#readying_attack
# 				(neq, reg0, 2),#releasing_attack
# 				(neq, reg0, 5),#reloading
# 				(neq, ":defend_action", 1),#parrying
# 				(neq, ":defend_action", 2),#blocking
#        (eq, ":weapon_r", -1),
#        (eq, ":weapon_l", -1),
        (assign, ":script_id", "script_agent_sprint_limiter"),
				(assign, ":event_id", multiplayer_event_agent_sprint_limiter),
      # (else_try),
				# (display_message, "str_cant_sprint", 0xFFFFFF),
      (try_end),
    (try_end),

    (gt, ":script_id", -1),
		(gt, ":event_id", -1),
    (try_begin),
      (game_in_multiplayer_mode),
      (neg|multiplayer_is_server),
      (try_begin),
        (gt, ":message", -1),
        (multiplayer_send_int_to_server, ":event_id", ":message"),
      (else_try),
        (multiplayer_send_message_to_server, ":event_id"),
      (try_end),
    (else_try),
      (try_begin),
        (gt, ":message", -1),
        (call_script, ":script_id", ":player_agent", ":message"),
      (else_try),
        (call_script, ":script_id", ":player_agent"),
      (try_end),
    (try_end),
  ]),

#shield bash
	(0, 0, 2,
  [
    (neg|multiplayer_is_dedicated_server),
    (game_key_is_down, gk_defend),
    (game_key_clicked, gk_quests_window),
	(call_script, "script_cf_no_action_menus_active"),
  ],
  [
    (call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (agent_is_active, ":player_agent"),
    (agent_is_alive, ":player_agent"),

    (agent_get_wielded_item, ":item_id", ":player_agent", 1),
    (gt, ":item_id", -1),
    (item_get_type, ":item_type", ":item_id"),
    (eq, ":item_type", itp_type_shield),
    (agent_get_defend_action, ":defend_action", ":player_agent"),
    (eq, ":defend_action", 2), #Blocking.
    (agent_get_horse, ":horse", ":player_agent"),
    (le, ":horse", 0),
		
		(agent_get_animation, ":anim", ":player_agent", 0),
		(neg|is_between, ":anim", "anim_jump", "anim_stand_unarmed"),#no jumping
		(neq, ":anim", "anim_shield_bash"),#no bashing
		(neq, ":anim", "anim_kick_right_leg"),#no kicking
    
		(call_script, "script_agent_lose_stamina", ":player_agent", stamina_attack_shield),
		(try_begin),
			(game_in_multiplayer_mode),
			(neg|multiplayer_is_server),
			(multiplayer_send_message_to_server, multiplayer_event_shield_bash),
		(else_try),
			(call_script, "script_shield_bash", ":player_agent"),
		(try_end),
  ]),


	(0, 0, 0,
  	[
    	(neg|multiplayer_is_dedicated_server),
		(this_or_next|game_key_clicked, gk_defend),
    	(game_key_clicked, gk_toggle_weapon_mode),
		(call_script, "script_cf_no_action_menus_active"),
  	],
  	[
		(call_script, "script_client_get_player_agent"),
    	(assign, ":player_agent", reg0),
    	(agent_is_active, ":player_agent"),
    	(agent_is_alive, ":player_agent"),

    	(agent_get_wielded_item, ":item_r", ":player_agent", 0),
		(try_begin),
			(gt, ":item_r", -1),
			(try_begin),
				(game_key_clicked, gk_toggle_weapon_mode),
				(try_begin),
					(store_add, ":next_item", ":item_r", 1),
					(this_or_next|item_slot_eq, ":item_r", slot_item_is_alt_mode, 2),
					(item_slot_eq, ":next_item", slot_item_is_alt_mode, 2),
					(try_begin),
						(game_in_multiplayer_mode),
						(neg|multiplayer_is_server),
						(multiplayer_send_message_to_server, multiplayer_event_switch_weapon_mode),
					(else_try),
						(call_script, "script_switch_weapon_mode", ":player_agent"),
					(try_end),
				(try_end),
			(else_try),
				(game_key_clicked, gk_defend),
				(eq, ":item_r", itm_engineer_hammer),
				(call_script, "script_cf_no_action_menus_active"),
				(start_presentation,"prsnt_multiplayer_construct"),
			(try_end),
		(try_end),
  ]),

#TEST
	# (0, 0, 0,
 #  	[
 #    	(neg|multiplayer_is_dedicated_server),
 #  	],
 #  	[
	# 	(call_script, "script_client_get_player_agent"),
 #    	(assign, ":player_agent", reg0),
 #    	(agent_is_active, ":player_agent"),
 #    	(agent_is_alive, ":player_agent"),

 #    	(agent_get_wielded_item, ":item", ":player_agent", 0),


	# 	(eq, ":item", itm_long_bow),

	# 	(assign, ":end_slot", ek_head),
	# 	(try_for_range, ":cur_slot", ek_item_0, ":end_slot"),
	# 	    (agent_get_item_slot, ":slot_item", ":player_agent", ":cur_slot"),
	# 	    (item_get_type, ":slot_type", ":slot_item"),
	#         (eq, ":slot_type", itp_type_arrows),
	# 		(assign, ":end_slot", -1),
	# 	(try_end),

	# 	(eq, ":end_slot", -1),#has ammo

	# 	(agent_get_wielded_item_slot_no, ":slot", ":player_agent"),#WSE
	# 	(val_add, ":slot", bmm_item_1),

	# 	(agent_get_animation, ":anim", ":player_agent", 1),
	# 	(agent_get_animation_progress, ":progress", ":player_agent", 1),

	# 	(try_begin),
	# 		(eq, ":anim", "anim_ready_bow"),
	# 		(agent_body_meta_mesh_set_vertex_keys_time_point, ":player_agent", ":slot", ":progress"),#WSE
	# 	(try_end),

	# 	(try_begin),
	# 		(game_key_is_down, gk_attack),
	# 		(try_begin),
	# 			(neq, ":anim", "anim_ready_bow"),
	# 			(agent_set_animation, ":player_agent", "anim_ready_bow", 1),
	# 		(try_end),
	# 	(else_try),
	# 		(eq, ":progress", 100),
	# 		(eq, ":anim", "anim_ready_bow"),
	# 		(agent_get_bone_position, pos1, ":player_agent", hb_item_l, 1),
	# 		(position_move_y, pos1, 6),
	# 		(position_move_z, pos1, 2),
	# 		(agent_get_look_position, pos0, ":player_agent"),
	# 		(position_copy_rotation, pos2, pos0),
	# 		(position_copy_origin, pos2, pos1),
	# 		(item_get_food_quality, ":velocity", ":item"),
	# 		(set_fixed_point_multiplier, 1),
 #    		(add_missile, ":player_agent", pos2, ":velocity", ":item", 0, ":slot_item", 0),
	# 		(agent_set_animation, ":player_agent", "anim_release_bow", 1),
	# 		(agent_body_meta_mesh_set_vertex_keys_time_point, ":player_agent", ":slot", 0),#WSE
	# 	(try_end),
	# ]),
#TEST


	(0, 0, 0.25,
  [
    (neg|multiplayer_is_dedicated_server),
    (this_or_next|game_key_is_down, gk_move_left),
    (this_or_next|game_key_is_down, gk_move_right),
    (this_or_next|game_key_is_down, gk_move_forward),
    (this_or_next|game_key_is_down, gk_move_backward),
    (this_or_next|game_key_is_down, gk_attack),
    (game_key_is_down, gk_defend),
    
    (call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
    (agent_is_active, ":player_agent"),
    (agent_is_alive, ":player_agent"),
		
    (agent_get_slot, ":instance_id", ":player_agent", slot_agent_use_scene_prop),
		(prop_instance_is_valid, ":instance_id"),

		(assign, ":continue", 0),
    (try_begin),
      (game_in_multiplayer_mode),
      (agent_get_player_id, ":player_no", ":player_agent"),
      (neg|player_is_busy_with_menus, ":player_no"),
			(assign, ":continue", 1),
		(else_try),
			(assign, ":continue", 1),
    (try_end),
		(eq, ":continue", 1),
  ],
  [
    (assign, ":command", -1),
		(call_script, "script_client_get_player_agent"),
    (assign, ":player_agent", reg0),
		(agent_get_slot, ":instance_id", ":player_agent", slot_agent_use_scene_prop),
		(scene_prop_get_slot, ":rot_x", ":instance_id", scene_prop_rot_x),
	
    (try_begin),#left
      (game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_right),
      (neg|game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_backward),
			(try_begin),#slow movement
				(game_key_is_down, gk_zoom),
				(assign, ":command", prop_move_left_alt),
			(else_try),
				(assign, ":command", prop_move_left),
			(try_end),
	  
    (else_try),#right
      (game_key_is_down, gk_move_right),
      (neg|game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_backward),
			(try_begin),#slow movement
				(game_key_is_down, gk_zoom),
				(assign, ":command", prop_move_right_alt),
			(else_try),
				(assign, ":command", prop_move_right),
			(try_end),
	  
    (else_try),#forward
      (game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_backward),
      (neg|game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_right),
			(lt, ":rot_x", cannon_1_barrel_max_angle),
			(try_begin),#slow movement
				(game_key_is_down, gk_zoom),
				(assign, ":command", prop_move_forward_alt),
			(else_try),
				(assign, ":command", prop_move_forward),
			(try_end),
	  
    (else_try),#backward
      (game_key_is_down, gk_move_backward),
      (neg|game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_right),
			(gt, ":rot_x", cannon_1_barrel_min_angle),
			(try_begin),#slow movement
				(game_key_is_down, gk_zoom),
				(assign, ":command", prop_move_backward_alt),
			(else_try),
				(assign, ":command", prop_move_backward),
			(try_end),
	  
    (else_try),#left forward
      (game_key_is_down, gk_move_left),
      (game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_right),
      (neg|game_key_is_down, gk_move_backward),
			(try_begin),
				(lt, ":rot_x", cannon_1_barrel_max_angle),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_left_forward_alt),
				(else_try),
					(assign, ":command", prop_move_left_forward),
				(try_end),
			(else_try),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_left_alt),
				(else_try),
					(assign, ":command", prop_move_left),
				(try_end),
			(try_end),
	  
    (else_try),#left backward
      (game_key_is_down, gk_move_left),
      (game_key_is_down, gk_move_backward),
      (neg|game_key_is_down, gk_move_right),
      (neg|game_key_is_down, gk_move_forward),
			(try_begin),
				(gt, ":rot_x", cannon_1_barrel_min_angle),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_left_backward_alt),
				(else_try),
					(assign, ":command", prop_move_left_backward),
				(try_end),
			(else_try),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_left_alt),
				(else_try),
					(assign, ":command", prop_move_left),
				(try_end),			
			(try_end),
			
    (else_try),#right forward
      (game_key_is_down, gk_move_right),
      (game_key_is_down, gk_move_forward),
      (neg|game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_backward),
			(try_begin),
				(lt, ":rot_x", cannon_1_barrel_max_angle),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_right_forward_alt),
				(else_try),
					(assign, ":command", prop_move_right_forward),
				(try_end),
			(else_try),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_right_alt),
				(else_try),
					(assign, ":command", prop_move_right),
				(try_end),			
			(try_end),
	  
    (else_try),#right backward
      (game_key_is_down, gk_move_right),
      (game_key_is_down, gk_move_backward),
      (neg|game_key_is_down, gk_move_left),
      (neg|game_key_is_down, gk_move_forward),
			(try_begin),
				(gt, ":rot_x", cannon_1_barrel_min_angle),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_right_backward_alt),
				(else_try),
					(assign, ":command", prop_move_right_backward),
				(try_end),
			(else_try),
				(try_begin),#slow movement
					(game_key_is_down, gk_zoom),
					(assign, ":command", prop_move_right_alt),
				(else_try),
					(assign, ":command", prop_move_right),
				(try_end),			
			(try_end),
	  
    (else_try),#action 1
      (game_key_is_down, gk_attack),
      (neg|game_key_is_down, gk_defend),
      (assign, ":command", prop_action_1),
	  
		(else_try),#action 2
      (game_key_is_down, gk_defend),
      (neg|game_key_is_down, gk_attack),
      (assign, ":command", prop_action_2),
    (try_end),

    (gt, ":command", -1),
    (try_begin),
      (game_in_multiplayer_mode),
			(neg|multiplayer_is_server),
      (multiplayer_send_int_to_server, multiplayer_event_server_control_scene_prop, ":command"),
    (try_end),
    (call_script, "script_control_scene_prop", ":instance_id", ":command"),
	#
	  #(store_mission_timer_a_msec, reg1),
	  #(display_message, "@control time: {reg1}"),
	#
  ]),

	(0, 0, 0, 
  	[
		
  	], 
  	[
		(store_mission_timer_a_msec, ":cur_time"),
		(set_fixed_point_multiplier, 100),

		# (try_for_agents, ":agent", 0, 0),
		# 	(agent_get_slot, ":time", ":agent", slot_agent_time_ms),
		# 	(ge, ":cur_time", ":time"),
		# 	# (agent_get_position, pos0, ":agent"),
		# 	# (particle_system_burst_no_sync, "psys_fire_xs", pos0, 1),
		# 	(val_add, ":time", 1000),
		# 	(agent_set_slot, ":agent", slot_agent_time_ms, ":time"),
		# (try_end),


		# (try_for_prop_instances, ":instance", "spr_arrow_missile", somt_temporary_object),
		# 	(scene_prop_slot_eq, ":instance", scene_prop_is_used, 1),
			
		# 	(scene_prop_get_slot, ":elapsed_time", ":instance", scene_prop_last_pos_time),
		# 	(scene_prop_set_slot, ":instance", scene_prop_last_pos_time, ":cur_time"),
		# 	(store_sub, ":elapsed_time", ":cur_time", ":elapsed_time"),

		# 	(scene_prop_get_slot, ":pos_x", ":instance", scene_prop_pos_x),
		# 	(scene_prop_get_slot, ":pos_y", ":instance", scene_prop_pos_y),
		# 	(scene_prop_get_slot, ":pos_z", ":instance", scene_prop_pos_z),

		# 	(scene_prop_get_slot, ":vel_x", ":instance", scene_prop_move_x),
		# 	(scene_prop_get_slot, ":vel_y", ":instance", scene_prop_move_y),
		# 	(scene_prop_get_slot, ":vel_z", ":instance", scene_prop_move_z),

		# 	(position_set_x, pos0, ":vel_x"),
		# 	(position_set_y, pos0, ":vel_y"),
		# 	(position_set_z, pos0, ":vel_z"),
		# 	(position_normalize_origin, ":vel", pos0),

		# 	(assign, ":gravity", 9810),
		# 	(scene_prop_get_slot, ":friction", ":instance", scene_prop_friction),

		# 	(set_fixed_point_multiplier, 1000),
		# 	(fld, fp0, ":vel"),
		# 	(fld, fp1, ":vel_x"),
		# 	(fld, fp2, ":vel_y"),
		# 	(fld, fp3, ":vel_z"),
		# 	(fld, fp4, ":friction"),
		# 	(fld, fp5, ":gravity"),
		# 	(fld, fp6, ":elapsed_time"),

		# 	(fmul, fp127, fp1, fp0),
		# 	(fmul, fp127, fp127, fp4),
		# 	(fmul, fp127, fp127, fp6),
		# 	(fsub, fp127, fp1, fp127),
		# 	(fst, ":vel_x_new", fp127),

		# 	(fmul, fp127, fp2, fp0),
		# 	(fmul, fp127, fp127, fp4),
		# 	(fmul, fp127, fp127, fp6),
		# 	(fsub, fp127, fp2, fp127),
		# 	(fst, ":vel_y_new", fp127),

		# 	(fmul, fp127, fp3, fp0),
		# 	(fmul, fp127, fp127, fp4),
		# 	(fadd, fp127, fp127, fp5),
		# 	(fmul, fp127, fp127, fp6),
		# 	(fsub, fp127, fp3, fp127),
		# 	(fst, ":vel_z_new", fp127),

		# 	(scene_prop_set_slot, ":instance", scene_prop_move_x, ":vel_x_new"),
		# 	(scene_prop_set_slot, ":instance", scene_prop_move_y, ":vel_y_new"),
		# 	(scene_prop_set_slot, ":instance", scene_prop_move_z, ":vel_z_new"),

		# 	(store_add, ":offset_x", ":vel_x", ":vel_x_new"),
		# 	(store_add, ":offset_y", ":vel_y", ":vel_y_new"),
		# 	(store_add, ":offset_z", ":vel_z", ":vel_z_new"),
		# 	(val_mul, ":offset_x", ":elapsed_time"),
		# 	(val_mul, ":offset_y", ":elapsed_time"),
		# 	(val_mul, ":offset_z", ":elapsed_time"),
		# 	(val_div, ":offset_x", 2000),
		# 	(val_div, ":offset_y", 2000),
		# 	(val_div, ":offset_z", 2000),
		# 	(store_add, ":pos_x_new", ":pos_x", ":offset_x"),
		# 	(store_add, ":pos_y_new", ":pos_y", ":offset_y"),
		# 	(store_add, ":pos_z_new", ":pos_z", ":offset_z"),
		# 	(scene_prop_set_slot, ":instance", scene_prop_pos_x, ":pos_x_new"),
		# 	(scene_prop_set_slot, ":instance", scene_prop_pos_y, ":pos_y_new"),
		# 	(scene_prop_set_slot, ":instance", scene_prop_pos_z, ":pos_z_new"),

		# 	(fld, fp7, ":offset_x"),
		# 	(fld, fp8, ":offset_y"),
		# 	(fld, fp9, ":offset_z"),
		# 	(fld, fp10, -1000),#negate dummy

		# 	(fmul, fp126, fp7, fp7),
		# 	(fmul, fp127, fp8, fp8),
		# 	(fmul, fp127, fp127, fp10),
		# 	(fsub, fp127, fp127, fp126),
		# 	(fsqrt, fp127, fp127),
		# 	(fst, ":length", fp127),

		# 	(fdiv, fp127, fp9, fp127),
		# 	(fatan, fp127, fp127),
		# 	(fst, ":rot_x", fp127),

		# 	(fdiv, fp127, fp7, fp8),
		# 	(fatan, fp127, fp127),
		# 	(fst, ":rot_z", fp127),

		# 	(init_position, pos1),
		# 	(position_set_x, pos1, ":pos_x_new"),
		# 	(position_set_y, pos1, ":pos_y_new"),
		# 	(position_set_z, pos1, ":pos_z_new"),
		# 	(position_rotate_x_floating, pos1, ":rot_x"),
		# 	(position_rotate_z_floating, pos1, ":rot_z"),
		# 	(prop_instance_set_position, ":instance", pos1, 1),

		# 	(set_fixed_point_multiplier, 100),
		# 	(try_begin),
		# 		(eq, "$frist_time", 1),
		# 		(assign, "$frist_time", 0),
		# 		(assign, reg0, ":elapsed_time"),
		# 		(assign, reg1, ":offset_x"),
		# 		(assign, reg2, ":offset_y"),
		# 		(assign, reg3, ":offset_z"),
		# 		(assign, reg4, ":rot_x"),
		# 		(assign, reg5, ":rot_z"),
		# 		(assign, reg6, ":length"),
		# 		(display_message,"@et:{reg0} x:{reg1} y:{reg2} z:{reg3} rotX:{reg4} rotZ:{reg5} length:{reg6}"),
		# 	(try_end),
		# (try_end),


		# (assign, "$last_frame_time", ":cur_time"),

		(neg|multiplayer_is_dedicated_server),

		# (try_begin),
		# 	(is_camera_in_first_person),
		# 	(call_script, "script_client_get_player_agent"),
		# 	(agent_is_active, reg0),
		# 	(agent_is_alive, reg0),
		# 	(assign, ":player_agent", reg0),
		# 	(agent_get_wielded_item, ":item", ":player_agent", 0),
		# 	(gt, ":item", -1),

		# 	(item_get_type, ":type", ":item"),
		# 	(this_or_next|is_between, ":type", itp_type_bow, itp_type_thrown),
		# 	(is_between, ":type", itp_type_pistol, itp_type_bullets),

		# 	(assign, ":info_box", 0),

		# 	(call_script, "script_workaround_agent_attack_action", ":player_agent"),
		# 	(try_begin),
		# 		(eq, reg0, 1),#readying_attack
		# 		(eq, "$custom_camera", 0),
		# 		(agent_get_animation_progress, reg1, ":player_agent", 1),
		# 		(try_begin),
		# 			(eq, ":type", itp_type_bow),
		# 			(is_between, reg1, 90, 100),#fix 100 sometimes on start
		# 			(assign, ":continue", 1),
		# 		(else_try),
		# 			(this_or_next|eq, ":type", itp_type_crossbow),
  #         			(eq, ":type", itp_type_musket),
  #         			(is_between, reg1, 20, 100),#fix 100 sometimes on start
		# 			(assign, ":continue", 1),
		# 		(else_try),
		# 			(eq, ":type", itp_type_pistol),
		# 			(is_between, reg1, 80, 100),#fix 100 sometimes on start
		# 			(assign, ":continue", 1),
		# 		(else_try),
		# 			(assign, ":continue", 0),
		# 		(try_end),
		# 		(try_begin),
		# 			(eq, ":continue", 1),
		# 			# (display_message, "@on {reg1}"),
		# 			(assign, "$custom_camera", 1),
		# 			(mission_cam_set_mode, 1, 0, 0),
		# 			(assign, ":info_box", 1),
		# 		(try_end),
		# 	(else_try),
		# 		(neq, reg0, 1),#readying_attack
		# 		(neq, "$custom_camera", 0),
		# 		# (display_message, "@off"),
		# 		(assign, "$custom_camera", 0),
		# 		(mission_cam_set_mode, 0, 250, 0),
		# 	(try_end),

		# 	(eq, "$custom_camera", 1),
		# 	(try_begin),
		# 		(game_key_clicked, gk_quests_window),
		# 		(gt, "$camera_height", 4),
		# 		(val_sub, "$camera_height", 1),
		# 		(assign, ":info_box", 1),
		# 	(else_try),
		# 		(game_key_clicked, gk_kick),
		# 		(lt, "$camera_height", 12),
		# 		(val_add, "$camera_height", 1),
		# 		(assign, ":info_box", 1),
		# 	(try_end),

		# 	(try_begin),
		# 		(eq, ":info_box", 1),
		# 		(assign, reg0, "$camera_height"),
		# 		(game_key_get_mapped_key_name, s0, gk_quests_window),
		# 		(game_key_get_mapped_key_name, s1, gk_kick),
		# 		(tutorial_message, "@Height: {reg0}. Press [{s0}] or [{s1}] to adjust.", 0, 3),
		# 	(try_end),

		# 	(try_begin),
		# 		(eq, ":type", itp_type_bow),
		# 		(agent_get_bone_position, pos1, ":player_agent", hb_item_l, 1),

		# 		(position_move_y, pos1, 6),
		# 		(position_move_z, pos1, 2),
		# 	(else_try),
		# 		(agent_get_bone_position, pos1, ":player_agent", hb_item_r, 1),
		# 		(try_begin),
		# 			(eq, ":type", itp_type_crossbow),
		# 			(position_move_x, pos1, -1),
		# 			(position_move_z, pos1, -3),
		# 		(else_try),
		# 			(eq, ":type", itp_type_musket),
		# 			(try_begin),
		# 				(this_or_next|eq, ":item", itm_handgonne_a),
		# 				(this_or_next|eq, ":item", itm_handgonne_a +loom_1_item),
		# 				(this_or_next|eq, ":item", itm_handgonne_a +loom_2_item),
		# 				(eq, ":item", itm_handgonne_a +loom_3_item),
		# 				(position_move_x, pos1, -3),
		# 			(else_try),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_1),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_1 +loom_1_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_1 +loom_2_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_1 +loom_3_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_2),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_2 +loom_1_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_2 +loom_2_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_2 +loom_3_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_3),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_3 +loom_1_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_3 +loom_2_item),
		# 				(this_or_next|eq, ":item", itm_matchlock_arquebus_3 +loom_3_item),
		# 				(eq, ":item", itm_autofire_musket),
		# 				(position_move_x, pos1, -1),
		# 			(try_end),
		# 		(else_try),
		# 			(position_move_x, pos1, -7),
		# 		(try_end),
		# 	(try_end),
		# 	(agent_get_look_position, pos0, ":player_agent"),
		# 	(position_copy_rotation, pos2, pos0),
		# 	(position_copy_origin, pos2, pos1),

		# 	(position_get_rotation_around_y, ":rot", pos1),
		# 	(val_mul, ":rot", -1),
		# 	(position_rotate_y, pos1, ":rot"),

		# 	(try_begin),
		# 		(eq, ":type", itp_type_bow),
		# 		(position_move_y, pos2, -60),
		# 	(else_try),
		# 		(eq, ":type", itp_type_pistol),
		# 		(position_move_y, pos2, -75),
		# 	(else_try),
		# 		(position_move_y, pos2, 3),
		# 	(try_end),
		# 	(position_move_z, pos1, "$camera_height"),

		# 	(mission_cam_set_position, pos1),
		# 	(try_begin),
		# 		(game_key_is_down, gk_zoom),
		# 		(mission_cam_set_aperture, 37),
		# 	(else_try),
		# 		(mission_cam_set_aperture, 75),
		# 	(try_end),
		# (try_end),
    	

		#lightning
		(try_begin),
			(gt, "$g_thunderbolt_end_time", -1),
			(try_begin),
				(lt, "$g_thunderbolt_end_time", ":cur_time"),
				(assign, "$g_thunderbolt_end_time", -1),
				(assign, "$g_thunderbolt_next_time", -1),
				(set_shader_param_float4, "@vLightning", 0, 0, 0, 0),
			(else_try),
				(lt, "$g_thunderbolt_next_time", ":cur_time"),
				# (assign, reg1, ":cur_time"),
				# (display_message, "@{reg1}"),
				(try_begin),
					(eq, "$g_thunderbolt_state", 1),
					
					(store_div, ":min_x", "$g_thunderbolt_x", 100),
					(val_sub, ":min_x", 125),
					(store_add, ":max_x", ":min_x", 251),
					(store_random_in_range, ":pos_x", ":min_x", ":max_x"),
					(val_mul, ":pos_x", 100),
					
					(store_div, ":min_y", "$g_thunderbolt_y", 100),
					(val_sub, ":min_y", 125),
					(store_add, ":max_y", ":min_y", 251),
					(store_random_in_range, ":pos_y", ":min_y", ":max_y"),
					(val_mul, ":pos_y", 100),
					
					(store_div, ":min_z", "$g_thunderbolt_z", 100),
					(val_sub, ":min_z", 125),
					(store_add, ":max_z", ":min_z", 251),
					(store_random_in_range, ":pos_z", ":min_z", ":max_z"),
					(val_mul, ":pos_z", 100),
					
					(store_random_in_range, ":strength", 25, 101),
					(set_shader_param_float4, "@vLightning", ":pos_x", ":pos_y", ":pos_z", ":strength"),
					(assign, "$g_thunderbolt_state", 0),
				(else_try),
					(set_shader_param_float4, "@vLightning", 0, 0, 0, 0),
					(assign, "$g_thunderbolt_state", 1),
				(try_end),
				(store_random_in_range, ":next_action", 25, 101),
				(store_add, "$g_thunderbolt_next_time", ":cur_time", ":next_action"),
			(try_end),
		(try_end),
		
		#sound channels
		(mission_cam_get_position, pos54),
		(try_for_range, ":cur_sound_channel", "trp_sound_channel_0", "trp_sound_channels_end"),
			(troop_get_slot, ":sound_id", ":cur_sound_channel", sound_slot_id),
			
			(gt, ":sound_id", -1),
			(troop_get_slot, ":time", ":cur_sound_channel", sound_slot_time),
			(troop_get_slot, reg0, ":cur_sound_channel", sound_slot_pos_x),
			(troop_get_slot, reg1, ":cur_sound_channel", sound_slot_pos_y),
			(troop_get_slot, reg2, ":cur_sound_channel", sound_slot_pos_z),
			(position_set_x, pos53, reg0),
			(position_set_y, pos53, reg1),
			(position_set_z, pos53, reg2),

			(get_distance_between_positions, ":distance", pos53, pos54),
			(store_sub, ":elapsed_time", ":cur_time", ":time"),
			(store_mul, ":reached_distance", ":elapsed_time", 34),#340m/s at 20C

			(ge, ":reached_distance", ":distance"),
			(try_begin),#assign distant sounds
				(this_or_next|eq, ":sound_id", "snd_release_gun"),
				(eq, ":sound_id", "snd_thunder_close"),
				(try_begin),
					(eq, ":sound_id", "snd_release_gun"),
					(assign, ":dist_adder", 5000),
				(else_try),
					(assign, ":dist_adder", 100000),
				(try_end),
				(assign, ":next_sound_dist", 0),
				(assign, ":end", 3),
				(try_for_range, ":i", 1, ":end"),
					(store_mul, ":add_dist", ":dist_adder", ":i"),
					(val_add, ":next_sound_dist", ":add_dist"),
					(try_begin),
						(gt, ":distance", ":next_sound_dist"),
						(val_add, ":sound_id", 1),
					(else_try),
						(assign, ":end", -1),#break loop
					(try_end),
				(try_end),
			(try_end),
			(play_sound_at_position, ":sound_id", pos53, 0),
			(troop_set_slot, ":cur_sound_channel", sound_slot_id, -1),#clear sound channel
				
			# (eq, "$g_show_debug_messages", 1),
			# (assign, reg60, ":sound_id"),
			# (store_div, reg61, ":distance", 100),
			# (store_mod, reg62, ":distance", 100),
			# (try_begin),
			# 	(lt, reg62, 10),
			# 	(str_store_string, s1, "@{reg61}.0{reg62}"),
			# (else_try),
			# 	(str_store_string, s1, "@{reg61}.{reg62}"),
			# (try_end),
			# (store_div, reg63, ":elapsed_time", 1000),
			# (store_mod, reg64, ":elapsed_time", 1000),
			# (try_begin),
			# 	(lt, reg64, 100),
			# 	(try_begin),
			# 		(lt, reg64, 10),
			# 		(str_store_string, s2, "@{reg63}.00{reg64}"),
			# 	(else_try),
			# 		(str_store_string, s2, "@{reg63}.0{reg64}"),
			# 	(try_end),
			# (else_try),
			# 	(str_store_string, s2, "@{reg63}.{reg64}"),
			# (try_end),
			# (display_message, "@[DEBUG] sound: {reg60} | distance: {s1}m | elapsed time: {s2}sec"),
		(try_end),
		
		#advanced AI orders
		(try_begin),
			(this_or_next|game_key_is_down, gk_order_1),
			# (this_or_next|game_key_released, gk_order_1),#WSE
			# (this_or_next|game_key_released, gk_order_2),#WSE
			# (this_or_next|game_key_released, gk_order_3),#WSE
			# (this_or_next|game_key_released, gk_order_4),#WSE
			(this_or_next|game_key_clicked, gk_order_1),
			(this_or_next|game_key_clicked, gk_order_2),
			(this_or_next|game_key_clicked, gk_order_3),
			(this_or_next|game_key_clicked, gk_order_4),
			#REWORK
			(this_or_next|game_key_clicked, gk_everyone_hear),
			(this_or_next|game_key_clicked, gk_group0_hear),
			(this_or_next|game_key_clicked, gk_group1_hear),
			(this_or_next|game_key_clicked, gk_group2_hear),
			(this_or_next|game_key_clicked, gk_group3_hear),
			(this_or_next|game_key_clicked, gk_group4_hear),
			(this_or_next|game_key_clicked, gk_group5_hear),
			(this_or_next|game_key_clicked, gk_group6_hear),
			(this_or_next|game_key_clicked, gk_group7_hear),
			(this_or_next|game_key_clicked, gk_group8_hear),
			(this_or_next|game_key_clicked, gk_reverse_order_group),
			(game_key_clicked, gk_everyone_around_hear),
			(call_script, "script_client_get_player_agent"),
			(assign, ":player_agent", reg0),
			(agent_is_active, ":player_agent"),
			(agent_is_alive, ":player_agent"),

			(assign, ":continue", 0),
			(try_begin),
				(neg|game_in_multiplayer_mode),
				(assign, ":continue", 1),
			(else_try),
				(agent_get_player_id, ":player_no", ":player_agent"),
				(player_is_active, ":player_no"),
				(this_or_next|neg|player_is_busy_with_menus, ":player_no"),
				(this_or_next|neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
				(neg|is_presentation_active, "prsnt_multiplayer_mod_menu"),
				(assign, ":continue", 1),
			(try_end),
			
			(eq, ":continue", 1),
			# (try_begin),#fix
				# (game_key_is_down, gk_order_1),
				# # (order_flag_is_active),#WSE
				# #REWORK
				# (assign, "$g_order_flag_was_active", 1),
			# (try_end),
			
			(try_begin),
				(assign, ":end_key", gk_order_5),
				(try_for_range, ":cur_key", gk_order_1, ":end_key"),
					# (game_key_released, ":cur_key"),#WSE
					(game_key_clicked, ":cur_key"),
					#REWORK
					(try_begin),
						(le, "$g_order_menu_open", order_menu_main),
						# (try_begin),
							# (eq, ":cur_key", gk_order_1),
							# (neg|game_key_is_down, gk_order_1),
							# (neg|is_presentation_active, "prsnt_advanced_order_menu"),
							# (start_presentation, "prsnt_advanced_order_menu"),
							# # (eq, "$g_order_flag_was_active", 1),
							# # (assign, "$g_order_flag_was_active", 0),
						# (else_try),
							(store_sub, "$g_order_menu_open", ":cur_key", gk_view_orders),
							(neg|is_presentation_active, "prsnt_advanced_order_menu"),
							(start_presentation, "prsnt_advanced_order_menu"),
						# (try_end),
					(try_end),
					(assign, ":end_key", -1),#break loop
					###
					# (assign, reg50, "$g_order_selected_group"),
					# (assign, reg51, "$g_order_menu_open"),
					# (display_message, "@trigger selected group:{reg50} order menu:{reg51}"),
					###
				(try_end),
			(try_end),
			
			(neq, ":end_key", -1),
			(assign, ":end_key", gk_mp_message_all),
			(try_for_range, ":cur_key", gk_everyone_hear, ":end_key"),
				(game_key_clicked, ":cur_key"),
				(assign, "$g_order_menu_open", order_menu_main),
				#adjust group orders
				(try_begin),
					(eq, ":cur_key", gk_everyone_hear),
					(assign, ":cur_group", grc_everyone),
				(else_try),
					(is_between, ":cur_key", gk_reverse_order_group, gk_mp_message_all),
					(store_sub, ":cur_group", ":cur_key", gk_everyone_hear),
				(else_try),
					(store_sub, ":cur_group", ":cur_key", gk_group0_hear),
				(try_end),
				(assign, "$g_order_selected_group", ":cur_group"),
				(assign, ":end_key", -1),#break loop
				###
				# (assign, reg50, "$g_order_selected_group"),
				# (display_message, "@selected group: {reg50}"),
				###
			(try_end),
		(try_end),
	]),
]


common_mod_multiplayer = common_mod + [
	(ti_before_mission_start, 0, 0, [],
  [
		(try_begin),
      (neg|multiplayer_is_dedicated_server),
			(is_presentation_active, "prsnt_game_multiplayer_admin_panel"),
			(eq, "$g_close_multiplayer_admin_panel", 0),
      (assign, "$g_close_multiplayer_admin_panel", 1),
		(try_end),

#      (server_set_anti_cheat, 0),
#      (server_set_friendly_fire, 1),
      (server_set_melee_friendly_fire, 1),
      (server_set_friendly_fire_damage_self_ratio, 0),
      (server_set_friendly_fire_damage_friend_ratio, 100),
#      (server_set_ghost_mode, ":value"),
      (server_set_control_block_dir, 1),
#      (server_set_combat_speed, ":value"),
      (server_set_add_to_game_servers_list, 1),
  ]),

  # (ti_on_multiplayer_mission_end, 0, 0,
  # [
    # (multiplayer_is_server),
  # ],
  # [
		# (call_script, "script_multiplayer_server_set_weather_time"),
    # (get_max_players, ":num_players"),
    # (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
      # (player_is_active, ":player_no"),
      # (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_return_weather_time, "$g_cloud_amount", "$g_precipitation_strength", "$g_fog_distance", "$g_day_time"),
    # (try_end),
  # ]),

  (ti_on_player_exit, 0, 0, 
	[
		(multiplayer_is_dedicated_server),
	],
	[
    (store_trigger_param_1, ":player_no"),
		
		(try_begin),
			(player_is_active, ":player_no"),
	
			(player_get_unique_id, ":unique_id", ":player_no"),
			(store_div, ":offset", ":unique_id", 1048576),
			(store_mul, ":id_sub", ":offset", 1048576),
			(val_sub, ":unique_id", ":id_sub"),
			
			(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
			(troop_set_slot, ":array", ":unique_id", 0),
			
			(neq, "$g_multiplayer_game_type", multiplayer_game_type_duel),
			(call_script, "script_multiplayer_server_init_upkeep", ":player_no"),#upkeep
		(try_end),
		
		#check if there is noone on server
		(get_max_players, ":max_players"),
		(try_for_range, ":cur_player", 1, ":max_players"),
			(player_is_active, ":cur_player"),
			(neq, ":cur_player", ":player_no"),
			(assign, ":max_players", -1),#break loop
		(try_end),
		(try_begin),
			(neq, ":max_players", -1),
			(gt, "$last_player_count", 0),
			(assign, "$last_player_count", 0),
			(display_message, "@server is empty"),

			(eq, "$g_allow_online_chars", 1),
			(str_store_string, s1, "str_server_security_token"),
			(str_store_server_name, s2),
			(str_store_string, s4, "@0"),
			(assign, reg2, "str_service_action_1"),
			(str_store_string, s10, "str_service_action_1"),
			(str_store_string, s10, "str_service_url_1"),
			# (str_encode_url, s10),
			(send_message_to_url, s10),
			
			(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
			(display_message, s0),
			(server_add_message_to_log, s0),
		(try_end),
    # (try_begin),
      # (player_is_admin, ":player_no"),
      # (str_store_player_username, s1, ":player_no"),
      # (str_store_string, s0, "@Administrator {s1} has left the game."),
      # (call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
    # (try_end),
  ]),
  
#add wage to players
(60, 0, 0,
[
		
],
[
	(try_for_players, ":cur_player", 0),
		(neg|player_slot_eq, ":cur_player", slot_player_use_troop, "trp_online_character"),

		(player_slot_ge, ":cur_player", slot_player_spawned_this_round, 1),#is spawned
		(player_get_team_no, ":team_no", ":cur_player"),
		(neq, ":team_no", multi_team_spectator),#no spectator

		#get score
		(player_get_slot, ":last_score", ":cur_player", slot_player_last_tick_score),
		(player_get_slot, ":cur_score", ":cur_player", slot_player_score),
		(player_set_slot, ":cur_player", slot_player_last_tick_score, ":cur_score"),
		(store_sub, ":tick_score", ":cur_score", ":last_score"),

		(store_mul, ":bonus_gold", ":tick_score", "$player_score_gold"),
		(store_add, ":tick_gold", "$player_tick_gold", ":bonus_gold"),
		
		(player_get_gold, ":gold", ":cur_player"),
		(val_add, ":gold", ":tick_gold"),
		(player_set_gold, ":cur_player", ":gold", 0),
	(try_end),


  		(multiplayer_is_dedicated_server),

		(assign, ":num_player_messages", 0),
		(get_max_players, ":max_players"),
		(try_for_range, ":cur_player", 1, ":max_players"),
			(player_is_active, ":cur_player"),
			(val_add, ":num_player_messages", 1),
			(player_get_unique_id, ":cur_unique_id", ":cur_player"),
			
			#get score
			(player_get_slot, ":last_score", ":cur_player", slot_player_last_tick_score),
			(player_get_slot, ":cur_score", ":cur_player", slot_player_score),
			(player_set_slot, ":cur_player", slot_player_last_tick_score, ":cur_score"),
			(store_sub, ":tick_score", ":cur_score", ":last_score"),
			#get kills
			(player_get_slot, ":last_kills", ":cur_player", slot_player_last_tick_kills),
			(player_get_slot, ":cur_kills", ":cur_player", slot_player_kills),
			(player_set_slot, ":cur_player", slot_player_last_tick_kills, ":cur_kills"),
			(store_sub, ":tick_kills", ":cur_kills", ":last_kills"),
			#get deaths
			(player_get_slot, ":last_deaths", ":cur_player", slot_player_last_tick_deaths),
			(player_get_slot, ":cur_deaths", ":cur_player", slot_player_deaths),
			(player_set_slot, ":cur_player", slot_player_last_tick_deaths, ":cur_deaths"),
			(store_sub, ":tick_deaths", ":cur_deaths", ":last_deaths"),
			
			(try_begin),
				(player_slot_eq, ":cur_player", slot_player_use_troop, "trp_online_character"),
				(this_or_next|player_slot_eq, ":cur_player", slot_player_online_status, online_status_registered),
				(this_or_next|player_slot_eq, ":cur_player", slot_player_online_status, online_status_new_player),
				(player_slot_eq, ":cur_player", slot_player_online_status, online_status_admin),
				(player_slot_ge, ":cur_player", slot_player_spawned_this_round, 1),#is spawned
				(player_get_team_no, ":team_no", ":cur_player"),
				(neq, ":team_no", multi_team_spectator),#no spectator
				(assign, ":do_tick", 1),
			(else_try),
				(assign, ":do_tick", 0),
			(try_end),
			
			(assign, reg1, ":cur_unique_id"),
			(assign, reg2, ":tick_score"),
			(assign, reg3, ":tick_kills"),
			(assign, reg4, ":tick_deaths"),
			(assign, reg5, ":do_tick"),
			(try_begin),
				(eq, ":num_player_messages", 1),
				(str_store_string, s3, "@[{reg1},{reg2},{reg3},{reg4},{reg5}]"),
			(else_try),
				(str_store_string, s3, "@{s3},[{reg1},{reg2},{reg3},{reg4},{reg5}]"),
			(try_end),
			
			(try_begin),
				(gt, ":do_tick", 0),
				
				(player_get_slot, ":ticks_since_upkeep", ":cur_player", slot_player_ticks_since_upkeep),
				(val_add, ":ticks_since_upkeep", 1),
				(player_set_slot, ":cur_player", slot_player_ticks_since_upkeep, ":ticks_since_upkeep"),
				
				(store_div, ":offset", ":cur_unique_id", 1048576),
				(store_mul, ":id_sub", ":offset", 1048576),
				(val_sub, ":cur_unique_id", ":id_sub"),
				
				(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
				
				(troop_get_slot, ":round_bonus", ":array", ":cur_unique_id"),
				(val_clamp, ":round_bonus", 0, 5),
				
				(store_mul, ":round_xp_bonus", ":round_bonus", online_round_xp_bonus),
				(store_mul, ":round_gold_bonus", ":round_bonus", online_round_gold_bonus),
				
				(assign, ":bonus_xp", ":tick_score"),
				(store_div, ":bonus_gold", ":tick_score", online_score_gold_divider),
				
				(store_add, ":base_xp", online_xp_tick, ":round_xp_bonus"),
				(player_get_slot, ":cur_gen", ":cur_player", slot_player_online_generation),
				(try_begin),
					(gt, ":cur_gen", 0),
					(assign, ":xp_adder", online_gen_xp_bonus),
					(store_add, ":end", ":cur_gen", 1),
					(try_for_range, ":unused", 1, ":end"),
						(try_begin),
							(lt, ":xp_adder", online_gen_xp_decrease),
							(assign, ":xp_adder", 1),#ensure bonus
						(try_end),
						(val_add, ":base_xp", ":xp_adder"),
						(val_sub, ":xp_adder", online_gen_xp_decrease),
					(try_end),
				(try_end),
				
				(assign, reg86, ":base_xp"),
				(str_store_string, s16, "@{reg86}"),
				(try_begin),
					(neq, ":bonus_xp", 0),
					(assign, reg86, ":bonus_xp"),
					(try_begin),
						(gt, ":bonus_xp", 0),
						(str_store_string, s16, "@{s16} +{reg86}"),
					(else_try),
						(str_store_string, s16, "@{s16} {reg86}"),
					(try_end),
				(try_end),
				
				(store_add, reg87, online_gold_tick, ":round_gold_bonus"),
				(str_store_string, s17, "@{reg87}"),
				(try_begin),
					(neq, ":bonus_gold", 0),
					(assign, reg87, ":bonus_gold"),
					(try_begin),
						(gt, ":bonus_gold", 0),
						(str_store_string, s17, "@{s17} +{reg87}"),
					(else_try),
						(str_store_string, s17, "@{s17} {reg87}"),
					(try_end),
				(try_end),
					
				(player_get_slot, ":cur_xp", ":cur_player", slot_player_online_xp),
				(val_add, ":cur_xp", ":base_xp"),
				(val_add, ":cur_xp", ":bonus_xp"),
				(player_set_slot, ":cur_player", slot_player_online_xp, ":cur_xp"),
					
				(player_get_slot, ":cur_lvl", ":cur_player", slot_player_online_lvl),
				(store_add, ":next_lvl", ":cur_lvl", 1),
				(troop_get_slot, ":next_lvl_xp", "trp_online_level_xp", ":next_lvl"),
				(try_begin),
					(ge, ":cur_xp", ":next_lvl_xp"),
					(player_set_slot, ":cur_player", slot_player_online_lvl, ":next_lvl"),
					(assign, reg88, ":next_lvl"),
					(str_store_string, s0, "@Gained {s16} xp and {s17} gold. ^You have reached level {reg88}."),
					(try_begin),
						(eq, ":next_lvl", online_retire_lvl),
						(str_store_string, s0, "@{s0} ^You are now able to retire. ^this will add a xp bonus and one heirloom point."),
					(try_end),
				(else_try),
					(str_store_string, s0, "@Gained {s16} xp and {s17} gold."),
				(try_end),
				(call_script, "script_multiplayer_send_message_to_player", ":cur_player", color_gained_wage),	
				(multiplayer_send_int_to_player, ":cur_player", multiplayer_event_client_set_experience, ":cur_xp"),
				
				(store_add, ":tick_gold", online_gold_tick, ":bonus_gold"),
				(val_add, ":tick_gold", ":round_gold_bonus"),
				
				(player_get_gold, ":gold", ":cur_player"),
				(val_add, ":gold", ":tick_gold"),
				(player_set_gold, ":cur_player", ":gold", 0),
				
				(player_get_slot, ":gold_since_upkeep", ":cur_player", slot_player_gold_earned_since_upkeep),
				(val_add, ":gold_since_upkeep", ":tick_gold"),
				(player_set_slot, ":cur_player", slot_player_gold_earned_since_upkeep, ":gold_since_upkeep"),
			(try_end),
		(try_end),

		(try_begin),
			(eq, "$g_allow_online_chars", 1),
			(gt, ":num_player_messages", 0),
			(str_store_string, s1, "str_server_security_token"),
			(str_store_server_name, s2),
			(assign, reg2, "str_service_action_3"),
			(str_store_string, s10, "str_service_action_3"),
			(str_store_string, s10, "str_service_url_1"),
			# (str_encode_url, s10),
			(send_message_to_url, s10),
			(str_store_string, s0, "@[DEBUG]: Send http request: {s10}"),
			(server_add_message_to_log, s0),
			(display_message, s0),
			#(call_script, "script_multiplayer_broadcast_message", 1, color_server_message),
		(try_end),
		
		(assign, reg11, ":num_player_messages"),
		(assign, reg12, "$last_player_count"),
		(display_message, "@cur players {reg11} last players {reg12}"),
		(assign, "$last_player_count", ":num_player_messages"),
  ]),
 
  (0, 0.01, 0,
  [
		(neg|multiplayer_is_dedicated_server),
    (call_script, "script_cf_no_action_menus_active"),
		
		(try_begin),#clan
			(key_clicked, key_o),
			(assign, "$g_cur_custom_chat", 0),
		(else_try),#admins
			(key_clicked, key_i),
			(assign, "$g_cur_custom_chat", 1),
		(else_try),#admin message
			(key_clicked, key_u),
			(multiplayer_get_my_player, ":my_player"),
			(player_is_admin, ":my_player"),
			(assign, "$g_cur_custom_chat", 2),
		(try_end),
		(gt, "$g_cur_custom_chat", -1),
  ],
  [
    (start_presentation, "prsnt_custom_chat"),
  ]),

 #  (0, 0, 0, [],
 #  [
 #  	(key_clicked, key_q),
  	
 #  	(call_script, "script_client_get_player_agent"),
	# (assign, ":player_agent", reg0),
 #    (agent_is_active, ":player_agent"),
 #    (agent_is_alive, ":player_agent"),

 #    (agent_get_position, pos0, ":player_agent"),
 #  	(try_for_agents, ":agent"),
 #  		(neq, ":agent", ":player_agent"),
 #  		(agent_get_position, pos1, ":agent"),
 #  	(try_end),

 #  	(position_is_behind_position, pos0, pos1),
 #  	(display_message,"@player behind agent"),
 #  ]),
]


# multiplayer_server_check_belfry_movement = (
  # 0, 0, 0, [],
  # [
    # (multiplayer_is_server),
    # (set_fixed_point_multiplier, 100),

    # (try_for_range, ":belfry_kind", 0, 2),
      # (try_begin),
        # (eq, ":belfry_kind", 0),
        # (assign, ":belfry_body_scene_prop", "spr_belfry_a"),
      # (else_try),
        # (assign, ":belfry_body_scene_prop", "spr_belfry_b"),
      # (try_end),
    
      # (scene_prop_get_num_instances, ":num_belfries", ":belfry_body_scene_prop"),
      # (try_for_range, ":belfry_no", 0, ":num_belfries"),
        # (scene_prop_get_instance, ":belfry_scene_prop_id", ":belfry_body_scene_prop", ":belfry_no"),
        # (prop_instance_get_position, pos1, ":belfry_scene_prop_id"), #pos1 holds position of current belfry 
        # (prop_instance_get_starting_position, pos11, ":belfry_scene_prop_id"),

        # (store_add, ":belfry_first_entry_point_id", 11, ":belfry_no"), #belfry entry points are 110..119 and 120..129 and 130..139
        # (try_begin),
          # (eq, ":belfry_kind", 1),
          # (scene_prop_get_num_instances, ":number_of_belfry_a", "spr_belfry_a"),
          # (val_add, ":belfry_first_entry_point_id", ":number_of_belfry_a"),
        # (try_end),        
                
        # (val_mul, ":belfry_first_entry_point_id", 10),
        # (store_add, ":belfry_last_entry_point_id", ":belfry_first_entry_point_id", 10),
    
        # (try_for_range, ":entry_point_id", ":belfry_first_entry_point_id", ":belfry_last_entry_point_id"),
          # (entry_point_is_auto_generated, ":entry_point_id"),
          # (assign, ":belfry_last_entry_point_id", ":entry_point_id"),
        # (try_end),
        
        # (assign, ":belfry_last_entry_point_id_plus_one", ":belfry_last_entry_point_id"),
        # (val_sub, ":belfry_last_entry_point_id", 1),
        # (assign, reg0, ":belfry_last_entry_point_id"),
        # (neg|entry_point_is_auto_generated, ":belfry_last_entry_point_id"),

        # (try_begin),
          # (get_sq_distance_between_positions, ":dist_between_belfry_and_its_destination", pos1, pos11),
          # (ge, ":dist_between_belfry_and_its_destination", 4), #0.2 * 0.2 * 100 = 4 (if distance between belfry and its destination already less than 20cm no need to move it anymore)

          # (assign, ":max_dist_between_entry_point_and_belfry_destination", -1), #should be lower than 0 to allow belfry to go last entry point
          # (assign, ":belfry_next_entry_point_id", -1),
          # (try_for_range, ":entry_point_id", ":belfry_first_entry_point_id", ":belfry_last_entry_point_id_plus_one"),
            # (entry_point_get_position, pos4, ":entry_point_id"),
            # (get_sq_distance_between_positions, ":dist_between_entry_point_and_belfry_destination", pos11, pos4),
            # (lt, ":dist_between_entry_point_and_belfry_destination", ":dist_between_belfry_and_its_destination"),
            # (gt, ":dist_between_entry_point_and_belfry_destination", ":max_dist_between_entry_point_and_belfry_destination"),
            # (assign, ":max_dist_between_entry_point_and_belfry_destination", ":dist_between_entry_point_and_belfry_destination"),
            # (assign, ":belfry_next_entry_point_id", ":entry_point_id"),
          # (try_end),

          # (try_begin),
            # (ge, ":belfry_next_entry_point_id", 0),
            # (entry_point_get_position, pos5, ":belfry_next_entry_point_id"), #pos5 holds belfry next entry point target during its path
          # (else_try),
            # (copy_position, pos5, pos11),    
          # (try_end),
        
          # (get_distance_between_positions, ":belfry_next_entry_point_distance", pos1, pos5),
        
          # #collecting scene prop ids of belfry parts
          # (try_begin),
            # (eq, ":belfry_kind", 0),
            # #belfry platform_a
            # (scene_prop_get_instance, ":belfry_platform_a_scene_prop_id", "spr_belfry_platform_a", ":belfry_no"),
            # #belfry platform_b
            # (scene_prop_get_instance, ":belfry_platform_b_scene_prop_id", "spr_belfry_platform_b", ":belfry_no"),
          # (else_try),
            # #belfry platform_a
            # (scene_prop_get_instance, ":belfry_platform_a_scene_prop_id", "spr_belfry_b_platform_a", ":belfry_no"),
          # (try_end),
    
          # #belfry wheel_1
          # (store_mul, ":wheel_no", ":belfry_no", 3),
          # (try_begin),
            # (eq, ":belfry_body_scene_prop", "spr_belfry_b"),
            # (scene_prop_get_num_instances, ":number_of_belfry_a", "spr_belfry_a"),    
            # (store_mul, ":number_of_belfry_a_wheels", ":number_of_belfry_a", 3),
            # (val_add, ":wheel_no", ":number_of_belfry_a_wheels"),
          # (try_end),
          # (scene_prop_get_instance, ":belfry_wheel_1_scene_prop_id", "spr_belfry_wheel", ":wheel_no"),
          # #belfry wheel_2
          # (val_add, ":wheel_no", 1),
          # (scene_prop_get_instance, ":belfry_wheel_2_scene_prop_id", "spr_belfry_wheel", ":wheel_no"),
          # #belfry wheel_3
          # (val_add, ":wheel_no", 1),
          # (scene_prop_get_instance, ":belfry_wheel_3_scene_prop_id", "spr_belfry_wheel", ":wheel_no"),

          # (init_position, pos17),
          # (position_move_y, pos17, -225),
          # (position_transform_position_to_parent, pos18, pos1, pos17),
          # (position_move_y, pos17, -225),
          # (position_transform_position_to_parent, pos19, pos1, pos17),

          # (assign, ":number_of_agents_around_belfry", 0),
          # (get_max_players, ":num_players"),
          # (try_for_range, ":player_no", 0, ":num_players"),
            # (player_is_active, ":player_no"),
            # (player_get_agent_id, ":agent_id", ":player_no"),
            # (ge, ":agent_id", 0),
            # (agent_get_team, ":agent_team", ":agent_id"),
            # (eq, ":agent_team", 1), #only team2 players allowed to move belfry (team which spawns outside the castle (team1 = 0, team2 = 1))
            # (agent_get_horse, ":agent_horse_id", ":agent_id"),
            # (eq, ":agent_horse_id", -1),
            # (agent_get_position, pos2, ":agent_id"),
            # (get_sq_distance_between_positions_in_meters, ":dist_between_agent_and_belfry", pos18, pos2),

            # (lt, ":dist_between_agent_and_belfry", multi_distance_sq_to_use_belfry), #must be at most 10m * 10m = 100m away from the player
            # (neg|scene_prop_has_agent_on_it, ":belfry_scene_prop_id", ":agent_id"),
            # (neg|scene_prop_has_agent_on_it, ":belfry_platform_a_scene_prop_id", ":agent_id"),

            # (this_or_next|eq, ":belfry_kind", 1), #there is this_or_next here because belfry_b has no platform_b
            # (neg|scene_prop_has_agent_on_it, ":belfry_platform_b_scene_prop_id", ":agent_id"),
    
            # (neg|scene_prop_has_agent_on_it, ":belfry_wheel_1_scene_prop_id", ":agent_id"),#can be removed to make faster
            # (neg|scene_prop_has_agent_on_it, ":belfry_wheel_2_scene_prop_id", ":agent_id"),#can be removed to make faster
            # (neg|scene_prop_has_agent_on_it, ":belfry_wheel_3_scene_prop_id", ":agent_id"),#can be removed to make faster
            # (neg|position_is_behind_position, pos2, pos19),
            # (position_is_behind_position, pos2, pos1),
            # (val_add, ":number_of_agents_around_belfry", 1),        
          # (try_end),

          # (val_min, ":number_of_agents_around_belfry", 16),

          # (try_begin),
            # (scene_prop_get_slot, ":pre_number_of_agents_around_belfry", ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing),
            # (scene_prop_get_slot, ":next_entry_point_id", ":belfry_scene_prop_id", scene_prop_next_entry_point_id),
            # (this_or_next|neq, ":pre_number_of_agents_around_belfry", ":number_of_agents_around_belfry"),
            # (neq, ":next_entry_point_id", ":belfry_next_entry_point_id"),

            # (try_begin),
              # (eq, ":next_entry_point_id", ":belfry_next_entry_point_id"), #if we are still targetting same entry point subtract 
              # (prop_instance_is_animating, ":is_animating", ":belfry_scene_prop_id"),
              # (eq, ":is_animating", 1),

              # (store_mul, ":sqrt_number_of_agents_around_belfry", "$g_last_number_of_agents_around_belfry", 100),
              # (store_sqrt, ":sqrt_number_of_agents_around_belfry", ":sqrt_number_of_agents_around_belfry"),
              # (val_min, ":sqrt_number_of_agents_around_belfry", 300),
              # (assign, ":distance", ":belfry_next_entry_point_distance"),
              # (val_mul, ":distance", ":sqrt_number_of_agents_around_belfry"),
              # (val_div, ":distance", 100), #100 is because of fixed_point_multiplier
              # (val_mul, ":distance", 4), #multiplying with 4 to make belfry pushing process slower, 
                                                                 # #with 16 agents belfry will go with 4 / 4 = 1 speed (max), with 1 agent belfry will go with 1 / 4 = 0.25 speed (min)    
            # (try_end),

            # (try_begin),
              # (ge, ":belfry_next_entry_point_id", 0),

              # #up down rotation of belfry's next entry point
              # (init_position, pos9),
              # (position_set_y, pos9, -500), #go 5.0 meters back
              # (position_set_x, pos9, -300), #go 3.0 meters left
              # (position_transform_position_to_parent, pos10, pos5, pos9), 
              # (position_get_distance_to_terrain, ":height_to_terrain_1", pos10), #learn distance between 5 meters back of entry point(pos10) and ground level at left part of belfry
      
              # (init_position, pos9),
              # (position_set_y, pos9, -500), #go 5.0 meters back
              # (position_set_x, pos9, 300), #go 3.0 meters right
              # (position_transform_position_to_parent, pos10, pos5, pos9), 
              # (position_get_distance_to_terrain, ":height_to_terrain_2", pos10), #learn distance between 5 meters back of entry point(pos10) and ground level at right part of belfry

              # (store_add, ":height_to_terrain", ":height_to_terrain_1", ":height_to_terrain_2"),
              # (val_mul, ":height_to_terrain", 100), #because of fixed point multiplier

              # (store_div, ":rotate_angle_of_next_entry_point", ":height_to_terrain", 24), #if there is 1 meters of distance (100cm) then next target position will rotate by 2 degrees. #ac sonra
              # (init_position, pos20),    
              # (position_rotate_x_floating, pos20, ":rotate_angle_of_next_entry_point"),
              # (position_transform_position_to_parent, pos23, pos5, pos20),

              # #right left rotation of belfry's next entry point
              # (init_position, pos9),
              # (position_set_x, pos9, -300), #go 3.0 meters left
              # (position_transform_position_to_parent, pos10, pos5, pos9), #applying 3.0 meters in -x to position of next entry point target, final result is in pos10
              # (position_get_distance_to_terrain, ":height_to_terrain_at_left", pos10), #learn distance between 3.0 meters left of entry point(pos10) and ground level
              # (init_position, pos9),
              # (position_set_x, pos9, 300), #go 3.0 meters left
              # (position_transform_position_to_parent, pos10, pos5, pos9), #applying 3.0 meters in x to position of next entry point target, final result is in pos10
              # (position_get_distance_to_terrain, ":height_to_terrain_at_right", pos10), #learn distance between 3.0 meters right of entry point(pos10) and ground level
              # (store_sub, ":height_to_terrain_1", ":height_to_terrain_at_left", ":height_to_terrain_at_right"),

              # (init_position, pos9),
              # (position_set_x, pos9, -300), #go 3.0 meters left
              # (position_set_y, pos9, -500), #go 5.0 meters forward
              # (position_transform_position_to_parent, pos10, pos5, pos9), #applying 3.0 meters in -x to position of next entry point target, final result is in pos10
              # (position_get_distance_to_terrain, ":height_to_terrain_at_left", pos10), #learn distance between 3.0 meters left of entry point(pos10) and ground level
              # (init_position, pos9),
              # (position_set_x, pos9, 300), #go 3.0 meters left
              # (position_set_y, pos9, -500), #go 5.0 meters forward
              # (position_transform_position_to_parent, pos10, pos5, pos9), #applying 3.0 meters in x to position of next entry point target, final result is in pos10
              # (position_get_distance_to_terrain, ":height_to_terrain_at_right", pos10), #learn distance between 3.0 meters right of entry point(pos10) and ground level
              # (store_sub, ":height_to_terrain_2", ":height_to_terrain_at_left", ":height_to_terrain_at_right"),

              # (store_add, ":height_to_terrain", ":height_to_terrain_1", ":height_to_terrain_2"),    
              # (val_mul, ":height_to_terrain", 100), #100 is because of fixed_point_multiplier
              # (store_div, ":rotate_angle_of_next_entry_point", ":height_to_terrain", 24), #if there is 1 meters of distance (100cm) then next target position will rotate by 25 degrees. 
              # (val_mul, ":rotate_angle_of_next_entry_point", -1),

              # (init_position, pos20),
              # (position_rotate_y_floating, pos20, ":rotate_angle_of_next_entry_point"),
              # (position_transform_position_to_parent, pos22, pos23, pos20),
            # (else_try),
              # (copy_position, pos22, pos5),      
            # (try_end),
              
            # (try_begin),
              # (ge, ":number_of_agents_around_belfry", 1), #if there is any agents pushing belfry

              # (store_mul, ":sqrt_number_of_agents_around_belfry", ":number_of_agents_around_belfry", 100),
              # (store_sqrt, ":sqrt_number_of_agents_around_belfry", ":sqrt_number_of_agents_around_belfry"),
              # (val_min, ":sqrt_number_of_agents_around_belfry", 300),
              # (val_mul, ":belfry_next_entry_point_distance", 100), #100 is because of fixed_point_multiplier
              # (val_mul, ":belfry_next_entry_point_distance", 3), #multiplying with 3 to make belfry pushing process slower, 
                                                                 # #with 9 agents belfry will go with 3 / 3 = 1 speed (max), with 1 agent belfry will go with 1 / 3 = 0.33 speed (min)    
              # (val_div, ":belfry_next_entry_point_distance", ":sqrt_number_of_agents_around_belfry"),
              # #calculating destination coordinates of belfry parts
              # #belfry platform_a
              # (prop_instance_get_position, pos6, ":belfry_platform_a_scene_prop_id"),
              # (position_transform_position_to_local, pos7, pos1, pos6),
              # (position_transform_position_to_parent, pos8, pos22, pos7),
              # (prop_instance_animate_to_position, ":belfry_platform_a_scene_prop_id", pos8, ":belfry_next_entry_point_distance"),    
              # #belfry platform_b
              # (try_begin),
                # (eq, ":belfry_kind", 0),
                # (prop_instance_get_position, pos6, ":belfry_platform_b_scene_prop_id"),
                # (position_transform_position_to_local, pos7, pos1, pos6),
                # (position_transform_position_to_parent, pos8, pos22, pos7),
                # (prop_instance_animate_to_position, ":belfry_platform_b_scene_prop_id", pos8, ":belfry_next_entry_point_distance"),
              # (try_end),
              # #wheel rotation
              # (store_mul, ":belfry_wheel_rotation", ":belfry_next_entry_point_distance", -25),
              # #(val_add, "$g_belfry_wheel_rotation", ":belfry_wheel_rotation"),
              # (assign, "$g_last_number_of_agents_around_belfry", ":number_of_agents_around_belfry"),

              # #belfry wheel_1
              # #(prop_instance_get_starting_position, pos13, ":belfry_wheel_1_scene_prop_id"),
              # (prop_instance_get_position, pos13, ":belfry_wheel_1_scene_prop_id"),
              # (prop_instance_get_position, pos20, ":belfry_scene_prop_id"),
              # (position_transform_position_to_local, pos7, pos20, pos13),
              # (position_transform_position_to_parent, pos21, pos22, pos7),
              # (prop_instance_rotate_to_position, ":belfry_wheel_1_scene_prop_id", pos21, ":belfry_next_entry_point_distance", ":belfry_wheel_rotation"),
      
              # #belfry wheel_2
              # #(prop_instance_get_starting_position, pos13, ":belfry_wheel_2_scene_prop_id"),
              # (prop_instance_get_position, pos13, ":belfry_wheel_2_scene_prop_id"),
              # (prop_instance_get_position, pos20, ":belfry_scene_prop_id"),
              # (position_transform_position_to_local, pos7, pos20, pos13),
              # (position_transform_position_to_parent, pos21, pos22, pos7),
              # (prop_instance_rotate_to_position, ":belfry_wheel_2_scene_prop_id", pos21, ":belfry_next_entry_point_distance", ":belfry_wheel_rotation"),
      
              # #belfry wheel_3
              # (prop_instance_get_position, pos13, ":belfry_wheel_3_scene_prop_id"),
              # (prop_instance_get_position, pos20, ":belfry_scene_prop_id"),
              # (position_transform_position_to_local, pos7, pos20, pos13),
              # (position_transform_position_to_parent, pos21, pos22, pos7),
              # (prop_instance_rotate_to_position, ":belfry_wheel_3_scene_prop_id", pos21, ":belfry_next_entry_point_distance", ":belfry_wheel_rotation"),

              # #belfry main body
              # (prop_instance_animate_to_position, ":belfry_scene_prop_id", pos22, ":belfry_next_entry_point_distance"),    
            # (else_try),
              # (prop_instance_is_animating, ":is_animating", ":belfry_scene_prop_id"),
              # (eq, ":is_animating", 1),

              # #belfry platform_a
              # (prop_instance_stop_animating, ":belfry_platform_a_scene_prop_id"),
              # #belfry platform_b
              # (try_begin),
                # (eq, ":belfry_kind", 0),
                # (prop_instance_stop_animating, ":belfry_platform_b_scene_prop_id"),
              # (try_end),
              # #belfry wheel_1
              # (prop_instance_stop_animating, ":belfry_wheel_1_scene_prop_id"),
              # #belfry wheel_2
              # (prop_instance_stop_animating, ":belfry_wheel_2_scene_prop_id"),
              # #belfry wheel_3
              # (prop_instance_stop_animating, ":belfry_wheel_3_scene_prop_id"),
              # #belfry main body
              # (prop_instance_stop_animating, ":belfry_scene_prop_id"),
            # (try_end),
        
            # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing, ":number_of_agents_around_belfry"),    
            # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_next_entry_point_id, ":belfry_next_entry_point_id"),
          # (try_end),
        # (else_try),
          # (le, ":dist_between_belfry_and_its_destination", 4),
          # (scene_prop_slot_eq, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 0),
      
          # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 1),    

          # (try_begin),
            # (eq, ":belfry_kind", 0),
            # (scene_prop_get_instance, ":belfry_platform_a_scene_prop_id", "spr_belfry_platform_a", ":belfry_no"),
          # (else_try),
            # (scene_prop_get_instance, ":belfry_platform_a_scene_prop_id", "spr_belfry_b_platform_a", ":belfry_no"),
          # (try_end),
    
          # (prop_instance_get_starting_position, pos0, ":belfry_platform_a_scene_prop_id"),
          # (prop_instance_animate_to_position, ":belfry_platform_a_scene_prop_id", pos0, 400),    
        # (try_end),
      # (try_end),
    # (try_end),
    # ])

multiplayer_server_spawn_bots = (
  0, 0, 0, [],
  [
    (multiplayer_is_server),
    (eq, "$g_multiplayer_ready_for_spawning_agent", 1),
    (store_add, ":total_req", "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_required_team_2"),
    (try_begin),
      (gt, ":total_req", 0),

      (try_begin),
        (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
        (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

        (team_get_score, ":team_1_score", 0),
        (team_get_score, ":team_2_score", 1),

        (store_add, ":current_round", ":team_1_score", ":team_2_score"),
        (eq, ":current_round", 0),

        (store_mission_timer_a, ":round_time"),
        (val_sub, ":round_time", "$g_round_start_time"),
        (lt, ":round_time", 20),

        (assign, ":rounded_game_first_round_time_limit_past", 0),
      (else_try),
        (assign, ":rounded_game_first_round_time_limit_past", 1),
      (try_end),
    
      (eq, ":rounded_game_first_round_time_limit_past", 1),
    
      (store_random_in_range, ":random_req", 0, ":total_req"),
      (val_sub, ":random_req", "$g_multiplayer_num_bots_required_team_1"),
      (try_begin),
        (lt, ":random_req", 0),
        #add to team 1
        (assign, ":selected_team", 0),
      (else_try),
        #add to team 2
        (assign, ":selected_team", 1),
      (try_end),

      (try_begin),
        (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
        (store_mission_timer_a, ":round_time"),
        (val_sub, ":round_time", "$g_round_start_time"),

        (try_begin),
          (le, ":round_time", 20),
          (assign, ":look_only_actives", 0),
        (else_try),
          (assign, ":look_only_actives", 1),
        (try_end),
      (else_try),
        (assign, ":look_only_actives", 1),
      (try_end),
    
      (call_script, "script_multiplayer_find_bot_troop_and_group_for_spawn", ":selected_team", ":look_only_actives"),
      (assign, ":selected_troop", reg0),
      (assign, ":selected_group", reg1),

      (team_get_faction, ":team_faction", ":selected_team"),
      (assign, ":num_ai_troops", 0),
      (try_for_range, ":cur_ai_troop", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
        (store_troop_faction, ":ai_troop_faction", ":cur_ai_troop"),
        (eq, ":ai_troop_faction", ":team_faction"),
        (val_add, ":num_ai_troops", 1),
      (try_end),

      (assign, ":number_of_active_players_wanted_bot", 0),

      (get_max_players, ":num_players"),
      (try_for_range, ":player_no", 0, ":num_players"),
        (player_is_active, ":player_no"),
        (player_get_team_no, ":player_team_no", ":player_no"),
        (eq, ":selected_team", ":player_team_no"),

        (assign, ":ai_wanted", 0),
        (store_add, ":end_cond", slot_player_bot_type_1_wanted, ":num_ai_troops"),
        (try_for_range, ":bot_type_wanted_slot", slot_player_bot_type_1_wanted, ":end_cond"),
          (player_slot_ge, ":player_no", ":bot_type_wanted_slot", 1),
          (assign, ":ai_wanted", 1),
          (assign, ":end_cond", 0), 
        (try_end),

        (ge, ":ai_wanted", 1),

        (val_add, ":number_of_active_players_wanted_bot", 1),
      (try_end),

      (try_begin),
        (this_or_next|ge, ":selected_group", 0),
        (eq, ":number_of_active_players_wanted_bot", 0),

        (troop_get_inventory_slot, ":has_item", ":selected_troop", ek_horse),
        (try_begin),
          (ge, ":has_item", 0),
          (assign, ":is_horseman", 1),
        (else_try),
          (assign, ":is_horseman", 0),
        (try_end),

        (try_begin),
          (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),

          (store_mission_timer_a, ":round_time"),
          (val_sub, ":round_time", "$g_round_start_time"),

          (try_begin),
            (lt, ":round_time", 20), #at start of game spawn at base entry point
            (try_begin),
              (eq, ":selected_team", 0),
              (call_script, "script_multiplayer_find_spawn_point", ":selected_team", 1, ":is_horseman"), 
            (else_try),
              (assign, reg0, multi_initial_spawn_point_team_2),
            (try_end),
          (else_try),
            (call_script, "script_multiplayer_find_spawn_point", ":selected_team", 0, ":is_horseman"), 
          (try_end),
        (else_try),
					(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
          (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
      
          (try_begin),
            (eq, ":selected_team", 0),
            (assign, reg0, 0),
          (else_try),
            (assign, reg0, 32),
          (try_end),
        (else_try),
          (call_script, "script_multiplayer_find_spawn_point", ":selected_team", 0, ":is_horseman"), 
        (try_end),
      
        (store_current_scene, ":cur_scene"),
        (modify_visitors_at_site, ":cur_scene"),
        (add_visitors_to_current_scene, reg0, ":selected_troop", 1, ":selected_team", ":selected_group"),
        (assign, "$g_multiplayer_ready_for_spawning_agent", 0),

        (try_begin),
          (eq, ":selected_team", 0),
          (val_sub, "$g_multiplayer_num_bots_required_team_1", 1),
        (else_try),
          (eq, ":selected_team", 1),
          (val_sub, "$g_multiplayer_num_bots_required_team_2", 1),
        (try_end),
      (try_end),
    (try_end),    
    ])

multiplayer_server_manage_bots = (
  3, 0, 0, [],
  [
    (multiplayer_is_server),
    (try_for_agents, ":cur_agent"),
      (agent_is_non_player, ":cur_agent"),
      (agent_is_human, ":cur_agent"),
      (agent_is_alive, ":cur_agent"),
      (agent_get_group, ":agent_group", ":cur_agent"),
      (try_begin),
        (neg|player_is_active, ":agent_group"),
        (call_script, "script_multiplayer_change_leader_of_bot", ":cur_agent"),
      (else_try),
        (player_get_team_no, ":leader_team_no", ":agent_group"),
        (agent_get_team, ":agent_team", ":cur_agent"),
        (neq, ":leader_team_no", ":agent_team"),
        (call_script, "script_multiplayer_change_leader_of_bot", ":cur_agent"),
      (try_end),
    (try_end),
    ])

multiplayer_server_check_polls = (
  1, 5, 0,
  [
    (multiplayer_is_server),
    (eq, "$g_multiplayer_poll_running", 1),
    (eq, "$g_multiplayer_poll_ended", 0),
    (store_mission_timer_a, ":mission_timer"),
    (store_add, ":total_votes", "$g_multiplayer_poll_no_count", "$g_multiplayer_poll_yes_count"),
    (this_or_next|eq, ":total_votes", "$g_multiplayer_poll_num_sent"),
    (gt, ":mission_timer", "$g_multiplayer_poll_end_time"),
    (call_script, "script_cf_multiplayer_evaluate_poll"),
    ],
  [
    (assign, "$g_multiplayer_poll_running", 0),
    (try_begin),
      (this_or_next|eq, "$g_multiplayer_poll_to_show", 0), #change map
      (eq, "$g_multiplayer_poll_to_show", 3), #change map with factions
      (call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
      (start_multiplayer_mission, reg0, "$g_multiplayer_poll_value_to_show", 1),
      (call_script, "script_game_set_multiplayer_mission_end"),
    (try_end),
    ])
    
multiplayer_server_check_end_map = ( 
  1, 1, 0, #MOD edit
	[
#MOD begin
    (multiplayer_is_server),
    #checking for restarting the map
    (assign, ":end_map", 0),
    (try_begin),
      # (this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
      # (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
    
      # (try_begin),
        # (eq, "$g_round_ended", 1),

        # (store_mission_timer_a, ":seconds_past_till_round_ended"),
        # (val_sub, ":seconds_past_till_round_ended", "$g_round_finish_time"),
        # (store_sub, ":multiplayer_respawn_period_minus_one", "$g_multiplayer_respawn_period", 1),
        # (ge, ":seconds_past_till_round_ended", ":multiplayer_respawn_period_minus_one"),

        # (store_mission_timer_a, ":mission_timer"),    
        # (try_begin),
          # (eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
          # (assign, ":reduce_amount", 90),
        # (else_try),
          # (assign, ":reduce_amount", 120),
        # (try_end),
    
        # (store_mul, ":game_max_seconds", "$g_multiplayer_game_max_minutes", 60),
        # (store_sub, ":game_max_seconds_min_n_seconds", ":game_max_seconds", ":reduce_amount"), #when round ends if there are 60 seconds to map change time then change map without completing exact map time.
        # (gt, ":mission_timer", ":game_max_seconds_min_n_seconds"),
        # (assign, ":end_map", 1),
      # (try_end),
      
      # (eq, ":end_map", 1),
#MOD begin
    # (else_try),
			(eq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
			(store_mission_timer_a, ":mission_timer"),
      (gt, ":mission_timer", "$g_multiplayer_round_max_seconds"),
			
			(val_add, "$g_round_ended", 1),
			(try_begin),
				(eq, "$g_round_ended", 1),
				
				(team_get_score, ":team_1_score", 0),
				(team_get_score, ":team_2_score", 1),
				
				(try_begin),
					(gt, ":team_1_score", ":team_2_score"),
					(assign, "$g_winner_team", 0),
				(else_try),
					(gt, ":team_2_score", ":team_1_score"),
					(assign, "$g_winner_team", 1),
				(else_try),
					(assign, "$g_winner_team", -1),
				(try_end),
						
				(call_script, "script_draw_this_round", "$g_winner_team"),
				(get_max_players, ":num_players"),
				(try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
					(player_is_active, ":player_no"),
					(multiplayer_send_int_to_player, ":player_no", multiplayer_event_draw_this_round, "$g_winner_team"),
				(try_end),
				
				(try_begin),#upkeep
					(multiplayer_is_dedicated_server),
					(call_script, "script_multiplayer_server_init_upkeep", -1),
				(try_end),
			(else_try),
				(eq, "$g_round_ended", 3),
				(assign, "$g_round_ended", 0),
				(assign, ":end_map", 1),
			(try_end),
#MOD end
		# (else_try),
      # (neq, "$g_multiplayer_game_type", multiplayer_game_type_battle), #battle mod has different end map condition by time
      # (neq, "$g_multiplayer_game_type", multiplayer_game_type_siege), #siege mod has different end map condition by time
      # (neq, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),#MOD
			# (store_mission_timer_a, ":mission_timer"),
      # (store_mul, ":game_max_seconds", "$g_multiplayer_game_max_minutes", 60),
      # (gt, ":mission_timer", ":game_max_seconds"),
      # (assign, ":end_map", 1),
    (else_try),
			(this_or_next|eq, "$g_multiplayer_game_type", multiplayer_game_type_battle),
      (eq, "$g_multiplayer_game_type", multiplayer_game_type_siege),
      #assuming only 2 teams in scene
      (team_get_score, ":team_1_score", 0),
      (team_get_score, ":team_2_score", 1),
			(this_or_next|ge, ":team_1_score", "$g_multiplayer_game_max_points"),
			(ge, ":team_2_score", "$g_multiplayer_game_max_points"),
			(assign, ":end_map", 1),
#MOD disable
      # (else_try),
        # (assign, ":at_least_one_player_is_at_game", 0),
        # (get_max_players, ":num_players"),
        # (try_for_range, ":player_no", 0, ":num_players"),
          # (player_is_active, ":player_no"),
          # (player_get_agent_id, ":agent_id", ":player_no"),
          # (ge, ":agent_id", 0),
          # (neg|agent_is_non_player, ":agent_id"),
          # (assign, ":at_least_one_player_is_at_game", 1),
          # (assign, ":num_players", 0),
        # (try_end),
    
        # (eq, ":at_least_one_player_is_at_game", 1),
        # (this_or_next|le, ":team_1_score", 0), #in headquarters game ends only if one team has 0 score.
        # (le, ":team_2_score", 0),
        # (assign, ":end_map", 1),
			# (try_end),
    (try_end),
		(eq, ":end_map", 1),
#MOD end
	],
  [
		(call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
		(start_multiplayer_mission, reg0, "$g_multiplayer_selected_map", 0),
		(call_script, "script_game_set_multiplayer_mission_end"),
	])

# common_battle_mission_start = (
  # ti_before_mission_start, 0, 0, [],
  # [
    # (team_set_relation, 0, 2, 1),
    # (team_set_relation, 1, 3, 1),
    # (call_script, "script_change_banners_and_chest"),
    # ])

# common_battle_tab_press = (
  # ti_tab_pressed, 0, 0, [],
  # [
    # (try_begin),
      # (eq, "$g_battle_won", 1),
      # (call_script, "script_count_mission_casualties_from_agents"),
      # (finish_mission,0),
    # (else_try),
      # (call_script, "script_cf_check_enemies_nearby"),
      # (question_box,"str_do_you_want_to_retreat"),
    # (else_try),
      # (display_message,"str_can_not_retreat"),
    # (try_end),
    # ])

common_battle_init_banner = (
  ti_on_agent_spawn, 0, 0, [],
  [
    (store_trigger_param_1, ":agent_no"),
    (agent_get_troop_id, ":troop_no", ":agent_no"),
    (call_script, "script_troop_agent_set_banner", "tableau_game_troop_label_banner", ":agent_no", ":troop_no"),
  ])

mission_templates = [
    (
    "multiplayer_dm",mtf_battle_mode,-1, #deathmatch mode
    "You lead your men to battle.",
    [
      (0,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (1,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (2,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (3,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (4,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (5,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (6,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (7,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (8,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (9,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (10,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (11,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (12,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (13,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (14,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (15,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (16,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (17,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (18,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (19,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (20,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (21,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (22,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (23,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (24,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (25,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (26,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (27,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (28,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (29,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (30,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (31,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (32,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (33,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (34,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (35,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (36,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (37,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (38,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (39,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (40,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (41,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (42,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (43,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (44,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (45,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (46,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (47,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (48,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (49,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (50,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (51,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (52,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (53,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (54,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (55,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (56,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (57,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (58,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (59,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (60,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (61,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (62,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (63,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
     ],
    [
      #multiplayer_server_check_belfry_movement,      
     
      multiplayer_server_check_polls,

      (ti_on_agent_spawn, 0, 0, [],
       [
         (store_trigger_param_1, ":agent_no"),
         (call_script, "script_multiplayer_server_on_agent_spawn_common", ":agent_no"),
         ]),

      (ti_server_player_joined, 0, 0, [],
       [
         (store_trigger_param_1, ":player_no"),
         (call_script, "script_multiplayer_server_player_joined_common", ":player_no"),
         ]),

      (ti_before_mission_start, 0, 0, [],
       [
         (assign, "$g_multiplayer_game_type", multiplayer_game_type_deathmatch),
         (call_script, "script_multiplayer_server_before_mission_start_common"),
         
         (multiplayer_make_everyone_enemy),

         (call_script, "script_multiplayer_init_mission_variables"),# close this line and open map in deathmatch mod and use all ladders firstly 
         ]),                                                            # to be able to edit maps without damaging any headquarters flags ext. 

      (ti_after_mission_start, 0, 0, [], 
       [
         (set_spawn_effector_scene_prop_kind, 0, -1), #during this mission, agents of "team 0" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (set_spawn_effector_scene_prop_kind, 1, -1), #during this mission, agents of "team 1" will try to spawn around scene props with kind equal to -1(no effector for this mod)

         (call_script, "script_initialize_all_scene_prop_slots"),
         
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),

         (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
         ]),

      (ti_on_multiplayer_mission_end, 0, 0, [],
       [
#MOD begin
				(try_begin),#upkeep
					(multiplayer_is_dedicated_server),
					(call_script, "script_multiplayer_server_init_upkeep", -1),
				(try_end),
#MOD end
         #ELITE_WARRIOR achievement
         (try_begin),
           (multiplayer_get_my_player, ":my_player_no"),
           (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
           (player_get_team_no, ":my_player_team", ":my_player_no"),
           (lt, ":my_player_team", multi_team_spectator),
           (player_get_kill_count, ":kill_count", ":my_player_no"),
           (player_get_death_count, ":death_count", ":my_player_no"),
           (store_mul, ":my_score_plus_death", ":kill_count", 1000),
           (val_sub, ":my_score_plus_death", ":death_count"),
           (assign, ":continue", 1),
           (get_max_players, ":num_players"),
           (assign, ":end_cond", ":num_players"),
           (try_for_range, ":player_no", 0, ":end_cond"),
             (player_is_active, ":player_no"),
             (player_get_team_no, ":player_team", ":player_no"),
             (this_or_next|eq, ":player_team", 0),
             (eq, ":player_team", 1),
             (player_get_kill_count, ":kill_count", ":player_no"),
             (player_get_death_count, ":death_count", ":player_no"), #get_death_count
             (store_mul, ":player_score_plus_death", ":kill_count", 1000),
             (val_sub, ":player_score_plus_death", ":death_count"),
             (gt, ":player_score_plus_death", ":my_score_plus_death"),
             (assign, ":continue", 0),
             (assign, ":end_cond", 0), #break
           (try_end),
           (eq, ":continue", 1),
           (unlock_achievement, ACHIEVEMENT_ELITE_WARRIOR),
         (try_end),
         #ELITE_WARRIOR achievement end

         (call_script, "script_multiplayer_event_mission_end"),

         (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
         (start_presentation, "prsnt_multiplayer_stats_chart_deathmatch"),
         ]),

      (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
         (store_trigger_param_1, ":dead_agent_no"), 
         (store_trigger_param_2, ":killer_agent_no"),
         (call_script, "script_multiplayer_server_on_agent_killed_or_wounded_common", ":dead_agent_no", ":killer_agent_no"),
#MOD begin
         (try_begin),
           (multiplayer_is_server),
           (agent_is_human, ":dead_agent_no"),
           (neg|agent_is_non_player, ":dead_agent_no"),
           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (player_set_slot, ":dead_agent_player_id", slot_player_spawned_this_round, 0),
         (try_end),
#MOD end 
				]),
         
      #  Tests for set_shader_param_ operations
      #   (1, 0, 0, [],
      # [
      #  (str_store_string, s0, "@user_value_int"),
      #  (assign, ":int_param", 100),
      #  (assign, ":float_param", 100),
      #  (set_fixed_point_multiplier, 100),
      #  (set_shader_param_int, s0, ":int_param"),
      #  (set_shader_param_float, "@user_value_float", ":float_param"),
      #  (set_shader_param_float4, "@user_value_float4", 10, 20, 30, 40),
      #  (set_shader_param_float4x4, "@user_value_float4x4", 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120),
      #   ]),
      
      (1, 0, 0, [],
       [
         (multiplayer_is_server),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (neg|player_is_busy_with_menus, ":player_no"),

           (player_get_team_no, ":player_team", ":player_no"), #if player is currently spectator do not spawn his agent
           (lt, ":player_team", multi_team_spectator),

#           (player_get_troop_id, ":player_troop", ":player_no"), #if troop is not selected do not spawn his agent #MOD disable
#MOD begin
           (player_get_slot, ":player_troop", ":player_no", slot_player_use_troop),
#MOD end  
           (ge, ":player_troop", 0),

           (player_get_agent_id, ":player_agent", ":player_no"),
           (assign, ":spawn_new", 0),
           (try_begin),
             (player_get_slot, ":player_first_spawn", ":player_no", slot_player_first_spawn),
             (eq, ":player_first_spawn", 1),
             (assign, ":spawn_new", 1),
             (player_set_slot, ":player_no", slot_player_first_spawn, 0),
           (else_try),
             (try_begin),
               (lt, ":player_agent", 0),
               (assign, ":spawn_new", 1),
             (else_try),
               (neg|agent_is_alive, ":player_agent"),
               (agent_get_time_elapsed_since_removed, ":elapsed_time", ":player_agent"),
               (gt, ":elapsed_time", "$g_multiplayer_respawn_period"),
               (assign, ":spawn_new", 1),
             (try_end),             
           (try_end),
           (eq, ":spawn_new", 1),
           (call_script, "script_multiplayer_buy_agent_equipment", ":player_no"),

           (troop_get_inventory_slot, ":has_item", ":player_troop", ek_horse),
           (try_begin),
             (ge, ":has_item", 0),
             (assign, ":is_horseman", 1),
           (else_try),
             (assign, ":is_horseman", 0),
           (try_end),
         
           (call_script, "script_multiplayer_find_spawn_point", ":player_team", 0, ":is_horseman"), 
           (player_spawn_new_agent, ":player_no", reg0),
					 (player_set_slot, ":player_no", slot_player_spawned_this_round, 1),#MOD
         (try_end),
         ]),

      (1, 0, 0, [], #do this in every new frame, but not at the same time
       [
         (multiplayer_is_server),
         (store_mission_timer_a, ":mission_timer"),
         (ge, ":mission_timer", 2),
         (assign, ":team_1_count", 0),
         (assign, ":team_2_count", 0),
         (try_for_agents, ":cur_agent"),
           (agent_is_non_player, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (assign, ":will_be_counted", 0),
           (try_begin),
             (agent_is_alive, ":cur_agent"),
             (assign, ":will_be_counted", 1), #alive so will be counted
           (else_try),
             (agent_get_time_elapsed_since_removed, ":elapsed_time", ":cur_agent"),
             (le, ":elapsed_time", "$g_multiplayer_respawn_period"),
             (assign, ":will_be_counted", 1), 
           (try_end),
           (eq, ":will_be_counted", 1),
           (agent_get_team, ":cur_team", ":cur_agent"),
           (try_begin),
             (eq, ":cur_team", 0),
             (val_add, ":team_1_count", 1),
           (else_try),
             (eq, ":cur_team", 1),
             (val_add, ":team_2_count", 1),
           (try_end),
         (try_end),
         (store_sub, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1", ":team_1_count"),
         (store_sub, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2", ":team_2_count"),
         (val_max, "$g_multiplayer_num_bots_required_team_1", 0),
         (val_max, "$g_multiplayer_num_bots_required_team_2", 0),
         ]),

      (0, 0, 0, [],
       [
         (multiplayer_is_server),
         (eq, "$g_multiplayer_ready_for_spawning_agent", 1),
         (store_add, ":total_req", "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_required_team_2"),
         (try_begin),
           (gt, ":total_req", 0),
           (store_random_in_range, ":random_req", 0, ":total_req"),
           (val_sub, ":random_req", "$g_multiplayer_num_bots_required_team_1"),
           (try_begin),
             (lt, ":random_req", 0),
             #add to team 1
             (assign, ":selected_team", 0),
             (val_sub, "$g_multiplayer_num_bots_required_team_1", 1),
           (else_try),
             #add to team 2
             (assign, ":selected_team", 1),
             (val_sub, "$g_multiplayer_num_bots_required_team_2", 1),
           (try_end),

           (team_get_faction, ":team_faction_no", ":selected_team"),
           (assign, ":available_troops_in_faction", 0),

           (try_for_range, ":troop_no", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
             (store_troop_faction, ":troop_faction", ":troop_no"),
             (eq, ":troop_faction", ":team_faction_no"),
             (val_add, ":available_troops_in_faction", 1),
           (try_end),

           (store_random_in_range, ":random_troop_index", 0, ":available_troops_in_faction"),
           (assign, ":end_cond", multiplayer_ai_troops_end),
           (try_for_range, ":troop_no", multiplayer_ai_troops_begin, ":end_cond"),
             (store_troop_faction, ":troop_faction", ":troop_no"),
             (eq, ":troop_faction", ":team_faction_no"),
             (val_sub, ":random_troop_index", 1),
             (lt, ":random_troop_index", 0),
             (assign, ":end_cond", 0),
             (assign, ":selected_troop", ":troop_no"),
           (try_end),
         
           (troop_get_inventory_slot, ":has_item", ":selected_troop", ek_horse),
           (try_begin),
             (ge, ":has_item", 0),
             (assign, ":is_horseman", 1),
           (else_try),
             (assign, ":is_horseman", 0),
           (try_end),

           (call_script, "script_multiplayer_find_spawn_point", ":selected_team", 0, ":is_horseman"), 
           (store_current_scene, ":cur_scene"),
           (modify_visitors_at_site, ":cur_scene"),
           (add_visitors_to_current_scene, reg0, ":selected_troop", 1, ":selected_team", -1),
           (assign, "$g_multiplayer_ready_for_spawning_agent", 0),
         (try_end),
         ]),

      # (1, 0, 0, [],
       # [
         # (multiplayer_is_server),
         # checking for restarting the map
         # (assign, ":end_map", 0),
         # (try_begin),
           # (store_mission_timer_a, ":mission_timer"),
           # (store_mul, ":game_max_seconds", "$g_multiplayer_game_max_minutes", 60),
           # (gt, ":mission_timer", ":game_max_seconds"),
           # (assign, ":end_map", 1),
         # (try_end),
         # (try_begin),
           # (eq, ":end_map", 1),
           # (call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
           # (start_multiplayer_mission, reg0, "$g_multiplayer_selected_map", 0),
           # (call_script, "script_game_set_multiplayer_mission_end"),
         # (try_end),
         # ]),
        
      (ti_tab_pressed, 0, 0, [],
       [
         (try_begin),
           (eq, "$g_multiplayer_mission_end_screen", 0),
           (assign, "$g_multiplayer_stats_chart_opened_manually", 1),
           (start_presentation, "prsnt_multiplayer_stats_chart_deathmatch"),
         (try_end),
         ]),

      # multiplayer_once_at_the_first_frame,
      
      (ti_escape_pressed, 0, 0, [],
       [
         (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
         (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
         (eq, "$g_waiting_for_confirmation_to_terminate", 0),
         (start_presentation, "prsnt_multiplayer_escape_menu"),
         ]),
      ] + common_mod_multiplayer, #MOD
  ),

    (
    "multiplayer_tdm",mtf_battle_mode,-1, #team_deathmatch mode
    "You lead your men to battle.",
    [
      (0,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (1,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (2,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (3,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (4,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (5,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (6,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (7,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (8,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (9,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (10,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (11,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (12,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (13,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (14,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (15,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (16,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (17,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (18,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (19,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (20,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (21,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (22,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (23,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (24,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (25,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (26,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (27,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (28,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (29,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (30,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (31,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (32,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (33,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (34,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (35,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (36,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (37,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (38,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (39,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (40,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (41,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (42,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (43,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (44,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (45,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (46,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (47,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (48,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (49,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (50,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (51,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (52,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (53,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (54,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (55,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (56,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (57,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (58,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (59,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (60,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (61,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (62,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (63,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
     ],
    [
      common_battle_init_banner,

      multiplayer_server_check_polls,

      (ti_on_agent_spawn, 0, 0, [],
       [
         (store_trigger_param_1, ":agent_no"),
         (call_script, "script_multiplayer_server_on_agent_spawn_common", ":agent_no"),
         ]),
      
      (ti_server_player_joined, 0, 0, [],
       [
         (store_trigger_param_1, ":player_no"),
         (call_script, "script_multiplayer_server_player_joined_common", ":player_no"),
         ]),

      (ti_before_mission_start, 0, 0, [],
       [
         (assign, "$g_multiplayer_game_type", multiplayer_game_type_team_deathmatch),
         (call_script, "script_multiplayer_server_before_mission_start_common"),
#MOD begin
				 (try_begin),
           (multiplayer_is_server),
           (assign, "$g_round_start_time", 0),
         (try_end),
#MOD end
				 
         (call_script, "script_multiplayer_init_mission_variables"),
         ]),

      (ti_after_mission_start, 0, 0, [], 
       [
         (set_spawn_effector_scene_prop_kind, 0, -1), #during this mission, agents of "team 0" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (set_spawn_effector_scene_prop_kind, 1, -1), #during this mission, agents of "team 1" will try to spawn around scene props with kind equal to -1(no effector for this mod)

         (call_script, "script_initialize_all_scene_prop_slots"),
         
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),

         (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
         ]),

      (ti_on_multiplayer_mission_end, 0, 0, [],
       [
         #GLORIOUS_MOTHER_FACTION achievement
         (try_begin),
           (multiplayer_get_my_player, ":my_player_no"),
           (is_between, ":my_player_no", 0, multiplayer_max_possible_player_id),
           (player_get_team_no, ":my_player_team", ":my_player_no"),
           (lt, ":my_player_team", multi_team_spectator),
           (team_get_score, ":team_1_score", 0),
           (team_get_score, ":team_2_score", 1),
           (assign, ":continue", 0),
           (try_begin),
             (eq, ":my_player_team", 0),
             (gt, ":team_1_score", ":team_2_score"),
             (assign, ":continue", 1),
           (else_try),
             (eq, ":my_player_team", 1),
             (gt, ":team_2_score", ":team_1_score"),
             (assign, ":continue", 1),
           (try_end),
           (eq, ":continue", 1),
           (unlock_achievement, ACHIEVEMENT_GLORIOUS_MOTHER_FACTION),
         (try_end),
         #GLORIOUS_MOTHER_FACTION achievement end

         (call_script, "script_multiplayer_event_mission_end"),
         
         (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
         (start_presentation, "prsnt_multiplayer_stats_chart"),
         ]),

      (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
         (store_trigger_param_1, ":dead_agent_no"), 
         (store_trigger_param_2, ":killer_agent_no"), 
         (call_script, "script_multiplayer_server_on_agent_killed_or_wounded_common", ":dead_agent_no", ":killer_agent_no"),
         #adding 1 score points to killer agent's team. (special for "headquarters" and "team deathmatch" mod)
         (try_begin),
           (ge, ":killer_agent_no", 0),
           (agent_is_human, ":dead_agent_no"),
           (agent_is_human, ":killer_agent_no"),
           (agent_get_team, ":killer_agent_team", ":killer_agent_no"),
           (le, ":killer_agent_team", 1), #0 or 1 is ok
           (agent_get_team, ":dead_agent_team", ":dead_agent_no"),
           (neq, ":killer_agent_team", ":dead_agent_team"),
           (team_get_score, ":team_score", ":killer_agent_team"),
           (val_add, ":team_score", 1),
           (team_set_score, ":killer_agent_team", ":team_score"),
         (try_end),
#MOD begin
         (try_begin),
           (multiplayer_is_server),
           (agent_is_human, ":dead_agent_no"),
           (neg|agent_is_non_player, ":dead_agent_no"),
           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (player_set_slot, ":dead_agent_player_id", slot_player_spawned_this_round, 0),
         (try_end),
#MOD end			 
         ]),

      (1, 0, 0, [],
       [
         (multiplayer_is_server),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (neg|player_is_busy_with_menus, ":player_no"),

           (player_get_team_no, ":player_team", ":player_no"), #if player is currently spectator do not spawn his agent
           (lt, ":player_team", multi_team_spectator),

#           (player_get_troop_id, ":player_troop", ":player_no"), #if troop is not selected do not spawn his agent #MOD disable
#MOD begin
           (player_get_slot, ":player_troop", ":player_no", slot_player_use_troop),
#MOD end  
           (ge, ":player_troop", 0),

           (player_get_agent_id, ":player_agent", ":player_no"),
           (assign, ":spawn_new", 0),
           (try_begin),
             (player_get_slot, ":player_first_spawn", ":player_no", slot_player_first_spawn),
             (eq, ":player_first_spawn", 1),
             (assign, ":spawn_new", 1),
             (player_set_slot, ":player_no", slot_player_first_spawn, 0),
           (else_try),
             (try_begin),
               (lt, ":player_agent", 0),
               (assign, ":spawn_new", 1),
             (else_try),
               (neg|agent_is_alive, ":player_agent"),
               (agent_get_time_elapsed_since_removed, ":elapsed_time", ":player_agent"),
               (gt, ":elapsed_time", "$g_multiplayer_respawn_period"),
               (assign, ":spawn_new", 1),
             (try_end),             
           (try_end),
           (eq, ":spawn_new", 1),
           (call_script, "script_multiplayer_buy_agent_equipment", ":player_no"),

           # (troop_get_inventory_slot, ":has_item", ":player_troop", ek_horse),
           # (try_begin),
             # (ge, ":has_item", 0),
             # (assign, ":is_horseman", 1),
           # (else_try),
             # (assign, ":is_horseman", 0),
           # (try_end),

           # (call_script, "script_multiplayer_find_spawn_point", ":player_team", 1, ":is_horseman"), 
					 (try_begin),
             (eq, ":player_team", 0),
             (assign, ":entry_no", multi_initial_spawn_point_team_1),
           (else_try),
             (eq, ":player_team", 1),
             (assign, ":entry_no", multi_initial_spawn_point_team_2),
           (try_end),
           (player_spawn_new_agent, ":player_no", ":entry_no"),
					 (player_set_slot, ":player_no", slot_player_spawned_this_round, 1),#MOD
         (try_end),
         ]),

      (1, 0, 0, [], #do this in every new frame, but not at the same time
       [
         (multiplayer_is_server),
         (store_mission_timer_a, ":mission_timer"),
         (ge, ":mission_timer", 2),
         (assign, ":team_1_count", 0),
         (assign, ":team_2_count", 0),
         (try_for_agents, ":cur_agent"),
           (agent_is_non_player, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (assign, ":will_be_counted", 0),
           (try_begin),
             (agent_is_alive, ":cur_agent"),
             (assign, ":will_be_counted", 1), #alive so will be counted
           (else_try),
             (agent_get_time_elapsed_since_removed, ":elapsed_time", ":cur_agent"),
             (le, ":elapsed_time", "$g_multiplayer_respawn_period"),
             (assign, ":will_be_counted", 1), 
           (try_end),
           (eq, ":will_be_counted", 1),
           (agent_get_team, ":cur_team", ":cur_agent"),
           (try_begin),
             (eq, ":cur_team", 0),
             (val_add, ":team_1_count", 1),
           (else_try),
             (eq, ":cur_team", 1),
             (val_add, ":team_2_count", 1),
           (try_end),
         (try_end),
         (store_sub, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1", ":team_1_count"),
         (store_sub, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2", ":team_2_count"),
         (val_max, "$g_multiplayer_num_bots_required_team_1", 0),
         (val_max, "$g_multiplayer_num_bots_required_team_2", 0),
         ]),
      
      multiplayer_server_spawn_bots,
      multiplayer_server_manage_bots,

      (20, 0, 0, [],
       [
         (multiplayer_is_server),
         #auto team balance control in every 20 seconds (tdm)
         (call_script, "script_check_team_balance"),
         ]),

      multiplayer_server_check_end_map,
        
      (ti_tab_pressed, 0, 0, [],
       [
         (try_begin),
           (eq, "$g_multiplayer_mission_end_screen", 0),
           (assign, "$g_multiplayer_stats_chart_opened_manually", 1),
           (start_presentation, "prsnt_multiplayer_stats_chart"),
         (try_end),
         ]),

			(ti_battle_window_opened, 0, 0, [], 
			[
				(start_presentation, "prsnt_multiplayer_team_score_display"),
				(start_presentation, "prsnt_multiplayer_round_time_counter"),
			]),

      (ti_escape_pressed, 0, 0, [],
       [
         (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
         (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
         (eq, "$g_waiting_for_confirmation_to_terminate", 0),
         (start_presentation, "prsnt_multiplayer_escape_menu"),
         ]),
      ] + common_mod_multiplayer, #MOD
  ),

    (
    "multiplayer_sg",mtf_battle_mode,-1, #siege
    "You lead your men to battle.",
    [
      (0,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (1,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (2,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (3,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (4,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (5,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (6,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (7,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (8,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (9,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (10,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (11,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (12,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (13,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (14,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (15,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (16,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (17,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (18,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (19,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (20,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (21,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (22,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (23,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (24,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (25,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (26,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (27,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (28,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (29,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (30,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (31,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (32,mtef_visitor_source|mtef_team_1|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (33,mtef_visitor_source|mtef_team_1|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (34,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (35,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (36,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (37,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (38,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (39,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (40,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (41,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (42,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (43,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (44,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (45,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (46,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (47,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (48,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (49,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (50,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (51,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (52,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (53,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (54,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (55,mtef_visitor_source,0,aif_start_alarmed,1,[]),

      (56,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (57,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (58,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (59,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (60,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (61,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (62,mtef_visitor_source,0,aif_start_alarmed,1,[]),
      (63,mtef_visitor_source,0,aif_start_alarmed,1,[]),
     ],
    [
      # multiplayer_server_check_belfry_movement,      

      common_battle_init_banner,

      multiplayer_server_check_polls,
      
      (ti_server_player_joined, 0, 0, [],
       [
         (store_trigger_param_1, ":player_no"),
         (call_script, "script_multiplayer_server_player_joined_common", ":player_no"),

         (try_begin),
           (multiplayer_is_server),
           (this_or_next|player_is_active, ":player_no"),
           (eq, ":player_no", 0),
           (store_mission_timer_a, ":round_time"),
           (val_sub, ":round_time", "$g_round_start_time"),
           (try_begin),
             (lt, ":round_time", 25),
             (assign, ":number_of_respawns_spent", 0),
           (else_try),
             (lt, ":round_time", 60),
             (assign, ":number_of_respawns_spent", 1),
           (else_try),
             (lt, ":round_time", 105),
             (assign, ":number_of_respawns_spent", 2),
           (else_try),
             (lt, ":round_time", 160),
             (assign, ":number_of_respawns_spent", 3),
           (else_try),
             (assign, ":number_of_respawns_spent", "$g_multiplayer_number_of_respawn_count"),
           (try_end),
           (player_set_slot, ":player_no", slot_player_spawn_count, ":number_of_respawns_spent"),
           (multiplayer_send_int_to_player, ":player_no", multiplayer_event_return_player_respawn_spent, ":number_of_respawns_spent"),
         (try_end),
         ]),

      (ti_before_mission_start, 0, 0, [],
       [
         (assign, "$g_multiplayer_game_type", multiplayer_game_type_siege),
         (call_script, "script_multiplayer_server_before_mission_start_common"),

         (try_begin),
           (multiplayer_is_server),
           (try_for_range, ":cur_flag_slot", multi_data_flag_pull_code_begin, multi_data_flag_pull_code_end),
             (troop_set_slot, "trp_multiplayer_data", ":cur_flag_slot", -1),
           (try_end),
           (assign, "$g_my_spawn_count", 0),
         (else_try),
           (assign, "$g_my_spawn_count", 0),
         (try_end),
      
         (assign, "$g_waiting_for_confirmation_to_terminate", 0),
         (assign, "$g_round_ended", 0),
         (try_begin),
           (multiplayer_is_server),
           (assign, "$g_round_start_time", 0),
         (try_end),
         (assign, "$my_team_at_start_of_round", -1),

         (assign, "$g_flag_is_not_ready", 0),

         # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
         (call_script, "script_multiplayer_init_mission_variables"),
         ]),

      (ti_after_mission_start, 0, 0, [], 
       [
         (call_script, "script_determine_team_flags", 0),
         (set_spawn_effector_scene_prop_kind, 0, -1), #during this mission, agents of "team 0" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (set_spawn_effector_scene_prop_kind, 1, -1), #during this mission, agents of "team 1" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         
         (call_script, "script_initialize_all_scene_prop_slots"),
         
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),

         (assign, "$g_number_of_flags", 0),
         (try_begin),
           (multiplayer_is_server),
           (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
         
           #place base flags
           (entry_point_get_position, pos1, multi_siege_flag_point),
           (set_spawn_position, pos1),
           (spawn_scene_prop, "spr_headquarters_pole_code_only", 0),         
           (position_move_z, pos1, multi_headquarters_pole_height),         
           (set_spawn_position, pos1),
           (spawn_scene_prop, "$team_1_flag_scene_prop", 0),
           (store_add, ":cur_flag_slot", multi_data_flag_owner_begin, "$g_number_of_flags"),
           (troop_set_slot, "trp_multiplayer_data", ":cur_flag_slot", 1),
         (try_end),
         (val_add, "$g_number_of_flags", 1),

         # (try_begin),
           # (multiplayer_is_server),
         
           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_a"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_a", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 1),
           # (try_end),
         
           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_b"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_b", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 1),
           # (try_end),

           # (call_script, "script_move_belfries_to_their_first_entry_point", "spr_belfry_a"),
           # (call_script, "script_move_belfries_to_their_first_entry_point", "spr_belfry_b"),
         
           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_a"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_a", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing, 0),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_next_entry_point_id, 0),
           # (try_end),
         
           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_b"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_b", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing, 0),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_next_entry_point_id, 0),
           # (try_end),

           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_a"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_a", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 0),
           # (try_end),

           # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_b"),
           # (try_for_range, ":belfry_no", 0, ":num_belfries"),
             # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_b", ":belfry_no"),
             # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 0),
           # (try_end),
         # (try_end),
         ]),

      (ti_on_agent_spawn, 0, 0, [],
       [
         (store_trigger_param_1, ":agent_no"),
         (call_script, "script_multiplayer_server_on_agent_spawn_common", ":agent_no"),

         (try_begin), #if my initial team still not initialized, find and assign its value.
           (lt, "$my_team_at_start_of_round", 0),
           (multiplayer_get_my_player, ":my_player_no"),
           (ge, ":my_player_no", 0),
           (player_get_agent_id, ":my_agent_id", ":my_player_no"),
           (eq, ":my_agent_id", ":agent_no"),
           (ge, ":my_agent_id", 0),
           (agent_get_team, "$my_team_at_start_of_round", ":my_agent_id"),
         (try_end),

         (try_begin),
           (neg|multiplayer_is_server),
           (try_begin),
             (eq, "$g_round_ended", 1),
             (assign, "$g_round_ended", 0),
             (assign, "$g_my_spawn_count", 0),

             #initialize scene object slots at start of new round at clients.
             (call_script, "script_initialize_all_scene_prop_slots"),

             #these lines are done in only clients at start of each new round.
             # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
             (call_script, "script_initialize_objects_clients"),
             #end of lines
           (try_end),  
         (try_end),         

         (try_begin), 
           (multiplayer_get_my_player, ":my_player_no"),
           (ge, ":my_player_no", 0),
           (player_get_agent_id, ":my_agent_id", ":my_player_no"),
           (eq, ":my_agent_id", ":agent_no"),

           (val_add, "$g_my_spawn_count", 1),
         
           (try_begin),
             (ge, "$g_my_spawn_count", "$g_multiplayer_number_of_respawn_count"),
             (gt, "$g_multiplayer_number_of_respawn_count", 0),
             (multiplayer_get_my_player, ":my_player_no"),
             (player_get_team_no, ":my_player_team_no", ":my_player_no"),
             (eq, ":my_player_team_no", 0),
             (assign, "$g_my_spawn_count", 999),
           (try_end),
         (try_end),
         ]),

      (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
         (store_trigger_param_1, ":dead_agent_no"),
         (store_trigger_param_2, ":killer_agent_no"),

         (call_script, "script_multiplayer_server_on_agent_killed_or_wounded_common", ":dead_agent_no", ":killer_agent_no"),

         (try_begin), #if my initial team still not initialized, find and assign its value.
           (lt, "$my_team_at_start_of_round", 0),
           (multiplayer_get_my_player, ":my_player_no"),
           (ge, ":my_player_no", 0),
           (player_get_agent_id, ":my_agent_id", ":my_player_no"),
           (ge, ":my_agent_id", 0),
           (agent_get_team, "$my_team_at_start_of_round", ":my_agent_id"),
         (try_end),         
         
         (try_begin),
           (multiplayer_is_server),
           (agent_is_human, ":dead_agent_no"),
           (neg|agent_is_non_player, ":dead_agent_no"),
           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (player_set_slot, ":dead_agent_player_id", slot_player_spawned_this_round, 0),
         (try_end),
         ]),

      (ti_on_multiplayer_mission_end, 0, 0, [],
       [
         (call_script, "script_multiplayer_event_mission_end"),
         (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
         (start_presentation, "prsnt_multiplayer_stats_chart"),
         ]),
      
      (0, 0, 0, [], #if this trigger takes lots of time in the future and make server machine runs siege mod
                    #very slow with lots of players make period of this trigger 1 seconds, but best is 0. Currently
                    #we are testing this mod with few players and no speed problem occured.
      [
        (multiplayer_is_server),
        (eq, "$g_round_ended", 0),
        #main trigger which controls which agent is moving/near which flag.
        (try_for_range, ":flag_no", 0, "$g_number_of_flags"),
          (store_add, ":cur_flag_owner_counts_slot", multi_data_flag_players_around_begin, ":flag_no"),
          (troop_get_slot, ":current_owner_code", "trp_multiplayer_data", ":cur_flag_owner_counts_slot"),
          (store_div, ":old_team_1_agent_count", ":current_owner_code", 100),
          (store_mod, ":old_team_2_agent_count", ":current_owner_code", 100),
        
          (assign, ":number_of_agents_around_flag_team_1", 0),
          (assign, ":number_of_agents_around_flag_team_2", 0),

          (scene_prop_get_instance, ":pole_id", "spr_headquarters_pole_code_only", ":flag_no"), 
          (prop_instance_get_position, pos0, ":pole_id"), #pos0 holds pole position.

          (get_max_players, ":num_players"),
            (try_for_range, ":player_no", 0, ":num_players"),
            (player_is_active, ":player_no"),
            (player_get_agent_id, ":cur_agent", ":player_no"),            
            (ge, ":cur_agent", 0),
            (agent_is_alive, ":cur_agent"),
            (agent_get_team, ":cur_agent_team", ":cur_agent"),
            (agent_get_position, pos1, ":cur_agent"), #pos1 holds agent's position.
            (get_sq_distance_between_positions, ":squared_dist", pos0, pos1),
            (get_sq_distance_between_position_heights, ":squared_height_dist", pos0, pos1),
            (val_add, ":squared_dist", ":squared_height_dist"),
            (lt, ":squared_dist", multi_headquarters_max_distance_sq_to_raise_flags),
            (try_begin),
              (eq, ":cur_agent_team", 0),
              (val_add, ":number_of_agents_around_flag_team_1", 1),
            (else_try),
              (eq, ":cur_agent_team", 1),
              (val_add, ":number_of_agents_around_flag_team_2", 1),
            (try_end),
          (try_end),

          (try_begin),
            (this_or_next|neq, ":old_team_1_agent_count", ":number_of_agents_around_flag_team_1"),
            (neq, ":old_team_2_agent_count", ":number_of_agents_around_flag_team_2"),

            (store_add, ":cur_flag_pull_code_slot", multi_data_flag_pull_code_begin, ":flag_no"),
            (troop_get_slot, ":cur_flag_pull_code", "trp_multiplayer_data", ":cur_flag_pull_code_slot"),
            (store_mod, ":cur_flag_pull_message_seconds_past", ":cur_flag_pull_code", 100),
            (store_div, ":cur_flag_puller_team_last", ":cur_flag_pull_code", 100),

            (try_begin),        
              (eq, ":old_team_2_agent_count", 0),
              (gt, ":number_of_agents_around_flag_team_2", 0),
              (eq, ":number_of_agents_around_flag_team_1", 0),
              (assign, ":puller_team", 2),

              (store_mul, ":puller_team_multiplied_by_100", ":puller_team", 100),
              (troop_set_slot, "trp_multiplayer_data", ":cur_flag_pull_code_slot", ":puller_team_multiplied_by_100"),

              (this_or_next|neq, ":cur_flag_puller_team_last", ":puller_team"),
              (ge, ":cur_flag_pull_message_seconds_past", 25),

              (store_mul, ":flag_code", ":puller_team", 100),
              (val_add, ":flag_code", ":flag_no"),
            (try_end),

            (try_begin),
              (store_mul, ":current_owner_code", ":number_of_agents_around_flag_team_1", 100),
              (val_add, ":current_owner_code", ":number_of_agents_around_flag_team_2"),        
              (troop_set_slot, "trp_multiplayer_data", ":cur_flag_owner_counts_slot", ":current_owner_code"),
              (get_max_players, ":num_players"),
              #for only server itself-----------------------------------------------------------------------------------------------
              (call_script, "script_set_num_agents_around_flag", ":flag_no", ":current_owner_code"),
              #for only server itself-----------------------------------------------------------------------------------------------
              (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
                (player_is_active, ":player_no"),
                (multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_set_num_agents_around_flag, ":flag_no", ":current_owner_code"),
              (try_end),
            (try_end),
          (try_end),
        (try_end),

        (try_for_range, ":flag_no", 0, "$g_number_of_flags"),
          (eq, "$g_round_ended", 0), #if round still continues and any team did not sucseed yet
          (eq, "$g_flag_is_not_ready", 0), #if round still continues and any team did not sucseed yet
        
          (scene_prop_get_instance, ":pole_id", "spr_headquarters_pole_code_only", ":flag_no"), 
          (prop_instance_get_position, pos0, ":pole_id"), #pos0 holds pole position.            

          (try_begin),
            (scene_prop_get_instance, ":flag_id", "$team_1_flag_scene_prop", ":flag_no"),

            #flag_id holds shown flag after this point
            (prop_instance_get_position, pos1, ":flag_id"), #pos1 holds gray/red/blue (current shown) flag position.
            (try_begin),
              (get_sq_distance_between_positions, ":squared_dist", pos0, pos1),        
              (lt, ":squared_dist", multi_headquarters_distance_sq_to_change_flag), #if distance is less than 2 meters
              
              (prop_instance_is_animating, ":is_animating", ":flag_id"),
              (eq, ":is_animating", 1),

              #end of round, attackers win
              (assign, "$g_winner_team", 1),
              (prop_instance_stop_animating, ":flag_id"),        
        
              (get_max_players, ":num_players"), 
              #for only server itself-----------------------------------------------------------------------------------------------
              (call_script, "script_draw_this_round", "$g_winner_team"),
              #for only server itself-----------------------------------------------------------------------------------------------
              (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
                (player_is_active, ":player_no"),
                (multiplayer_send_int_to_player, ":player_no", multiplayer_event_draw_this_round, "$g_winner_team"),
              (try_end),

              (assign, "$g_flag_is_not_ready", 1),
            (try_end),        
          (try_end),
        (try_end),
        ]),

      (0, 0, 0, [], #if there is nobody in any teams do not reduce round time.
       [
         #(multiplayer_is_server),
         (assign, ":human_agents_spawned_at_team_1", "$g_multiplayer_num_bots_team_1"),
         (assign, ":human_agents_spawned_at_team_2", "$g_multiplayer_num_bots_team_2"),
         
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_get_team_no, ":player_team", ":player_no"), 
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":human_agents_spawned_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":human_agents_spawned_at_team_2", 1),
           (try_end),
         (try_end),

         (try_begin),
           (this_or_next|eq, ":human_agents_spawned_at_team_1", 0),
           (eq, ":human_agents_spawned_at_team_2", 0),

           (store_mission_timer_a, ":seconds_past_since_round_started"),
           (val_sub, ":seconds_past_since_round_started", "$g_round_start_time"),
           (le, ":seconds_past_since_round_started", 2),
                  
           (store_mission_timer_a, "$g_round_start_time"),
         (try_end),
       ]),

      (1, 0, 0, [(multiplayer_is_server),
                 (eq, "$g_round_ended", 0),
                 (eq, "$g_flag_is_not_ready", 0),
                 (store_mission_timer_a, ":current_time"),
                 (store_sub, ":seconds_past_in_round", ":current_time", "$g_round_start_time"),
                 (ge, ":seconds_past_in_round", "$g_multiplayer_round_max_seconds")],
       [
         (assign, ":flag_no", 0),
         (store_add, ":cur_flag_owner_counts_slot", multi_data_flag_players_around_begin, ":flag_no"),
         (troop_get_slot, ":current_owner_code", "trp_multiplayer_data", ":cur_flag_owner_counts_slot"),
         (store_mod, ":team_2_agent_count_around_flag", ":current_owner_code", 100),

         (try_begin),
           (eq, ":team_2_agent_count_around_flag", 0),
         
           (store_mission_timer_a, "$g_round_finish_time"),
           (assign, "$g_round_ended", 1),

           (assign, "$g_flag_is_not_ready", 1),
        
           (assign, "$g_winner_team", 0),

           (get_max_players, ":num_players"),
           #for only server itself-----------------------------------------------------------------------------------------------
           (call_script, "script_draw_this_round", "$g_winner_team"),
           #for only server itself-----------------------------------------------------------------------------------------------
           (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
             (player_is_active, ":player_no"),
             (multiplayer_send_int_to_player, ":player_no", multiplayer_event_draw_this_round, "$g_winner_team"),
           (try_end),
         (try_end),
         ]),          

      (1, 0, 0, [],
      [
        (multiplayer_is_server),
        #trigger for calculating seconds past after that flag's pull message has shown          
        (try_for_range, ":flag_no", 0, "$g_number_of_flags"),
          (store_add, ":cur_flag_pull_code_slot", multi_data_flag_pull_code_begin, ":flag_no"),
          (troop_get_slot, ":cur_flag_pull_code", "trp_multiplayer_data", ":cur_flag_pull_code_slot"),
          (store_mod, ":cur_flag_pull_message_seconds_past", ":cur_flag_pull_code", 100),
          (try_begin),
            (ge, ":cur_flag_pull_code", 100),
            (lt, ":cur_flag_pull_message_seconds_past", 25),
            (val_add, ":cur_flag_pull_code", 1),
            (troop_set_slot, "trp_multiplayer_data", ":cur_flag_pull_code_slot", ":cur_flag_pull_code"),
          (try_end),
        (try_end),        
      ]),               

      (10, 0, 0, [(multiplayer_is_server)],
       [
         #auto team balance control during the round         
         (assign, ":number_of_players_at_team_1", 0),
         (assign, ":number_of_players_at_team_2", 0),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 0, ":num_players"),
           (player_is_active, ":cur_player"),
           (player_get_team_no, ":player_team", ":cur_player"),
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":number_of_players_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":number_of_players_at_team_2", 1),
           (try_end),         
         (try_end),
         #end of counting active players per team.
         (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),
         (assign, ":number_of_players_will_be_moved", 0),
         (try_begin),
           (try_begin),
             (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
             (le, ":difference_of_number_of_players", ":checked_value"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", -2),
           (else_try),
             (ge, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", 2),
           (try_end),          
         (try_end),         
         #number of players will be moved calculated. (it is 0 if no need to make team balance)
         (try_begin),
           (gt, ":number_of_players_will_be_moved", 0),
           (try_begin),
             (eq, "$g_team_balance_next_round", 0),
         
             (assign, "$g_team_balance_next_round", 1),

             #for only server itself-----------------------------------------------------------------------------------------------
             (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_next, 0), #0 is useless here
             #for only server itself-----------------------------------------------------------------------------------------------     
             (get_max_players, ":num_players"),                               
             (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
               (player_is_active, ":player_no"),
               (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_next),
             (try_end),
             
             (call_script, "script_warn_player_about_auto_team_balance"),
           (try_end),
         (try_end),           
         #team balance check part finished
         ]),          

      (1, 0, 3, [(multiplayer_is_server),
                 (eq, "$g_round_ended", 1),
                 (store_mission_timer_a, ":seconds_past_till_round_ended"),
                 (val_sub, ":seconds_past_till_round_ended", "$g_round_finish_time"),
                 (ge, ":seconds_past_till_round_ended", "$g_multiplayer_respawn_period")],
       [
         #auto team balance control at the end of round         
         (assign, ":number_of_players_at_team_1", 0),
         (assign, ":number_of_players_at_team_2", 0),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 0, ":num_players"),
           (player_is_active, ":cur_player"),
           (player_get_team_no, ":player_team", ":cur_player"),
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":number_of_players_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":number_of_players_at_team_2", 1),
           (try_end),         
         (try_end),
         #end of counting active players per team.
         (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),
         (assign, ":number_of_players_will_be_moved", 0),
         (try_begin),
           (try_begin),
             (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
             (le, ":difference_of_number_of_players", ":checked_value"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", -2),
             (assign, ":team_with_more_players", 1),
             (assign, ":team_with_less_players", 0),
           (else_try),
             (ge, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", 2),
             (assign, ":team_with_more_players", 0),
             (assign, ":team_with_less_players", 1),
           (try_end),          
         (try_end),         
         #number of players will be moved calculated. (it is 0 if no need to make team balance)
         (try_begin),
           (gt, ":number_of_players_will_be_moved", 0),
           (try_begin),
             (try_for_range, ":unused", 0, ":number_of_players_will_be_moved"), 
               (assign, ":max_player_join_time", 0),
               (assign, ":latest_joined_player_no", -1),
               (get_max_players, ":num_players"),                               
               (try_for_range, ":player_no", 0, ":num_players"),
                 (player_is_active, ":player_no"),
                 (player_get_team_no, ":player_team", ":player_no"),
                 (eq, ":player_team", ":team_with_more_players"),
                 (player_get_slot, ":player_join_time", ":player_no", slot_player_join_time),
                 (try_begin),
                   (gt, ":player_join_time", ":max_player_join_time"),
                   (assign, ":max_player_join_time", ":player_join_time"),
                   (assign, ":latest_joined_player_no", ":player_no"),
                 (try_end),
               (try_end),
               (try_begin),
                 (ge, ":latest_joined_player_no", 0),
                 (try_begin),
                   #if player is living add +1 to his kill count because he will get -1 because of team change while living.
                   (player_get_agent_id, ":latest_joined_agent_id", ":latest_joined_player_no"), 
                   (ge, ":latest_joined_agent_id", 0),
                   (agent_is_alive, ":latest_joined_agent_id"),
#MOD disable
                   # (player_get_kill_count, ":player_kill_count", ":latest_joined_player_no"), #adding 1 to his kill count, because he will lose 1 undeserved kill count for dying during team change
                   # (val_add, ":player_kill_count", 1),
                   # (player_set_kill_count, ":latest_joined_player_no", ":player_kill_count"),

                   (player_get_death_count, ":player_death_count", ":latest_joined_player_no"), #subtracting 1 to his death count, because he will gain 1 undeserved death count for dying during team change
                   (val_sub, ":player_death_count", 1),
                   (player_set_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD begin
									(try_begin),
										(player_get_slot, ":cur_player_deaths", ":latest_joined_player_no", slot_player_deaths),
										(val_sub, ":cur_player_deaths", 1),
										(player_set_slot, ":latest_joined_player_no", slot_player_deaths, ":cur_player_deaths"),
									(try_end),
#MOD end

#MOD disable
                   # (player_get_score, ":player_score", ":latest_joined_player_no"), #adding 1 to his score count, because he will lose 1 undeserved score for dying during team change
                   # (val_add, ":player_score", 1),
                   # (player_set_score, ":latest_joined_player_no", ":player_score"),

                   (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
                     (player_is_active, ":player_no"),
#MOD begin											 
#                     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_set_player_score_kill_death, ":latest_joined_player_no", ":player_score", ":player_kill_count", ":player_death_count"),
										(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD end
                   (try_end),         
#MOD begin
				   # (player_get_slot, ":troop_no", ":latest_joined_player_no", slot_player_use_troop),
				   # (try_begin),
					 # (neq, ":troop_no", "trp_online_character"),
                     # (player_get_value_of_original_items, ":old_items_value", ":latest_joined_player_no"),
                     # (player_get_gold, ":player_gold", ":latest_joined_player_no"),
                     # (val_add, ":player_gold", ":old_items_value"),
                     # (player_set_gold, ":latest_joined_player_no", ":player_gold", multi_max_gold_that_can_be_stored),
                   # (try_end),
				 (try_end),
#                 (player_set_troop_id, ":latest_joined_player_no", -1),#MOD disable
                 (player_set_slot, ":latest_joined_player_no", slot_player_use_troop, -1),
#MOD end
                 (player_set_team_no, ":latest_joined_player_no", ":team_with_less_players"),
                 (multiplayer_send_message_to_player, ":latest_joined_player_no", multiplayer_event_force_start_team_selection),
               (try_end),
             (try_end),
             #tutorial message (after team balance)
             
             #(tutorial_message_set_position, 500, 500),
             #(tutorial_message_set_size, 30, 30),
             #(tutorial_message_set_center_justify, 1),
             #(tutorial_message, "str_auto_team_balance_done", 0xFFFFFFFF, 5),
             
             #for only server itself
             (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_done, 0), 

             #no need to send also server here
             (multiplayer_get_my_player, ":my_player_no"),
             (get_max_players, ":num_players"),                               
             (try_for_range, ":player_no", 0, ":num_players"),
               (player_is_active, ":player_no"),
               (neq, ":my_player_no", ":player_no"),
               (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_done),
             (try_end),
             (assign, "$g_team_balance_next_round", 0),
           (try_end),
         (try_end),           
         #team balance check part finished
         (assign, "$g_team_balance_next_round", 0),

         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_set_slot, ":player_no", slot_player_spawned_this_round, 0),
           (player_set_slot, ":player_no", slot_player_spawned_at_siege_round, 0),  				 
           (player_get_agent_id, ":player_agent", ":player_no"),
           (ge, ":player_agent", 0),
           (agent_is_alive, ":player_agent"),
           # (player_save_picked_up_items_for_next_spawn, ":player_no"), #MOD disable
           (player_get_value_of_original_items, ":old_items_value", ":player_no"),
           (player_set_slot, ":player_no", slot_player_last_rounds_used_item_earnings, ":old_items_value"),
         (try_end),

         #money management
         (assign, ":per_round_gold_addition", multi_battle_round_team_money_add),
         (val_mul, ":per_round_gold_addition", "$g_multiplayer_round_earnings_multiplier"),
         (val_div, ":per_round_gold_addition", 100),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_get_gold, ":player_gold", ":player_no"),
           (player_get_team_no, ":player_team", ":player_no"),
         
           (try_begin),
             (this_or_next|eq, ":player_team", 0),
             (eq, ":player_team", 1),
             (val_add, ":player_gold", ":per_round_gold_addition"), 
           (try_end),
#MOD disable
           # #(below lines added new at 25.11.09 after Armagan decided new money system)
           # (try_begin),
             # (player_get_slot, ":old_items_value", ":player_no", slot_player_last_rounds_used_item_earnings),
             # (store_add, ":player_total_potential_gold", ":player_gold", ":old_items_value"),
             # (store_mul, ":minimum_gold", "$g_multiplayer_initial_gold_multiplier", 10),
             # (lt, ":player_total_potential_gold", ":minimum_gold"),
             # (store_sub, ":additional_gold", ":minimum_gold", ":player_total_potential_gold"),
             # (val_add, ":player_gold", ":additional_gold"),
           # (try_end),
           # #new money system addition end
         (try_end),

         #initialize my team at start of round (it will be assigned again at next round's first death)
         (assign, "$my_team_at_start_of_round", -1),

         #clear scene and end round
         (multiplayer_clear_scene),
         
         #assigning everbody's spawn counts to 0
         (assign, "$g_my_spawn_count", 0),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_set_slot, ":player_no", slot_player_spawn_count, 0),
         (try_end),

         #(call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
         (call_script, "script_initialize_objects"),

         #initialize moveable object positions
         # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
         (call_script, "script_multiplayer_close_gate_if_it_is_open"),
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),
         # (call_script, "script_move_belfries_to_their_first_entry_point", "spr_belfry_a"),
         # (call_script, "script_move_belfries_to_their_first_entry_point", "spr_belfry_b"),
         
         # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_a"),
         # (try_for_range, ":belfry_no", 0, ":num_belfries"),
           # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_a", ":belfry_no"),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing, 0),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_next_entry_point_id, 0),
         # (try_end),

         # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_a"),
         # (try_for_range, ":belfry_no", 0, ":num_belfries"),
           # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_a", ":belfry_no"),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 0),
         # (try_end),

         # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_b"),
         # (try_for_range, ":belfry_no", 0, ":num_belfries"),
           # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_b", ":belfry_no"),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_number_of_agents_pushing, 0),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_next_entry_point_id, 0),
         # (try_end),

         # (scene_prop_get_num_instances, ":num_belfries", "spr_belfry_b"),
         # (try_for_range, ":belfry_no", 0, ":num_belfries"),
           # (scene_prop_get_instance, ":belfry_scene_prop_id", "spr_belfry_b", ":belfry_no"),
           # (scene_prop_set_slot, ":belfry_scene_prop_id", scene_prop_belfry_platform_moved, 0),
         # (try_end),

         #initialize flag coordinates (move up the flag at pole)
         (try_for_range, ":flag_no", 0, "$g_number_of_flags"),
           (scene_prop_get_instance, ":pole_id", "spr_headquarters_pole_code_only", ":flag_no"),
           (prop_instance_get_position, pos1, ":pole_id"),
           (position_move_z, pos1, multi_headquarters_pole_height),
           (scene_prop_get_instance, ":flag_id", "$team_1_flag_scene_prop", ":flag_no"),
           (prop_instance_stop_animating, ":flag_id"),
           (prop_instance_set_position, ":flag_id", pos1),
         (try_end),
         
         (assign, "$g_round_ended", 0),
         
         (store_mission_timer_a, "$g_round_start_time"),
         (call_script, "script_initialize_all_scene_prop_slots"),

				 (call_script, "script_init_on_round_start"),#MOD
         #initialize round start time for clients
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (multiplayer_send_int_to_player, ":player_no", multiplayer_event_set_round_start_time, -9999),
         (try_end),         

         (assign, "$g_flag_is_not_ready", 0),
       ]),
           
      (1, 0, 0, [],
       [
         (multiplayer_is_server),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (neg|player_is_busy_with_menus, ":player_no"),
           (player_slot_eq, ":player_no", slot_player_spawned_this_round, 0),

           (player_get_team_no, ":player_team", ":player_no"), #if player is currently spectator do not spawn his agent
           (lt, ":player_team", multi_team_spectator),
#           (player_get_troop_id, ":player_troop", ":player_no"), #if troop is not selected do not spawn his agent #MOD disable
#MOD begin
           (player_get_slot, ":player_troop", ":player_no", slot_player_use_troop),
#MOD end  
           (ge, ":player_troop", 0),
           (player_get_agent_id, ":player_agent", ":player_no"), #new added for siege mod
         
           (assign, ":spawn_new", 0), 
           (assign, ":num_active_players_in_team_0", 0),
           (assign, ":num_active_players_in_team_1", 0),
           (try_begin),
             (assign, ":num_active_players", 0),
             (get_max_players, ":num_players"),
             (try_for_range, ":cur_player", 0, ":num_players"),
               (player_is_active, ":cur_player"),

               (player_get_team_no, ":cur_player_team", ":cur_player"),
               (try_begin),
                 (eq, ":cur_player_team", 0),
                 (val_add, ":num_active_players_in_team_0", 1),
               (else_try),
                 (eq, ":cur_player_team", 1),
                 (val_add, ":num_active_players_in_team_1", 1),
               (try_end),

               (val_add, ":num_active_players", 1),
             (try_end),
             (store_mission_timer_a, ":round_time"),
             (val_sub, ":round_time", "$g_round_start_time"),
                  
             (eq, "$g_round_ended", 0),
         
             (try_begin), #addition for siege mod to allow players spawn more than once (begin)
               (lt, ":player_agent", 0), 

               (try_begin), #new added begin, to avoid siege-crack (rejoining of defenders when they die)
                 (eq, ":player_team", 0), 
                 (player_get_slot, ":player_last_team_select_time", ":player_no", slot_player_last_team_select_time),
                 (store_mission_timer_a, ":current_time"),
                 (store_sub, ":elapsed_time", ":current_time", ":player_last_team_select_time"),
                 
                 (assign, ":player_team_respawn_period", "$g_multiplayer_respawn_period"), 
                 # (val_add, ":player_team_respawn_period", multiplayer_siege_mod_defender_team_extra_respawn_time), #new added for siege mod
                 (lt, ":elapsed_time", ":player_team_respawn_period"),

                 (store_sub, ":round_time", ":current_time", "$g_round_start_time"),
                 (ge, ":round_time", multiplayer_new_agents_finish_spawning_time),
                 (gt, ":num_active_players", 2),
                 (store_mul, ":multipication_of_num_active_players_in_teams", ":num_active_players_in_team_0", ":num_active_players_in_team_1"),
                 (neq, ":multipication_of_num_active_players_in_teams", 0),
         
                 (assign, ":spawn_new", 0),
               (else_try), #new added end         
                 (assign, ":spawn_new", 1),
               (try_end),
             (else_try), 
               (agent_get_time_elapsed_since_removed, ":elapsed_time", ":player_agent"), 
               (assign, ":player_team_respawn_period", "$g_multiplayer_respawn_period"), 
               # (try_begin), 
                 # (eq, ":player_team", 0), 
                 # (val_add, ":player_team_respawn_period", multiplayer_siege_mod_defender_team_extra_respawn_time), 
               # (try_end), 
               (this_or_next|gt, ":elapsed_time", ":player_team_respawn_period"), 
               (player_slot_eq, ":player_no", slot_player_spawned_at_siege_round, 0), 
               (assign, ":spawn_new", 1),
             (try_end), 
           (try_end), #addition for siege mod to allow players spawn more than once (end)

           (player_get_slot, ":spawn_count", ":player_no", slot_player_spawn_count),

           (try_begin),
             (gt, "$g_multiplayer_number_of_respawn_count", 0),
             (try_begin),
               (eq, ":spawn_new", 1),
               (eq, ":player_team", 0),
               (ge, ":spawn_count", "$g_multiplayer_number_of_respawn_count"),
               (assign, ":spawn_new", 0),
             (else_try),
               (eq, ":spawn_new", 1),
               (eq, ":player_team", 1),      
               (ge, ":spawn_count", 999),
               (assign, ":spawn_new", 0),
             (try_end),
           (try_end),

           (eq, ":spawn_new", 1),

           (call_script, "script_multiplayer_buy_agent_equipment", ":player_no"),

           (player_get_slot, ":spawn_count", ":player_no", slot_player_spawn_count),
           (val_add, ":spawn_count", 1),
           (player_set_slot, ":player_no", slot_player_spawn_count, ":spawn_count"),

           (try_begin),
             (ge, ":spawn_count", "$g_multiplayer_number_of_respawn_count"),
             (gt, "$g_multiplayer_number_of_respawn_count", 0),
             (eq, ":player_team", 0),
             (assign, ":spawn_count", 999),
             (player_set_slot, ":player_no", slot_player_spawn_count, ":spawn_count"),
           (try_end),

           (assign, ":player_is_horseman", 0),
           (player_get_item_id, ":item_id", ":player_no", ek_horse),
           (try_begin),
             (gt, ":item_id", -1),#MOD
             (assign, ":player_is_horseman", 1),
           (try_end),
#MOD disable
           # (try_begin),
             # (lt, ":round_time", 20), #at start of game spawn at base entry point (only enemies)
             # (try_begin),         
               # (eq, ":player_team", 0), #defenders in siege mod at start of round
               # (call_script, "script_multiplayer_find_spawn_point", ":player_team", 1, ":player_is_horseman"),
               # (assign, ":entry_no", reg0),             
             # (else_try),
               # (eq, ":player_team", 1), #attackers in siege mod at start of round
               # (assign, ":entry_no", multi_initial_spawn_point_team_2), #change later
             # (try_end),
           # (else_try),
             (call_script, "script_multiplayer_find_spawn_point", ":player_team", 0, ":player_is_horseman"),
             (assign, ":entry_no", reg0),             
           # (try_end),
         
           (player_spawn_new_agent, ":player_no", ":entry_no"),
           (player_set_slot, ":player_no", slot_player_spawned_this_round, 1),
           (player_set_slot, ":player_no", slot_player_spawned_at_siege_round, 1),         
         (try_end),
         ]),

      (1, 0, 0, [], #do this in every new frame, but not at the same time
       [
         (multiplayer_is_server),
         (store_mission_timer_a, ":mission_timer"),
         (ge, ":mission_timer", 2),
         (assign, ":team_1_count", 0),
         (assign, ":team_2_count", 0),
         (try_for_agents, ":cur_agent"),
           (agent_is_non_player, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (assign, ":will_be_counted", 0),
           (try_begin),
             (agent_is_alive, ":cur_agent"),
             (assign, ":will_be_counted", 1), #alive so will be counted
           (else_try),
             (agent_get_time_elapsed_since_removed, ":elapsed_time", ":cur_agent"),
             (le, ":elapsed_time", "$g_multiplayer_respawn_period"),
             (assign, ":will_be_counted", 1), 
           (try_end),
           (eq, ":will_be_counted", 1),
           (agent_get_team, ":cur_team", ":cur_agent"),
           (try_begin),
             (eq, ":cur_team", 0),
             (val_add, ":team_1_count", 1),
           (else_try),
             (eq, ":cur_team", 1),
             (val_add, ":team_2_count", 1),
           (try_end),
         (try_end),
         (store_sub, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1", ":team_1_count"),
         (store_sub, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2", ":team_2_count"),
         (val_max, "$g_multiplayer_num_bots_required_team_1", 0),
         (val_max, "$g_multiplayer_num_bots_required_team_2", 0),
         ]),

      multiplayer_server_spawn_bots, 
      multiplayer_server_manage_bots, 

      multiplayer_server_check_end_map,
        
      (ti_tab_pressed, 0, 0, [],
       [
         (try_begin),
           (eq, "$g_multiplayer_mission_end_screen", 0),
           (assign, "$g_multiplayer_stats_chart_opened_manually", 1),
           (start_presentation, "prsnt_multiplayer_stats_chart"),
         (try_end),
         ]),

      # multiplayer_once_at_the_first_frame,

      (ti_battle_window_opened, 0, 0, [], [
				(eq, "$g_disable_hud", 0),#MOD
        (start_presentation, "prsnt_multiplayer_round_time_counter"),
        (start_presentation, "prsnt_multiplayer_team_score_display"),
        ]),

      (ti_escape_pressed, 0, 0, [],
       [
         (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
         (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
         (eq, "$g_waiting_for_confirmation_to_terminate", 0),
         (start_presentation, "prsnt_multiplayer_escape_menu"),
         ]),
      ] + common_mod_multiplayer, #MOD
  ),

    (
    "multiplayer_bt",mtf_battle_mode,-1, #battle mode
    "You lead your men to battle.",
    [
      (0,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (1,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (2,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (3,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (4,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (5,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (6,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (7,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (8,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (9,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (10,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (11,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (12,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (13,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (14,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (15,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (16,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (17,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (18,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (19,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (20,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (21,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (22,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (23,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (24,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (25,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (26,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (27,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (28,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (29,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (30,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (31,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (32,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (33,mtef_visitor_source|mtef_team_0|mtef_no_auto_reset,0,aif_start_alarmed,1,[]),
      (34,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (35,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (36,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (37,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (38,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (39,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (40,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (41,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (42,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (43,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (44,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (45,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (46,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (47,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (48,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (49,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (50,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (51,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (52,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (53,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (54,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (55,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (56,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (57,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (58,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (59,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (60,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (61,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (62,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (63,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
     ],
    [
      common_battle_init_banner,

      multiplayer_server_check_polls,
      
      (ti_server_player_joined, 0, 0, [],
       [
         (store_trigger_param_1, ":player_no"),
         (call_script, "script_multiplayer_server_player_joined_common", ":player_no"),
         ]),

      (ti_before_mission_start, 0, 0, [],
       [
         (assign, "$g_multiplayer_game_type", multiplayer_game_type_battle),
         (call_script, "script_multiplayer_server_before_mission_start_common"),
         
         (assign, "$g_waiting_for_confirmation_to_terminate", 0),
         (assign, "$g_round_ended", 0),
         # (assign, "$g_battle_death_mode_started", 0),
         (assign, "$g_reduced_waiting_seconds", 0),

         (try_begin),
           (multiplayer_is_server),
           (assign, "$server_mission_timer_while_player_joined", 0),
           (assign, "$g_round_start_time", 0),
         (try_end),
         (assign, "$my_team_at_start_of_round", -1),

         (call_script, "script_multiplayer_init_mission_variables"),
         ]),

      (ti_after_mission_start, 0, 0, [], 
       [
         (call_script, "script_determine_team_flags", 0),
         (call_script, "script_determine_team_flags", 1),
         (set_spawn_effector_scene_prop_kind, 0, -1), #during this mission, agents of "team 0" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (set_spawn_effector_scene_prop_kind, 1, -1), #during this mission, agents of "team 1" will try to spawn around scene props with kind equal to -1(no effector for this mod)

         (try_begin),
           (multiplayer_is_server),

           (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
         
           # (entry_point_get_position, pos0, multi_death_mode_point),
           # (position_set_z_to_ground_level, pos0),
           # (position_move_z, pos0, -2000),

           # (position_move_x, pos0, 100), 
           # (set_spawn_position, pos0),
           # (spawn_scene_prop, "spr_headquarters_pole_code_only", 0),

           # (position_move_x, pos0, -200), 
           # (set_spawn_position, pos0),
           # (spawn_scene_prop, "spr_headquarters_pole_code_only", 0),

           # (scene_prop_get_instance, ":pole_1_id", "spr_headquarters_pole_code_only", 0),
           # (prop_instance_get_position, pos0, ":pole_1_id"),
           # (spawn_scene_prop, "$team_1_flag_scene_prop", 0),
           # (position_move_z, pos0, multi_headquarters_flag_initial_height),
           # (prop_instance_set_position, reg0, pos0),
         
           # (scene_prop_get_instance, ":pole_2_id", "spr_headquarters_pole_code_only", 1),
           # (prop_instance_get_position, pos0, ":pole_2_id"),
           # (spawn_scene_prop, "$team_2_flag_scene_prop", 0),
           # (position_move_z, pos0, multi_headquarters_flag_initial_height),
           # (prop_instance_set_position, reg0, pos0),

           (assign, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1"), 
           (assign, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2"), 
         (try_end),

         (call_script, "script_initialize_all_scene_prop_slots"),
         
         # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),
         ]),

      (ti_on_agent_spawn, 0, 0, [],
       [
         (store_trigger_param_1, ":agent_no"),
         (call_script, "script_multiplayer_server_on_agent_spawn_common", ":agent_no"),
         
         (try_begin), #if my initial team still not initialized, find and assign its value.
           (lt, "$my_team_at_start_of_round", 0),
           (multiplayer_get_my_player, ":my_player_no"),
           (ge, ":my_player_no", 0),
           (player_get_agent_id, ":my_agent_id", ":my_player_no"),
           (eq, ":my_agent_id", ":agent_no"),
           (ge, ":my_agent_id", 0),
           (agent_get_team, "$my_team_at_start_of_round", ":my_agent_id"),		   
         (try_end),         
         
         # (call_script, "script_calculate_new_death_waiting_time_at_death_mod"),
 
         (try_begin),
           (neg|multiplayer_is_server),
           (try_begin),
             (eq, "$g_round_ended", 1),
             (assign, "$g_round_ended", 0),

             #initialize scene object slots at start of new round at clients.
             (call_script, "script_initialize_all_scene_prop_slots"),

             #these lines are done in only clients at start of each new round.
             # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),
             (call_script, "script_initialize_objects_clients"),
             #end of lines
             (try_begin),
               (eq, "$g_team_balance_next_round", 1),
               (assign, "$g_team_balance_next_round", 0),
             (try_end),
           (try_end),  
         (try_end),         
         ]),

      (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
         (store_trigger_param_1, ":dead_agent_no"),
         (store_trigger_param_2, ":killer_agent_no"),

         (call_script, "script_multiplayer_server_on_agent_killed_or_wounded_common", ":dead_agent_no", ":killer_agent_no"),

         (try_begin), #if my initial team still not initialized, find and assign its value.
           (lt, "$my_team_at_start_of_round", 0),
           (multiplayer_get_my_player, ":my_player_no"),
           (ge, ":my_player_no", 0),
           (player_get_agent_id, ":my_agent_id", ":my_player_no"),
           (ge, ":my_agent_id", 0),
           (agent_get_team, "$my_team_at_start_of_round", ":my_agent_id"),
         (try_end),         
         
         (try_begin), #count players and if round ended understand this.
           (agent_is_human, ":dead_agent_no"),
           (assign, ":team1_living_players", 0),
           (assign, ":team2_living_players", 0),
           (try_for_agents, ":cur_agent"),
             (agent_is_human, ":cur_agent"),
             (try_begin),
               (agent_is_alive, ":cur_agent"),  
               (agent_get_team, ":cur_agent_team", ":cur_agent"),
               (try_begin),
                 (eq, ":cur_agent_team", 0),
               (val_add, ":team1_living_players", 1),
               (else_try),
                 (eq, ":cur_agent_team", 1),
                 (val_add, ":team2_living_players", 1),
               (try_end),
             (try_end),
           (try_end),                    
           (try_begin),         
             (eq, "$g_round_ended", 0),
             (try_begin),
               (this_or_next|eq, ":team1_living_players", 0),
               (eq, ":team2_living_players", 0),                
               (assign, "$g_winner_team", -1),
               (assign, reg0, "$g_multiplayer_respawn_period"),
               (try_begin),
                 (eq, ":team1_living_players", 0),
                 (try_begin),
                   (neq, ":team2_living_players", 0),
                   (team_get_score, ":team_2_score", 1),
                   (val_add, ":team_2_score", 1),
                   (team_set_score, 1, ":team_2_score"),
                   (assign, "$g_winner_team", 1),
                 (try_end),

                 (call_script, "script_show_multiplayer_message", multiplayer_message_type_round_result_in_battle_mode, "$g_winner_team"), #1 is winner team
                 (call_script, "script_check_achievement_last_man_standing", "$g_winner_team"),
               (else_try),
                 (try_begin),
                   (neq, ":team1_living_players", 0),
                   (team_get_score, ":team_1_score", 0),
                   (val_add, ":team_1_score", 1),
                   (team_set_score, 0, ":team_1_score"),
                   (assign, "$g_winner_team", 0),
                 (try_end),

                 (call_script, "script_show_multiplayer_message", multiplayer_message_type_round_result_in_battle_mode, "$g_winner_team"), #0 is winner team  
                 (call_script, "script_check_achievement_last_man_standing", "$g_winner_team"),       
               (try_end),
               (store_mission_timer_a, "$g_round_finish_time"),
               (assign, "$g_round_ended", 1),
#MOD begin
							(try_begin),
								(multiplayer_is_server),
								(get_max_players, ":num_players"),
								(try_for_range, ":cur_player_no", 1, ":num_players"),
									(player_is_active, ":cur_player_no"),
									(player_get_unique_id, ":cur_unique_id", ":cur_player_no"),
									(store_div, ":offset", ":cur_unique_id", 1048576),
									(store_mul, ":id_sub", ":offset", 1048576),
									(val_sub, ":cur_unique_id", ":id_sub"),
									
									(store_add, ":array", "trp_unquie_id_team_hits_0", ":offset"),
									(troop_set_slot, ":array", ":cur_unique_id", 0),#reset
									#apply round bonus
									(multiplayer_is_dedicated_server),
									(player_get_team_no, ":cur_player_team", ":cur_player_no"),
									(store_add, ":array", "trp_unquie_id_round_bonus_0", ":offset"),
									(try_begin),
										(eq, ":cur_player_team", "$g_winner_team"),
										(troop_get_slot, ":cur_round_bonus", ":array", ":cur_unique_id"),
										(val_add, ":cur_round_bonus", 1),
										(troop_set_slot, ":array", ":cur_unique_id", ":cur_round_bonus"),
									(else_try),#reset
										(troop_set_slot, ":array", ":cur_unique_id", 0),
									(try_end),
								(try_end),
							(try_end),
#MOD end
             (try_end),
           (try_end),
         (try_end),

         (try_begin),
           (multiplayer_is_server),
           (agent_is_human, ":dead_agent_no"),
           (neg|agent_is_non_player, ":dead_agent_no"),

           (ge, ":dead_agent_no", 0),
           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (ge, ":dead_agent_player_id", 0),

           (set_fixed_point_multiplier, 100),

           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (agent_get_position, pos0, ":dead_agent_no"),

           (position_get_x, ":x_coor", pos0),
           (position_get_y, ":y_coor", pos0),
           (position_get_z, ":z_coor", pos0),
         
           (player_set_slot, ":dead_agent_player_id", slot_player_death_pos_x, ":x_coor"),
           (player_set_slot, ":dead_agent_player_id", slot_player_death_pos_y, ":y_coor"),
           (player_set_slot, ":dead_agent_player_id", slot_player_death_pos_z, ":z_coor"),
         (try_end),    
         ]),

      (ti_on_multiplayer_mission_end, 0, 0, [],
       [
         (call_script, "script_multiplayer_event_mission_end"),
         (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
         (start_presentation, "prsnt_multiplayer_stats_chart"),
         ]),
      
      # (1, 0, 0, [(multiplayer_is_server), 
                 # (eq, "$g_round_ended", 0),
                 # (store_mission_timer_a, ":current_time"),
                 # (store_sub, ":seconds_past_in_round", ":current_time", "$g_round_start_time"),
                 # (ge, ":seconds_past_in_round", "$g_multiplayer_round_max_seconds"),

                 # (assign, ":overtime_needed", 0), #checking for if overtime is needed. Overtime happens when lower heighted flag is going up
                 # (try_begin),
                   # (eq, "$g_battle_death_mode_started", 2), #if death mod is currently open
                    
                   # (scene_prop_get_instance, ":pole_1_id", "spr_headquarters_pole_code_only", 0),
                   # (scene_prop_get_instance, ":pole_2_id", "spr_headquarters_pole_code_only", 1),
                   # (scene_prop_get_instance, ":flag_1_id", "$team_1_flag_scene_prop", 0),
                   # (scene_prop_get_instance, ":flag_2_id", "$team_2_flag_scene_prop", 0),

                   # (prop_instance_get_position, pos1, ":pole_1_id"),
                   # (prop_instance_get_position, pos2, ":pole_2_id"),
                   # (prop_instance_get_position, pos3, ":flag_1_id"),
                   # (prop_instance_get_position, pos4, ":flag_2_id"),

                   # (get_distance_between_positions, ":height_of_flag_1", pos1, pos3),
                   # (get_distance_between_positions, ":height_of_flag_2", pos2, pos4),
                   # (store_add, ":height_of_flag_1_plus", ":height_of_flag_1", min_allowed_flag_height_difference_to_make_score),
                   # (store_add, ":height_of_flag_2_plus", ":height_of_flag_2", min_allowed_flag_height_difference_to_make_score),

                   # (try_begin),
                     # (le, ":height_of_flag_1", ":height_of_flag_2_plus"),
                     # (prop_instance_is_animating, ":is_animating", ":flag_1_id"),
                     # (eq, ":is_animating", 1),
                     # (prop_instance_get_animation_target_position, pos5, ":flag_1_id"),
                     # (position_get_z, ":flag_2_animation_target_z", pos5),
                     # (position_get_z, ":flag_1_cur_z", pos3),
                     # (ge, ":flag_2_animation_target_z", ":flag_1_cur_z"),
                     # (assign, ":overtime_needed", 1),
                   # (try_end),
                   
                   # (try_begin),
                     # (le, ":height_of_flag_2", ":height_of_flag_1_plus"),
                     # (prop_instance_is_animating, ":is_animating", ":flag_2_id"),
                     # (eq, ":is_animating", 1),
                     # (prop_instance_get_animation_target_position, pos5, ":flag_2_id"),
                     # (position_get_z, ":flag_2_animation_target_z", pos5),
                     # (position_get_z, ":flag_2_cur_z", pos4),
                     # (ge, ":flag_2_animation_target_z", ":flag_2_cur_z"),
                     # (assign, ":overtime_needed", 1),
                   # (try_end),
                 # (try_end),
                 # (eq, ":overtime_needed", 0),
                 # ],
       # [ #round time is up
         # (store_mission_timer_a, "$g_round_finish_time"),                          
         # (assign, "$g_round_ended", 1),
         # (assign, "$g_winner_team", -1),
         
         # (try_begin), #checking for winning by death mod
           # (eq, "$g_battle_death_mode_started", 2), #if death mod is currently open

           # (scene_prop_get_instance, ":pole_1_id", "spr_headquarters_pole_code_only", 0),
           # (scene_prop_get_instance, ":pole_2_id", "spr_headquarters_pole_code_only", 1),
           # (scene_prop_get_instance, ":flag_1_id", "$team_1_flag_scene_prop", 0),
           # (scene_prop_get_instance, ":flag_2_id", "$team_2_flag_scene_prop", 0),

           # (prop_instance_get_position, pos1, ":pole_1_id"),
           # (prop_instance_get_position, pos2, ":pole_2_id"),
           # (prop_instance_get_position, pos3, ":flag_1_id"),
           # (prop_instance_get_position, pos4, ":flag_2_id"),

           # (get_distance_between_positions, ":height_of_flag_1", pos1, pos3),
           # (get_distance_between_positions, ":height_of_flag_2", pos2, pos4),

           # (try_begin),
             # (ge, ":height_of_flag_1", ":height_of_flag_2"), #if flag_1 is higher than flag_2
             # (store_sub, ":difference_of_heights", ":height_of_flag_1", ":height_of_flag_2"), 
             # (ge, ":difference_of_heights", min_allowed_flag_height_difference_to_make_score), #if difference between flag heights is greater than 
             # (assign, "$g_winner_team", 0),                                                    #"min_allowed_flag_height_difference_to_make_score" const value
           # (else_try), #if flag_2 is higher than flag_1
             # (store_sub, ":difference_of_heights", ":height_of_flag_2", ":height_of_flag_1"),
             # (ge, ":difference_of_heights", min_allowed_flag_height_difference_to_make_score), #if difference between flag heights is greater than 
             # (assign, "$g_winner_team", 1),                                                    #"min_allowed_flag_height_difference_to_make_score" const value
           # (try_end),
         # (try_end),
    
         # (multiplayer_get_my_player, ":my_player_no"), #send all players draw information of round.
         # #for only server itself-----------------------------------------------------------------------------------------------
         # (call_script, "script_draw_this_round", "$g_winner_team"),
         # #for only server itself-----------------------------------------------------------------------------------------------
         # (get_max_players, ":num_players"), 
         # (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
           # (player_is_active, ":player_no"),
           # (neq, ":player_no", ":my_player_no"),
           # (multiplayer_send_int_to_player, ":player_no", multiplayer_event_draw_this_round, "$g_winner_team"),
         # (try_end),
        # ]),          

      (10, 0, 0, [(multiplayer_is_server)],
       [
         #auto team balance control during the round         
         (assign, ":number_of_players_at_team_1", 0),
         (assign, ":number_of_players_at_team_2", 0),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 0, ":num_players"),
           (player_is_active, ":cur_player"),
           (player_get_team_no, ":player_team", ":cur_player"),
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":number_of_players_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":number_of_players_at_team_2", 1),
           (try_end),         
         (try_end),
         #end of counting active players per team.
         (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),
         (assign, ":number_of_players_will_be_moved", 0),
         (try_begin),
           (try_begin),
             (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
             (le, ":difference_of_number_of_players", ":checked_value"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", -2),
           (else_try),
             (ge, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", 2),
           (try_end),          
         (try_end),         
         #number of players will be moved calculated. (it is 0 if no need to make team balance)
         (try_begin),
           (gt, ":number_of_players_will_be_moved", 0),
           (try_begin),
             (eq, "$g_team_balance_next_round", 0),
         
             (assign, "$g_team_balance_next_round", 1),

             #for only server itself-----------------------------------------------------------------------------------------------
             (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_next, 0), #0 is useless here
             #for only server itself-----------------------------------------------------------------------------------------------     
             (get_max_players, ":num_players"),                               
             (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
               (player_is_active, ":player_no"),
               (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_next),
             (try_end),
             
             (call_script, "script_warn_player_about_auto_team_balance"),
           (try_end),
         (try_end),           
         #team balance check part finished
         ]),
                
      (1, 0, 3, [(multiplayer_is_server),
                 (eq, "$g_round_ended", 1),
                 (store_mission_timer_a, ":seconds_past_till_round_ended"),
                 (val_sub, ":seconds_past_till_round_ended", "$g_round_finish_time"),
                 (ge, ":seconds_past_till_round_ended", "$g_multiplayer_respawn_period")],
       [
         #auto team balance control at the end of round         
         (assign, ":number_of_players_at_team_1", 0),
         (assign, ":number_of_players_at_team_2", 0),
         (get_max_players, ":num_players"),
         (try_for_range, ":cur_player", 0, ":num_players"),
           (player_is_active, ":cur_player"),
           (player_get_team_no, ":player_team", ":cur_player"),
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":number_of_players_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":number_of_players_at_team_2", 1),
           (try_end),         
         (try_end),
         #end of counting active players per team.
         (store_sub, ":difference_of_number_of_players", ":number_of_players_at_team_1", ":number_of_players_at_team_2"),
         (assign, ":number_of_players_will_be_moved", 0),
         (try_begin),
           (try_begin),
             (store_mul, ":checked_value", "$g_multiplayer_auto_team_balance_limit", -1),
             (le, ":difference_of_number_of_players", ":checked_value"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", -2),
             (assign, ":team_with_more_players", 1),
             (assign, ":team_with_less_players", 0),
           (else_try),
             (ge, ":difference_of_number_of_players", "$g_multiplayer_auto_team_balance_limit"),
             (store_div, ":number_of_players_will_be_moved", ":difference_of_number_of_players", 2),
             (assign, ":team_with_more_players", 0),
             (assign, ":team_with_less_players", 1),
           (try_end),
         (try_end),         
         #number of players will be moved calculated. (it is 0 if no need to make team balance)
         (try_begin),
           (gt, ":number_of_players_will_be_moved", 0),
           (try_begin),
             #(eq, "$g_team_balance_next_round", 1), #control if at pre round players are warned about team change.

             (try_for_range, ":unused", 0, ":number_of_players_will_be_moved"), 
               (assign, ":max_player_join_time", 0),
               (assign, ":latest_joined_player_no", -1),
               (get_max_players, ":num_players"),                               
               (try_for_range, ":player_no", 0, ":num_players"),
                 (player_is_active, ":player_no"),
                 (player_get_team_no, ":player_team", ":player_no"),
                 (eq, ":player_team", ":team_with_more_players"),
                 (player_get_slot, ":player_join_time", ":player_no", slot_player_join_time),
                 (try_begin),
                   (gt, ":player_join_time", ":max_player_join_time"),
                   (assign, ":max_player_join_time", ":player_join_time"),
                   (assign, ":latest_joined_player_no", ":player_no"),
                 (try_end),
               (try_end),
               (try_begin),
                 (ge, ":latest_joined_player_no", 0),
                 (try_begin),
                   #if player is living add +1 to his kill count because he will get -1 because of team change while living.
                   (player_get_agent_id, ":latest_joined_agent_id", ":latest_joined_player_no"), 
                   (ge, ":latest_joined_agent_id", 0),
                   (agent_is_alive, ":latest_joined_agent_id"),
#MOD disable
                   # (player_get_kill_count, ":player_kill_count", ":latest_joined_player_no"), #adding 1 to his kill count, because he will lose 1 undeserved kill count for dying during team change
                   # (val_add, ":player_kill_count", 1),
                   # (player_set_kill_count, ":latest_joined_player_no", ":player_kill_count"),

                   (player_get_death_count, ":player_death_count", ":latest_joined_player_no"), #subtracting 1 to his death count, because he will gain 1 undeserved death count for dying during team change
                   (val_sub, ":player_death_count", 1),
                   (player_set_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD begin
									(try_begin),
										(player_get_slot, ":cur_player_deaths", ":latest_joined_player_no", slot_player_deaths),
										(val_sub, ":cur_player_deaths", 1),
										(player_set_slot, ":latest_joined_player_no", slot_player_deaths, ":cur_player_deaths"),
									(try_end),
#MOD end

#MOD disable
                   # (player_get_score, ":player_score", ":latest_joined_player_no"), #adding 1 to his score count, because he will lose 1 undeserved score for dying during team change
                   # (val_add, ":player_score", 1),
                   # (player_set_score, ":latest_joined_player_no", ":player_score"),

                   (try_for_range, ":player_no", 1, ":num_players"), #0 is server so starting from 1
                     (player_is_active, ":player_no"),
#MOD begin											 
#                     (multiplayer_send_4_int_to_player, ":player_no", multiplayer_event_set_player_score_kill_death, ":latest_joined_player_no", ":player_score", ":player_kill_count", ":player_death_count"),
										(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_player_death_count, ":latest_joined_player_no", ":player_death_count"),
#MOD end
                   (try_end),  
#MOD begin
				   # (player_get_slot, ":troop_no", ":latest_joined_player_no", slot_player_use_troop),
				   # (try_begin),
				     # (neq, ":troop_no", "trp_online_character"),
                     # (player_get_value_of_original_items, ":old_items_value", ":latest_joined_player_no"),
                     # (player_get_gold, ":player_gold", ":latest_joined_player_no"),
                     # (val_add, ":player_gold", ":old_items_value"),
                     # (player_set_gold, ":latest_joined_player_no", ":player_gold", multi_max_gold_that_can_be_stored),
				   # (try_end),
                 (try_end),
#                 (player_set_troop_id, ":latest_joined_player_no", -1),#MOD disable
                 (player_set_slot, ":latest_joined_player_no", slot_player_use_troop, -1),
#MOD end
                 (player_set_team_no, ":latest_joined_player_no", ":team_with_less_players"),
                 (multiplayer_send_message_to_player, ":latest_joined_player_no", multiplayer_event_force_start_team_selection),
               (try_end),
             (try_end),
             #tutorial message (after team balance)
             
             #(tutorial_message_set_position, 500, 500),
             #(tutorial_message_set_size, 30, 30),
             #(tutorial_message_set_center_justify, 1),
             #(tutorial_message, "str_auto_team_balance_done", 0xFFFFFFFF, 5),

             #for only server itself
             (call_script, "script_show_multiplayer_message", multiplayer_message_type_auto_team_balance_done, 0), 

             #no need to send also server here
             (multiplayer_get_my_player, ":my_player_no"),
             (get_max_players, ":num_players"),                               
             (try_for_range, ":player_no", 0, ":num_players"),
               (player_is_active, ":player_no"),
               (neq, ":my_player_no", ":player_no"),
               (multiplayer_send_int_to_player, ":player_no", multiplayer_event_show_multiplayer_message, multiplayer_message_type_auto_team_balance_done),
             (try_end),
             (assign, "$g_team_balance_next_round", 0),
           (try_end),
         (try_end),           
         #team balance check part finished
         (assign, "$g_team_balance_next_round", 0),

         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),           
           (player_get_agent_id, ":player_agent", ":player_no"),
           (ge, ":player_agent", 0),
           (agent_is_alive, ":player_agent"),
           # (player_save_picked_up_items_for_next_spawn, ":player_no"),#MOD disable
           (player_get_value_of_original_items, ":old_items_value", ":player_no"),
           (player_set_slot, ":player_no", slot_player_last_rounds_used_item_earnings, ":old_items_value"),
         (try_end),

         #money management
         (assign, ":per_round_gold_addition", multi_battle_round_team_money_add),
         (val_mul, ":per_round_gold_addition", "$g_multiplayer_round_earnings_multiplier"),
         (val_div, ":per_round_gold_addition", 100),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),		   
					 (player_slot_eq, ":player_no", slot_player_spawned_this_round, 1),

           (player_get_gold, ":player_gold", ":player_no"),
           (player_get_team_no, ":player_team", ":player_no"),

           (try_begin),
             (this_or_next|eq, ":player_team", 0),
             (eq, ":player_team", 1),
             (val_add, ":player_gold", ":per_round_gold_addition"), 
           (try_end),
#MOD disable
           # #(below lines added new at 25.11.09 after Armagan decided new money system)
           # (try_begin),
             # (player_get_slot, ":old_items_value", ":player_no", slot_player_last_rounds_used_item_earnings),
             # (store_add, ":player_total_potential_gold", ":player_gold", ":old_items_value"),
             # (store_mul, ":minimum_gold", "$g_multiplayer_initial_gold_multiplier", 10),
             # (lt, ":player_total_potential_gold", ":minimum_gold"),
             # (store_sub, ":additional_gold", ":minimum_gold", ":player_total_potential_gold"),
             # (val_add, ":player_gold", ":additional_gold"),
           # (try_end),
           # #new money system addition end
         (try_end),

         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_set_slot, ":player_no", slot_player_spawned_this_round, 0),
         (try_end),

         #initialize my team at start of round (it will be assigned again at next round's first death)
         (assign, "$my_team_at_start_of_round", -1),

         #clear scene and end round
         (multiplayer_clear_scene),

         # (call_script, "script_multiplayer_initialize_belfry_wheel_rotations"),

         # (try_begin),
           # (eq, "$g_battle_death_mode_started", 2),
           # (call_script, "script_move_death_mode_flags_down"),
         # (try_end),

         # (assign, "$g_battle_death_mode_started", 0),
         (assign, "$g_reduced_waiting_seconds", 0),
         
         #initialize moveable object positions
         (call_script, "script_multiplayer_close_gate_if_it_is_open"),
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),
                  
         (assign, "$g_round_ended", 0), 

         (assign, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1"), 
         (assign, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2"), 

         (store_mission_timer_a, "$g_round_start_time"),
         (call_script, "script_initialize_all_scene_prop_slots"),

				 (call_script, "script_init_on_round_start"),#MOD
         #initialize round start times for clients
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (multiplayer_send_int_to_player, ":player_no", multiplayer_event_set_round_start_time, -9999), #this will also initialize moveable object slots.
         (try_end),         
       ]),

      (0, 0, 0, [], #if there is nobody in any teams do not reduce round time.
       [
         #(multiplayer_is_server),
         (assign, ":human_agents_spawned_at_team_1", "$g_multiplayer_num_bots_team_1"),
         (assign, ":human_agents_spawned_at_team_2", "$g_multiplayer_num_bots_team_2"),
         
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (player_get_team_no, ":player_team", ":player_no"), 
           (try_begin),
             (eq, ":player_team", 0),
             (val_add, ":human_agents_spawned_at_team_1", 1),
           (else_try),
             (eq, ":player_team", 1),
             (val_add, ":human_agents_spawned_at_team_2", 1),
           (try_end),
         (try_end),

         (try_begin),
           (this_or_next|eq, ":human_agents_spawned_at_team_1", 0),
           (eq, ":human_agents_spawned_at_team_2", 0),

           (store_mission_timer_a, ":seconds_past_since_round_started"),
           (val_sub, ":seconds_past_since_round_started", "$g_round_start_time"),
           (le, ":seconds_past_since_round_started", 2),
                  
           (store_mission_timer_a, "$g_round_start_time"),
         (try_end),
       ]),    
           
      (1, 0, 0, [],
       [
         (multiplayer_is_server),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (neg|player_is_busy_with_menus, ":player_no"),
           (try_begin),
             (player_slot_eq, ":player_no", slot_player_spawned_this_round, 0),

             (player_get_team_no, ":player_team", ":player_no"), #if player is currently spectator do not spawn his agent
             (lt, ":player_team", multi_team_spectator),

#             (player_get_troop_id, ":player_troop", ":player_no"), #if troop is not selected do not spawn his agent #MOD disable
#MOD begin
             (player_get_slot, ":player_troop", ":player_no", slot_player_use_troop),
#MOD end  
             (ge, ":player_troop", 0),

             (assign, ":spawn_new", 0),
             (assign, ":num_active_players_in_team_0", 0),
             (assign, ":num_active_players_in_team_1", 0),
             (try_begin),
               (assign, ":num_active_players", 0),
               (get_max_players, ":num_players"),
               (try_for_range, ":player_no_2", 0, ":num_players"),
                 (player_is_active, ":player_no_2"),
                 (val_add, ":num_active_players", 1),
                 (player_get_team_no, ":player_team_2", ":player_no_2"),
                 (try_begin),
                   (eq, ":player_team_2", 0),
                   (val_add, ":num_active_players_in_team_0", 1),
                 (else_try),
                   (eq, ":player_team_2", 1),
                   (val_add, ":num_active_players_in_team_1", 1),
                 (try_end),
               (try_end),

               (store_mul, ":multipication_of_num_active_players_in_teams", ":num_active_players_in_team_0", ":num_active_players_in_team_1"),

               (store_mission_timer_a, ":round_time"),
               (val_sub, ":round_time", "$g_round_start_time"),

               (this_or_next|lt, ":round_time", multiplayer_new_agents_finish_spawning_time),
               (this_or_next|le, ":num_active_players", 2),
               (eq, ":multipication_of_num_active_players_in_teams", 0),
         
               (eq, "$g_round_ended", 0),
               (assign, ":spawn_new", 1),
             (try_end),
             (eq, ":spawn_new", 1),
             (try_begin),
               (eq, ":player_team", 0),
               (assign, ":entry_no", multi_initial_spawn_point_team_1),
             (else_try),
               (eq, ":player_team", 1),
               (assign, ":entry_no", multi_initial_spawn_point_team_2),
             (try_end),
             (call_script, "script_multiplayer_buy_agent_equipment", ":player_no"),
             (player_spawn_new_agent, ":player_no", ":entry_no"),
             (player_set_slot, ":player_no", slot_player_spawned_this_round, 1),
           (else_try), #spawning as a bot (if option ($g_multiplayer_player_respawn_as_bot) is 1)
             (eq, "$g_multiplayer_player_respawn_as_bot", 1),
             (player_get_agent_id, ":player_agent", ":player_no"),
             (ge, ":player_agent", 0),
             (neg|agent_is_alive, ":player_agent"),
             (agent_get_time_elapsed_since_removed, ":elapsed_time", ":player_agent"),
             (gt, ":elapsed_time", "$g_multiplayer_respawn_period"),

             (player_get_team_no, ":player_team", ":player_no"),
             (assign, ":is_found", 0),
             (try_for_agents, ":cur_agent"),
               (eq, ":is_found", 0),
               (agent_is_alive, ":cur_agent"),
               (agent_is_human, ":cur_agent"),
               (agent_is_non_player, ":cur_agent"),
               (agent_get_team ,":cur_team", ":cur_agent"),
               (eq, ":cur_team", ":player_team"),
               (assign, ":is_found", 1),
               #(player_control_agent, ":player_no", ":cur_agent"),
             (try_end),

             (try_begin),
               (eq, ":is_found", 1),
               (call_script, "script_find_most_suitable_bot_to_control", ":player_no"),
               (player_control_agent, ":player_no", reg0),

               (player_get_slot, ":num_spawns", ":player_no", slot_player_spawned_this_round),
               (val_add, ":num_spawns", 1),
               (player_set_slot, ":player_no", slot_player_spawned_this_round, ":num_spawns"),
#MOD begin					 
								(try_begin),#synce slots
									(gt, ":player_no", 0), #dont sent to server
									(try_for_range, ":slot_no", slot_agent_drown_time, slot_agent_nudge_state),
										(agent_get_slot, ":value", reg0, ":slot_no"),
										(neq, ":value", 0),
										(multiplayer_send_2_int_to_player, ":player_no", multiplayer_event_client_set_agent_slot, ":slot_no", ":value"),
									(try_end),
								(try_end),
#MOD end
             (try_end),
           (try_end),
         (try_end),
         ]),

      multiplayer_server_spawn_bots, 
      multiplayer_server_manage_bots, 

      multiplayer_server_check_end_map,
        
      (ti_tab_pressed, 0, 0, [],
       [
         (try_begin),
           (eq, "$g_multiplayer_mission_end_screen", 0),
           (assign, "$g_multiplayer_stats_chart_opened_manually", 1),
           (start_presentation, "prsnt_multiplayer_stats_chart"),
         (try_end),
         ]),

      # multiplayer_once_at_the_first_frame,

      (ti_battle_window_opened, 0, 0, [], [
				(eq, "$g_disable_hud", 0),#MOD
        (start_presentation, "prsnt_multiplayer_round_time_counter"),
        (start_presentation, "prsnt_multiplayer_team_score_display"),
        # (try_begin),
          # (eq, "$g_battle_death_mode_started", 2),
          # (start_presentation, "prsnt_multiplayer_flag_projection_display_bt"),
        # (try_end),
        ]),

      (ti_escape_pressed, 0, 0, [],
       [
         (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
         (neg|is_presentation_active, "prsnt_multiplayer_stats_chart"),
         (eq, "$g_waiting_for_confirmation_to_terminate", 0),
         (start_presentation, "prsnt_multiplayer_escape_menu"),
         ]),
      ] + common_mod_multiplayer, #MOD
  ),

    (
    "multiplayer_duel",mtf_battle_mode,-1, #duel mode
    "You lead your men to battle.",
    [
      (0,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (1,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (2,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (3,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (4,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (5,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (6,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (7,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (8,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (9,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (10,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (11,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (12,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (13,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (14,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (15,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (16,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (17,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (18,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (19,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (20,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (21,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (22,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (23,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (24,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (25,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (26,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (27,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (28,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (29,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (30,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),
      (31,mtef_visitor_source|mtef_team_0,0,aif_start_alarmed,1,[]),

      (32,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (33,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (34,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (35,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (36,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (37,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (38,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (39,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (40,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (41,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (42,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (43,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (44,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (45,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (46,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (47,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (48,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (49,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (50,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (51,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (52,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (53,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (54,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (55,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),

      (56,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (57,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (58,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (59,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (60,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (61,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (62,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
      (63,mtef_visitor_source|mtef_team_1,0,aif_start_alarmed,1,[]),
     ],
    [
      multiplayer_server_check_polls,

      (ti_on_agent_spawn, 0, 0, [],
       [
         (store_trigger_param_1, ":agent_no"),
         (call_script, "script_multiplayer_server_on_agent_spawn_common", ":agent_no"),
         ]),
      
      (ti_server_player_joined, 0, 0, [],
       [
         (store_trigger_param_1, ":player_no"),
         (call_script, "script_multiplayer_server_player_joined_common", ":player_no"),
         ]),

      (ti_before_mission_start, 0, 0, [],
       [
         (assign, "$g_multiplayer_game_type", multiplayer_game_type_duel),
         (call_script, "script_multiplayer_server_before_mission_start_common"),
         #make everyone see themselves as allies, no friendly fire
         (team_set_relation, 0, 0, 1),
         (team_set_relation, 0, 1, 1),
         (team_set_relation, 1, 1, 1),
         (mission_set_duel_mode, 1),
         (call_script, "script_multiplayer_init_mission_variables"),# close this line and open map in deathmatch mod and use all ladders firstly 
         ]),                                                            # to be able to edit maps without damaging any headquarters flags ext. 

      (ti_after_mission_start, 0, 0, [], 
       [
         (set_spawn_effector_scene_prop_kind, 0, -1), #during this mission, agents of "team 0" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (set_spawn_effector_scene_prop_kind, 1, -1), #during this mission, agents of "team 1" will try to spawn around scene props with kind equal to -1(no effector for this mod)
         (call_script, "script_initialize_all_scene_prop_slots"),
         (call_script, "script_multiplayer_move_moveable_objects_initial_positions"),
         (assign, "$g_multiplayer_ready_for_spawning_agent", 1),
         ]),

      (ti_on_multiplayer_mission_end, 0, 0, [],
       [
         (call_script, "script_multiplayer_event_mission_end"),
         (assign, "$g_multiplayer_stats_chart_opened_manually", 0),
         (start_presentation, "prsnt_multiplayer_stats_chart_deathmatch"),
         ]),

      (ti_on_agent_killed_or_wounded, 0, 0, [],
       [
         (store_trigger_param_1, ":dead_agent_no"), 
         (store_trigger_param_2, ":killer_agent_no"), 

         (call_script, "script_multiplayer_server_on_agent_killed_or_wounded_common", ":dead_agent_no", ":killer_agent_no"),
#MOD begin
         (try_begin),
           (multiplayer_is_server),
           (agent_is_human, ":dead_agent_no"),
           (neg|agent_is_non_player, ":dead_agent_no"),
           (agent_get_player_id, ":dead_agent_player_id", ":dead_agent_no"),
           (player_set_slot, ":dead_agent_player_id", slot_player_spawned_this_round, 0),
         (try_end),
#MOD end				
         (try_begin),
           (get_player_agent_no, ":player_agent"),
           (agent_is_active, ":player_agent"),
           (agent_slot_ge, ":player_agent", slot_agent_in_duel_with, 0),
           (try_begin),
             (eq, ":dead_agent_no", ":player_agent"),
             (display_message, "str_you_have_lost_a_duel"),
           (else_try),
             (agent_slot_eq, ":player_agent", slot_agent_in_duel_with, ":dead_agent_no"),
             (display_message, "str_you_have_won_a_duel"),
           (try_end),
         (try_end),
         (try_begin),
           (agent_slot_ge, ":dead_agent_no", slot_agent_in_duel_with, 0),
           (agent_get_slot, ":duelist_agent_no", ":dead_agent_no", slot_agent_in_duel_with),
           (agent_set_slot, ":dead_agent_no", slot_agent_in_duel_with, -1),
           (try_begin),
             (agent_is_active, ":duelist_agent_no"),
             (agent_set_slot, ":duelist_agent_no", slot_agent_in_duel_with, -1),
             (agent_clear_relations_with_agents, ":duelist_agent_no"),
             (try_begin),
               (agent_get_player_id, ":duelist_player_no", ":duelist_agent_no"),
               (neg|player_is_active, ":duelist_player_no"), #might be AI
               (agent_force_rethink, ":duelist_agent_no"),
             (try_end),
           (try_end),
         (try_end),
         ]),
      
      (1, 0, 0, [],
       [
         (multiplayer_is_server),
         (get_max_players, ":num_players"),
         (try_for_range, ":player_no", 0, ":num_players"),
           (player_is_active, ":player_no"),
           (neg|player_is_busy_with_menus, ":player_no"),

           (player_get_team_no, ":player_team", ":player_no"), #if player is currently spectator do not spawn his agent
           (lt, ":player_team", multi_team_spectator),

#           (player_get_troop_id, ":player_troop", ":player_no"), #if troop is not selected do not spawn his agent #MOD disable
#MOD begin
           (player_get_slot, ":player_troop", ":player_no", slot_player_use_troop),
#MOD end  
           (ge, ":player_troop", 0),

           (player_get_agent_id, ":player_agent", ":player_no"),
           (assign, ":spawn_new", 0),
           (try_begin),
             (player_get_slot, ":player_first_spawn", ":player_no", slot_player_first_spawn),
             (eq, ":player_first_spawn", 1),
             (assign, ":spawn_new", 1),
             (player_set_slot, ":player_no", slot_player_first_spawn, 0),
           (else_try),
             (try_begin),
               (lt, ":player_agent", 0),
               (assign, ":spawn_new", 1),
             (else_try),
               (neg|agent_is_alive, ":player_agent"),
               (agent_get_time_elapsed_since_removed, ":elapsed_time", ":player_agent"),
               (gt, ":elapsed_time", "$g_multiplayer_respawn_period"),
               (assign, ":spawn_new", 1),
             (try_end),             
           (try_end),
           (eq, ":spawn_new", 1),
           (call_script, "script_multiplayer_buy_agent_equipment", ":player_no"),

           (troop_get_inventory_slot, ":has_item", ":player_troop", ek_horse),
           (try_begin),
             (ge, ":has_item", 0),
             (assign, ":is_horseman", 1),
           (else_try),
             (assign, ":is_horseman", 0),
           (try_end),
         
           (call_script, "script_multiplayer_find_spawn_point", ":player_team", 0, ":is_horseman"), 
           (player_spawn_new_agent, ":player_no", reg0),
					 (player_set_slot, ":player_no", slot_player_spawned_this_round, 1),#MOD
         (try_end),
         ]),

      (1, 0, 0, [], #do this in every new frame, but not at the same time
       [
         (multiplayer_is_server),
         (store_mission_timer_a, ":mission_timer"),
         (ge, ":mission_timer", 2),
         (assign, ":team_1_count", 0),
         (assign, ":team_2_count", 0),
         (try_for_agents, ":cur_agent"),
           (agent_is_non_player, ":cur_agent"),
           (agent_is_human, ":cur_agent"),
           (assign, ":will_be_counted", 0),
           (try_begin),
             (agent_is_alive, ":cur_agent"),
             (assign, ":will_be_counted", 1), #alive so will be counted
           (else_try),
             (agent_get_time_elapsed_since_removed, ":elapsed_time", ":cur_agent"),
             (le, ":elapsed_time", "$g_multiplayer_respawn_period"),
             (assign, ":will_be_counted", 1), 
           (try_end),
           (eq, ":will_be_counted", 1),
           (agent_get_team, ":cur_team", ":cur_agent"),
           (try_begin),
             (eq, ":cur_team", 0),
             (val_add, ":team_1_count", 1),
           (else_try),
             (eq, ":cur_team", 1),
             (val_add, ":team_2_count", 1),
           (try_end),
         (try_end),
         (store_sub, "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_team_1", ":team_1_count"),
         (store_sub, "$g_multiplayer_num_bots_required_team_2", "$g_multiplayer_num_bots_team_2", ":team_2_count"),
         (val_max, "$g_multiplayer_num_bots_required_team_1", 0),
         (val_max, "$g_multiplayer_num_bots_required_team_2", 0),
         ]),

      (0, 0, 0, [],
       [
         (multiplayer_is_server),
         (eq, "$g_multiplayer_ready_for_spawning_agent", 1),
         (store_add, ":total_req", "$g_multiplayer_num_bots_required_team_1", "$g_multiplayer_num_bots_required_team_2"),
         (try_begin),
           (gt, ":total_req", 0),
           (store_random_in_range, ":random_req", 0, ":total_req"),
           (val_sub, ":random_req", "$g_multiplayer_num_bots_required_team_1"),
           (try_begin),
             (lt, ":random_req", 0),
             #add to team 1
             (assign, ":selected_team", 0),
             (val_sub, "$g_multiplayer_num_bots_required_team_1", 1),
           (else_try),
             #add to team 2
             (assign, ":selected_team", 1),
             (val_sub, "$g_multiplayer_num_bots_required_team_2", 1),
           (try_end),

           (team_get_faction, ":team_faction_no", ":selected_team"),
           (assign, ":available_troops_in_faction", 0),

           (try_for_range, ":troop_no", multiplayer_ai_troops_begin, multiplayer_ai_troops_end),
             (store_troop_faction, ":troop_faction", ":troop_no"),
             (eq, ":troop_faction", ":team_faction_no"),
             (val_add, ":available_troops_in_faction", 1),
           (try_end),

           (store_random_in_range, ":random_troop_index", 0, ":available_troops_in_faction"),
           (assign, ":end_cond", multiplayer_ai_troops_end),
           (try_for_range, ":troop_no", multiplayer_ai_troops_begin, ":end_cond"),
             (store_troop_faction, ":troop_faction", ":troop_no"),
             (eq, ":troop_faction", ":team_faction_no"),
             (val_sub, ":random_troop_index", 1),
             (lt, ":random_troop_index", 0),
             (assign, ":end_cond", 0),
             (assign, ":selected_troop", ":troop_no"),
           (try_end),
         
           (troop_get_inventory_slot, ":has_item", ":selected_troop", ek_horse),
           (try_begin),
             (ge, ":has_item", 0),
             (assign, ":is_horseman", 1),
           (else_try),
             (assign, ":is_horseman", 0),
           (try_end),

           (call_script, "script_multiplayer_find_spawn_point", ":selected_team", 0, ":is_horseman"), 
           (store_current_scene, ":cur_scene"),
           (modify_visitors_at_site, ":cur_scene"),
           (add_visitors_to_current_scene, reg0, ":selected_troop", 1, ":selected_team", -1),
           (assign, "$g_multiplayer_ready_for_spawning_agent", 0),
         (try_end),
         ]),

      # (1, 0, 0, [],
       # [
         # (multiplayer_is_server),
         # checking for restarting the map
         # (assign, ":end_map", 0),
         # (try_begin),
           # (store_mission_timer_a, ":mission_timer"),
           # (store_mul, ":game_max_seconds", "$g_multiplayer_game_max_minutes", 60),
           # (gt, ":mission_timer", ":game_max_seconds"),
           # (assign, ":end_map", 1),
         # (try_end),
         # (try_begin),
           # (eq, ":end_map", 1),
           # (call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
           # (start_multiplayer_mission, reg0, "$g_multiplayer_selected_map", 0),
           # (call_script, "script_game_set_multiplayer_mission_end"),
         # (try_end),
         # ]),
        
      (ti_tab_pressed, 0, 0, [],
       [
         (try_begin),
           (eq, "$g_multiplayer_mission_end_screen", 0),
           (assign, "$g_multiplayer_stats_chart_opened_manually", 1),
           (start_presentation, "prsnt_multiplayer_stats_chart_deathmatch"),
         (try_end),
         ]),

      # multiplayer_once_at_the_first_frame,
      
      (ti_escape_pressed, 0, 0, [],
       [
         (neg|is_presentation_active, "prsnt_multiplayer_escape_menu"),
         (neg|is_presentation_active, "prsnt_multiplayer_stats_chart_deathmatch"),
         (eq, "$g_waiting_for_confirmation_to_terminate", 0),
         (start_presentation, "prsnt_multiplayer_escape_menu"),
         ]),

      (1, 0, 0, [],
       [
         (store_mission_timer_a, ":mission_timer"),
         (store_sub, ":duel_start_time", ":mission_timer", 3),
         (try_for_agents, ":cur_agent"),
           (agent_slot_ge, ":cur_agent", slot_agent_in_duel_with, 0),
           (agent_get_slot, ":duel_time", ":cur_agent", slot_agent_duel_start_time),
           (ge, ":duel_time", 0),
           (le, ":duel_time", ":duel_start_time"),
           (agent_set_slot, ":cur_agent", slot_agent_duel_start_time, -1),
           (agent_get_slot, ":opponent_agent", ":cur_agent", slot_agent_in_duel_with),
           (agent_is_active, ":opponent_agent"),
           (agent_add_relation_with_agent, ":cur_agent", ":opponent_agent", -1),
           (agent_force_rethink, ":cur_agent"),
         (try_end),
         ]),
      ] + common_mod_multiplayer, #MOD
  ),
]